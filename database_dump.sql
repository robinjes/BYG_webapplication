-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: mysql17j08.db.hostpoint.internal
-- Generation Time: Jan 25, 2016 at 04:06 PM
-- Server version: 5.5.40-log
-- PHP Version: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beyondyo_gallery`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE IF NOT EXISTS `access_token` (
  `token` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` (`token`, `user_id`) VALUES
('26cf211dc14ee58dbeede52366d05b9d', 2);

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL DEFAULT 'Anonymous',
  `location` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Switzerland',
  `description` varchar(150) DEFAULT NULL,
  `gallery_id` int(11) NOT NULL,
  `img` varchar(45) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `location`, `description`, `gallery_id`, `img`, `display`) VALUES
(11, 'Tilt', '-', '-', 1, '1_11', 1),
(12, 'Artist #2', '-', '-', 1, '1_12', 1),
(13, 'Artist #3', '-', '-', 1, '1_13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artwork`
--

CREATE TABLE IF NOT EXISTS `artwork` (
  `id` int(11) NOT NULL,
  `title` varchar(35) NOT NULL DEFAULT 'Anonymous',
  `year` int(4) DEFAULT '1988',
  `technique` varchar(35) NOT NULL DEFAULT 'Painting',
  `dim_horizon_cm` double NOT NULL DEFAULT '0',
  `dim_vertica_cm` double NOT NULL DEFAULT '0',
  `description` varchar(100) DEFAULT NULL,
  `artist_id` int(11) NOT NULL,
  `builtFrame_id` int(11) DEFAULT NULL,
  `img` varchar(45) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artwork`
--

INSERT INTO `artwork` (`id`, `title`, `year`, `technique`, `dim_horizon_cm`, `dim_vertica_cm`, `description`, `artist_id`, `builtFrame_id`, `img`, `display`) VALUES
(36, 'Back to boring 30', 0, '', 30, 30, NULL, 11, NULL, '1_11_36', 1),
(37, 'Back to boring 31', 0, '', 30, 30, NULL, 11, NULL, '1_11_37', 1),
(38, 'Back to boring 37', 0, '', 200, 200, NULL, 11, NULL, '1_11_38', 1),
(39, 'Back to boring 41', 0, '', 65, 100, NULL, 11, NULL, '1_11_39', 1),
(40, 'artwork #1', 0, '', 10, 10, NULL, 12, NULL, '1_12_40', 1),
(41, 'artwork #3', 0, '', 50, 20, NULL, 12, NULL, '1_12_41', 1),
(42, 'artwork #2', 0, '', 40, 40, NULL, 12, NULL, '1_12_42', 1),
(43, 'artwork #4', 0, '', 100, 120, NULL, 12, NULL, '1_12_43', 1),
(44, 'artwork #5', 0, '', 200, 100, NULL, 12, NULL, '1_12_44', 1),
(45, 'artwork #1', 2015, '', 100, 120, NULL, 13, NULL, '1_13_45', 1),
(46, 'artwork #2', 2016, '', 40, 40, NULL, 13, NULL, '1_13_46', 1),
(47, 'artwork #3', 2014, '', 50, 20, NULL, 13, NULL, '1_13_47', 1),
(48, 'artwork #4', 2015, '', 200, 100, NULL, 13, NULL, '1_13_48', 1),
(49, 'artwork #5', 2015, '', 10, 10, NULL, 13, NULL, '1_13_49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `control`
--

CREATE TABLE IF NOT EXISTS `control` (
  `id` int(11) NOT NULL,
  `frame_section_enable` int(1) DEFAULT '0',
  `artworks_tag_enable` int(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `control`
--

INSERT INTO `control` (`id`, `frame_section_enable`, `artworks_tag_enable`) VALUES
(2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `error`
--

CREATE TABLE IF NOT EXISTS `error` (
  `id` int(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(35) NOT NULL DEFAULT 'none',
  `source` varchar(100) NOT NULL DEFAULT '-',
  `message` varchar(300) NOT NULL DEFAULT '-',
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `error`
--

INSERT INTO `error` (`id`, `time`, `reference`, `source`, `message`, `user_id`) VALUES
(11, '2015-07-20 08:25:46', 'PHP', 'send_message.php', 'Access Token is Missing!', NULL),
(12, '2015-07-20 13:19:18', 'PHP', 'init_user.php', 'No user found.', NULL),
(13, '2015-07-20 13:20:36', 'PHP', 'init_user.php', 'No user found.', NULL),
(14, '2015-07-20 13:20:49', 'PHP', 'init_user.php', 'No user found.', NULL),
(15, '2015-07-20 14:24:38', 'PHP', 'init_user.php', 'No user found.', NULL),
(16, '2015-07-20 14:24:44', 'PHP', 'init_user.php', 'No user found.', NULL),
(17, '2015-07-20 14:24:54', 'PHP', 'init_user.php', 'No user found.', NULL),
(18, '2015-07-20 14:31:21', 'PHP', 'init_user.php', 'No user found.', NULL),
(19, '2015-10-01 12:31:07', 'PHP', 'get_errors.php', 'Access Token is Missing!', NULL),
(20, '2015-10-07 17:04:10', 'PHP', 'get_artists.php', 'Access Token is Missing!', NULL),
(21, '2015-10-07 17:05:29', 'PHP', 'get_artists.php', 'Access Token is Missing!', NULL),
(22, '2015-10-07 17:26:33', 'PHP', 'get_artists.php', 'Access Token is Missing!', NULL),
(23, '2015-10-16 18:30:58', 'PHP', 'get_artists.php', 'No access found', NULL),
(24, '2015-10-16 18:31:20', 'PHP', 'get_artists.php', 'No access found', NULL),
(25, '2015-10-16 19:07:58', 'PHP', 'get_session.php', 'Access Token is Missing!', NULL),
(26, '2015-10-16 19:09:49', 'PHP', 'get_session.php', 'Access Token is Missing!', NULL),
(27, '2015-10-16 19:12:00', 'PHP', 'get_session.php', 'Access Token is Missing!', NULL),
(28, '2015-10-16 19:12:17', 'PHP', 'get_session.php', 'Access Token is Missing!', NULL),
(29, '2015-10-18 19:15:41', 'PHP', 'get_artists.php', 'No access found', NULL),
(30, '2015-10-18 19:21:05', 'PHP', 'get_artists.php', 'No access found', NULL),
(31, '2015-10-19 13:06:43', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(32, '2015-10-19 13:06:43', 'PHP', 'get_session.php', 'No access found', NULL),
(33, '2015-10-19 13:07:13', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(34, '2015-10-19 13:07:13', 'PHP', 'get_session.php', 'No access found', NULL),
(35, '2015-10-19 13:09:27', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(36, '2015-10-19 13:09:28', 'PHP', 'get_session.php', 'No access found', NULL),
(37, '2015-10-19 13:12:32', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(38, '2015-10-19 13:12:33', 'PHP', 'get_session.php', 'No access found', NULL),
(39, '2015-10-19 13:12:56', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(40, '2015-10-19 13:12:57', 'PHP', 'get_session.php', 'No access found', NULL),
(41, '2015-10-19 13:13:25', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(42, '2015-10-19 13:13:26', 'PHP', 'get_session.php', 'No access found', NULL),
(43, '2015-10-19 13:13:49', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(44, '2015-10-19 13:13:50', 'PHP', 'get_session.php', 'No access found', NULL),
(45, '2015-10-19 13:13:58', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(46, '2015-10-19 13:13:58', 'PHP', 'get_session.php', 'No access found', NULL),
(47, '2015-10-19 13:14:47', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(48, '2015-10-19 13:14:47', 'PHP', 'get_session.php', 'No access found', NULL),
(49, '2015-10-19 13:15:46', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(50, '2015-10-19 13:15:46', 'PHP', 'get_session.php', 'No access found', NULL),
(51, '2015-10-19 13:16:00', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(52, '2015-10-19 13:16:00', 'PHP', 'get_session.php', 'No access found', NULL),
(53, '2015-10-19 13:16:49', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(54, '2015-10-19 13:19:12', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(55, '2015-10-19 13:22:09', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(56, '2015-10-19 13:22:48', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(57, '2015-10-19 13:30:41', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(58, '2015-10-19 13:31:24', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(59, '2015-10-19 13:33:30', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(60, '2015-10-19 13:37:24', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(61, '2015-10-19 13:39:05', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(62, '2015-10-19 13:51:51', 'PHP', 'get_session.php', 'No access found', NULL),
(63, '2015-10-19 16:50:49', 'PHP', 'get_session.php', 'No access found', NULL),
(64, '2015-10-19 19:49:24', 'PHP', 'get_session.php', 'No access found', NULL),
(65, '2015-10-19 19:51:58', 'PHP', 'get_session.php', 'No access found', NULL),
(66, '2015-10-19 19:52:44', 'PHP', 'get_session.php', 'No access found', NULL),
(67, '2015-10-19 20:13:12', 'PHP', 'init_user.php', 'No user found.', NULL),
(68, '2015-10-19 20:14:43', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(69, '2015-10-19 20:14:56', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(70, '2015-10-22 16:52:40', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(71, '2015-10-22 19:49:45', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(72, '2015-10-22 19:52:26', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(73, '2015-10-23 16:27:49', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(74, '2015-10-23 16:37:47', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(75, '2015-10-23 16:43:47', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(76, '2015-10-24 10:58:46', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(77, '2015-10-24 11:43:03', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(78, '2015-10-24 11:50:04', 'PHP', 'get_user.php', 'No user found - Part 1', NULL),
(79, '2015-10-24 11:55:52', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(80, '2015-10-24 12:16:46', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(81, '2015-10-24 13:13:15', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(82, '2015-10-24 13:37:04', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(83, '2015-10-24 19:23:28', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(84, '2015-10-24 19:25:25', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(85, '2015-10-25 16:44:26', 'PHP', 'create_artist_img.php', 'Access Token is Missing!', NULL),
(86, '2016-01-12 15:12:25', 'PHP', 'admin_delete_artist.php', 'Not able to find or delete the artist', 2),
(87, '2016-01-12 16:13:30', 'PHP', 'admin_delete_artist.php', 'Not able to find or delete the artist', 2),
(88, '2016-01-12 16:18:45', 'PHP', 'admin_delete_artist.php', 'Not able to find or delete the artist', 2),
(89, '2016-01-12 16:21:36', 'PHP', 'admin_delete_artist.php', 'Not able to find or delete the artist', 2),
(90, '2016-01-12 16:31:48', 'PHP', 'admin_delete_artist.php', 'Not able to find or delete the artist', 2);

-- --------------------------------------------------------

--
-- Table structure for table `frame`
--

CREATE TABLE IF NOT EXISTS `frame` (
  `id` int(11) NOT NULL,
  `fin_angle_tl` varchar(45) NOT NULL DEFAULT '255255255',
  `fin_angle_tr` varchar(45) NOT NULL DEFAULT '255255255',
  `fin_angle_br` varchar(45) NOT NULL DEFAULT '255255255',
  `fin_angle_bl` varchar(45) NOT NULL DEFAULT '255255255',
  `fin_side_top` varchar(3000) NOT NULL DEFAULT '255255255',
  `fin_side_right` varchar(3000) NOT NULL DEFAULT '255255255',
  `fin_side_bottom` varchar(3000) NOT NULL DEFAULT '255255255',
  `fin_side_left` varchar(3000) NOT NULL DEFAULT '255255255',
  `selected` tinyint(4) NOT NULL DEFAULT '0',
  `artwork_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `location` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `city`, `location`) VALUES
(1, 'Kolly Gallery', 'Republic of the Congo', 'Switzerland');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `session_id` int(11) NOT NULL,
  `time_of_creation` datetime NOT NULL,
  `time_of_terminaison` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`session_id`, `time_of_creation`, `time_of_terminaison`) VALUES
(7, '2015-04-17 17:15:59', '0000-00-00 00:00:00'),
(7, '2015-04-17 18:22:19', '0000-00-00 00:00:00'),
(7, '2015-04-20 11:54:10', '0000-00-00 00:00:00'),
(7, '2015-04-20 11:54:52', '0000-00-00 00:00:00'),
(7, '2015-04-22 14:36:44', '0000-00-00 00:00:00'),
(7, '2015-05-14 16:34:43', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:30:27', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:30:42', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:32:54', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:33:24', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:35:27', '0000-00-00 00:00:00'),
(7, '2015-05-17 15:36:03', '0000-00-00 00:00:00'),
(7, '2015-05-18 19:05:58', '0000-00-00 00:00:00'),
(7, '2015-05-18 21:10:27', '0000-00-00 00:00:00'),
(7, '2015-05-18 21:13:40', '0000-00-00 00:00:00'),
(7, '2015-05-18 21:28:21', '0000-00-00 00:00:00'),
(7, '2015-05-21 09:59:20', '0000-00-00 00:00:00'),
(7, '2015-05-21 09:59:37', '0000-00-00 00:00:00'),
(7, '2015-05-21 10:06:19', '0000-00-00 00:00:00'),
(7, '2015-05-25 14:02:42', '0000-00-00 00:00:00'),
(7, '2015-05-25 14:31:06', '0000-00-00 00:00:00'),
(7, '2015-05-25 14:44:49', '0000-00-00 00:00:00'),
(7, '2015-05-25 17:39:58', '0000-00-00 00:00:00'),
(7, '2015-05-25 21:04:16', '0000-00-00 00:00:00'),
(7, '2015-05-25 21:05:40', '0000-00-00 00:00:00'),
(7, '2015-06-18 09:21:42', '0000-00-00 00:00:00'),
(7, '2015-06-19 14:43:48', '0000-00-00 00:00:00'),
(7, '2015-06-19 14:47:01', '0000-00-00 00:00:00'),
(7, '2015-06-19 14:50:26', '0000-00-00 00:00:00'),
(7, '2015-06-19 14:57:13', '0000-00-00 00:00:00'),
(7, '2015-06-19 14:59:49', '0000-00-00 00:00:00'),
(7, '2015-06-19 15:18:02', '0000-00-00 00:00:00'),
(7, '2015-06-19 15:22:39', '0000-00-00 00:00:00'),
(7, '2015-06-19 15:23:16', '0000-00-00 00:00:00'),
(7, '2015-06-26 18:32:52', '0000-00-00 00:00:00'),
(7, '2015-06-27 11:52:46', '0000-00-00 00:00:00'),
(7, '2015-06-27 11:53:19', '0000-00-00 00:00:00'),
(7, '2015-07-06 11:59:46', '0000-00-00 00:00:00'),
(7, '2015-07-13 13:43:16', '0000-00-00 00:00:00'),
(7, '2015-07-17 21:25:57', '0000-00-00 00:00:00'),
(7, '2015-07-20 11:13:45', '0000-00-00 00:00:00'),
(7, '2015-07-20 14:26:01', '0000-00-00 00:00:00'),
(7, '2015-07-20 14:48:09', '0000-00-00 00:00:00'),
(7, '2015-07-20 14:55:12', '0000-00-00 00:00:00'),
(7, '2015-07-20 15:20:53', '0000-00-00 00:00:00'),
(7, '2015-07-20 16:24:59', '0000-00-00 00:00:00'),
(7, '2015-07-20 16:31:30', '0000-00-00 00:00:00'),
(7, '2015-10-01 13:40:13', '0000-00-00 00:00:00'),
(7, '2015-10-19 11:45:21', '0000-00-00 00:00:00'),
(7, '2015-10-19 11:45:22', '0000-00-00 00:00:00'),
(7, '2015-10-19 12:35:49', '0000-00-00 00:00:00'),
(7, '2015-10-19 15:39:18', '0000-00-00 00:00:00'),
(7, '2015-10-19 15:51:50', '0000-00-00 00:00:00'),
(7, '2015-10-19 18:50:49', '0000-00-00 00:00:00'),
(7, '2015-10-19 21:49:23', '0000-00-00 00:00:00'),
(7, '2015-10-19 21:51:58', '0000-00-00 00:00:00'),
(7, '2015-10-19 21:52:44', '0000-00-00 00:00:00'),
(7, '2015-10-19 21:54:21', '0000-00-00 00:00:00'),
(7, '2015-10-19 22:02:33', '0000-00-00 00:00:00'),
(7, '2015-10-19 22:06:34', '0000-00-00 00:00:00'),
(7, '2015-10-19 22:08:31', '0000-00-00 00:00:00'),
(7, '2015-10-19 22:11:12', '0000-00-00 00:00:00'),
(7, '2015-10-19 22:15:05', '0000-00-00 00:00:00'),
(7, '2015-10-21 17:56:40', '0000-00-00 00:00:00'),
(7, '2015-10-24 13:43:15', '0000-00-00 00:00:00'),
(7, '2015-10-24 13:50:40', '0000-00-00 00:00:00'),
(7, '2015-10-27 18:15:50', '0000-00-00 00:00:00'),
(7, '2015-10-28 17:45:05', '0000-00-00 00:00:00'),
(7, '2015-11-11 21:22:23', '0000-00-00 00:00:00'),
(7, '2015-11-12 10:21:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `info_for_user`
--

CREATE TABLE IF NOT EXISTS `info_for_user` (
  `user_id` int(11) NOT NULL,
  `info_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `id` int(50) NOT NULL,
  `time` datetime NOT NULL,
  `message` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`id`, `time`, `message`) VALUES
(1, '0000-00-00 00:00:00', 'Some newssss'),
(2, '0000-00-00 00:00:00', 'Ohhh so many newsss');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `ref` int(11) NOT NULL DEFAULT '500'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `name`, `ref`) VALUES
(1, 'super', 1000),
(2, 'client', 500);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `user_id`) VALUES
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL,
  `resolutionPx_horizon` double NOT NULL,
  `resolutionPx_vertical` double NOT NULL,
  `projectionCm_horizon` double NOT NULL,
  `projectionCm_vertical` double NOT NULL,
  `auto_slideshow_enable` int(1) NOT NULL DEFAULT '0',
  `auto_slideshow_time` int(2) NOT NULL DEFAULT '1',
  `scale_real_enable` int(1) NOT NULL DEFAULT '0',
  `selection_onCountdown_enable` int(1) NOT NULL DEFAULT '0',
  `selection_onCountdown_time` int(2) NOT NULL DEFAULT '5',
  `frame_section_enable` int(1) NOT NULL DEFAULT '1',
  `allowed_diskspace_go` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `resolutionPx_horizon`, `resolutionPx_vertical`, `projectionCm_horizon`, `projectionCm_vertical`, `auto_slideshow_enable`, `auto_slideshow_time`, `scale_real_enable`, `selection_onCountdown_enable`, `selection_onCountdown_time`, `frame_section_enable`, `allowed_diskspace_go`) VALUES
(1, 1920, 1080, 52, 29, 0, 1000, 1, 0, 7, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `settings_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `username`, `password`, `settings_id`, `section_id`, `gallery_id`, `exclude`) VALUES
(2, 'Robin Jespierre', 'robin.jespierre@yahoo.fr', 'robinje', 'e587f6146ebfbdefdc028c591643f220', 1, 1, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_token`
--
ALTER TABLE `access_token`
  ADD PRIMARY KEY (`token`),
  ADD UNIQUE KEY `token_UNIQUE` (`token`),
  ADD UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  ADD KEY `accessToken_userId_idx` (`user_id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `user_id_idx` (`gallery_id`),
  ADD KEY `artist_gallery_id_idx` (`gallery_id`);

--
-- Indexes for table `artwork`
--
ALTER TABLE `artwork`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `artwork_artist_id_idx` (`artist_id`);

--
-- Indexes for table `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `error`
--
ALTER TABLE `error`
  ADD PRIMARY KEY (`id`),
  ADD KEY `error_user_id_idx` (`user_id`);

--
-- Indexes for table `frame`
--
ALTER TABLE `frame`
  ADD PRIMARY KEY (`id`),
  ADD KEY `artwork_idx` (`artwork_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`session_id`,`time_of_creation`),
  ADD KEY `history_session_id_idx` (`session_id`);

--
-- Indexes for table `info_for_user`
--
ALTER TABLE `info_for_user`
  ADD PRIMARY KEY (`info_id`,`user_id`),
  ADD KEY `gallery_id_idx` (`user_id`),
  ADD KEY `info_for_gallery_info_id_idx` (`info_id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_UNIQUE` (`ref`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_user_id_idx` (`user_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `configuration_id_idx` (`settings_id`),
  ADD KEY `option_id_idx` (`settings_id`),
  ADD KEY `group_id_idx` (`section_id`),
  ADD KEY `section_id_idx` (`section_id`),
  ADD KEY `gallery_id_idx` (`gallery_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `artwork`
--
ALTER TABLE `artwork`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `control`
--
ALTER TABLE `control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `error`
--
ALTER TABLE `error`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `frame`
--
ALTER TABLE `frame`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `access_token`
--
ALTER TABLE `access_token`
  ADD CONSTRAINT `accessToken_userId` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `artist`
--
ALTER TABLE `artist`
  ADD CONSTRAINT `artist_gallery_id` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `artwork`
--
ALTER TABLE `artwork`
  ADD CONSTRAINT `artwork_artist_id` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `error`
--
ALTER TABLE `error`
  ADD CONSTRAINT `error_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `info_for_user`
--
ALTER TABLE `info_for_user`
  ADD CONSTRAINT `info_for_gallery_info_id` FOREIGN KEY (`info_id`) REFERENCES `information` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `info_for_gallery_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `gallery_id` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `option_id` FOREIGN KEY (`settings_id`) REFERENCES `setting` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
