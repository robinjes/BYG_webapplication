<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="fr"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Beyon your gallery - Error 404</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="NOINDEX, NOFOLLOW, noimageclick" />
    </head>
    <body style="max-width: 800px; margin: 7% auto 0;">
        <a href="http://beyondyourgallery.com/">
        	<img src="http://beyondyourgallery.com/img/image404.jpg" alt="Olfraime - erreur 404" style="text-align:center; margin-top:10%; margin-left:auto;margin-right:auto; width:100%; max-width:788px"/>
        </a>
        <a href="mailto:contact@beyondyourgallery.com?Subject=Error" target="_top">Contact</a>
    </body>
</html>