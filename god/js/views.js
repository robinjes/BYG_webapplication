Gallery.MainView = Ember.View.extend({
	templateName: 'main',
	rightLogin: "Congratulation! You successfully logged!",
	wrongLogin: "Wrong username or password. Try again!"
});

Gallery.GodErrorView = Ember.View.extend({
	templateName: 'god_error',
	didInsertElement: function(){
		setErrorPart_Sizes();
	}
});

Gallery.GodControlView = Ember.View.extend({
	templateName: 'god_control',
	didInsertElement: function(){
		setControlPart_Sizes();
	}
});