/*#################*/
/* Get all errors */
/*#################*/
function getAllErrors(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_errors.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_errors_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*#################*/
/* Get all settings */
/*#################*/
function getAllSettings(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_options.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_settings_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*############*/
/* Check user */
/*############*/
function initUser(usernameoremail,password){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/init_user.php",
        type: "POST",
        data: {'usernameoremail':usernameoremail, 'password':password},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: initUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: initUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Get a user */
/*############*/
function getUser(access_token){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/get_user.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Set a user */
/*############*/
function setUser(access_token,name,username,gallery_name,gallery_location){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/set_user.php",
        type: "POST",
        data: {'access_token':access_token,'name':name,'username':username,'gallery_name':gallery_name,'gallery_location':gallery_location},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setUser()');
            }
        }
    });
    return user;   
}
/*###############*/
/* Get a session */
/*###############*/
function getSession(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_session.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_session_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);  
}
/*#################*/
/* Get control */
/*#################*/
function getControl(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_control.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_control_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*#####################*/
/* Get all information */
/*#####################*/
function getAllInformation(access_token){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/get_information.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getAllInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getAllInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Add a new information */
/*#######################*/
function deleteInformation(access_token,info_id){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/delete_information.php",
        type: "POST",
        data: {'access_token':access_token,'info_id':info_id},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: deleteInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: deleteInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Remove an information */
/*#######################*/
function addInformation(access_token,message,users_ids){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/create_information.php",
        type: "POST",
        data: {'access_token':access_token,'message':message,'message':message},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addInformation()');
            }
        }
    });
    return infos;   
}
/*####################################*/
/* Set Control - Frame Section Enable */
/*####################################*/
function setControl_FrameSection(access_token,serverpath,value){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/set_control_frameSectionEnable.php";
    var vars = "access_token="+access_token+"value="+value;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            confirm_property_modified();
        }else{
            property_modification_failed();
        }
    }
    hr.send(vars);
}
/*################*/
/* Remove a token */
/*################*/
function removeTokenInDB(access_token){
    $.ajax({
        url: window.serverpath+"/store/delete_token.php",
        type: "POST",
        data: {'access_token':access_token},
        async: false,
    });
}