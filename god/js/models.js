//Artists
Gallery.Error = DS.Model.extend({
	reference: DS.attr('string'),
	source_file: DS.attr('string'),
  	time: DS.attr('string'),
  	message: DS.attr('string'),
  	username: DS.attr('string'),
  	gallery_name: DS.attr('string')
});
//Control
Gallery.Control = DS.Model.extend({
  frame_section_enable: DS.attr('string'),
  frame_section_on: function() {
  	if((this.get('frame_section_enable') == 0)||(this.get('frame_section_enable') == '0')){
  		return false;
  	}else{
  		return true;
  	}
  }.property('frame_section_on'),
  artworks_tag_enable: DS.attr('string'),
  artworks_tag_on: function() {
    if((this.get('artworks_tag_enable') == 0)||(this.get('artworks_tag_enable') == '0')){
      return false;
    }else{
      return true;
    }
  }.property('frame_section_on')
});
//Session
Gallery.Session = DS.Model.extend({
  	username: DS.attr('string')
});