/*####  Worker for loading the gallery & Login/Logout ####*/
importScripts('crud.js');
var serverpath;

onmessage = function (oEvent) {
    serverpath = oEvent.data.serverpath;
    load_session(oEvent.data.access_token);
};


/*******************************/
function load_session(access_token){
    postMessage({'message':'downloading_session'});
    var session = getSession(access_token,serverpath);
}
function load_session_inApp(access_token,session_data){
    postMessage({'message':'session_downloaded','session':session_data});
    load_errors(access_token);
}

function load_errors(access_token){
	postMessage({'message':'downloading_errors'});
    var errors = getAllErrors(access_token,serverpath);
}
function load_errors_inApp(access_token,errors_data){
    postMessage({'message':'errors_downloaded','errors':errors_data});
    load_control(access_token);
}

function load_control(access_token){
	postMessage({'message':'downloading_control'});
    var control = getControl(access_token,serverpath);
}
function load_control_inApp(access_token,control_data){
    postMessage({'message':'control_downloaded','control':control_data});
}