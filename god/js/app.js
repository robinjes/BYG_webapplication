window.serverpath = 'http://beyondyourgallery.com/god';
window.current_route = '';
Gallery = Ember.Application.create({
  ready: function(){
    $('.loading_container').hide();
    if(localStorage.getItem('userLogged') == "true") {
        window.access_token = localStorage.getItem('accessTok');
        var get_user_response = getUser(window.access_token);
        if((get_user_response == null) || (get_user_response == '')){
            localStorage.setItem('userLogged', false);
            localStorage.removeItem('accessTok');
            localStorage.removeItem('session');
            window.location.reload();
        }else{
            var user_data = JSON.stringify(getUser(window.access_token));
            localStorage.setItem('session', user_data);
            if (window.Worker) {
                $('.gallery_loading').animate({
                    height: $( window ).height(),
                    opacity: 1
                }, 500, function() {
                    // Animation complete.
                    $('.loader').height($( window ).height()/4);
                    $('.gallery_loading').css('display','block');
                });
                window.var_start = new Date();
                var worker_loadGallery = new Worker("js/worker_connection.js");
                worker_loadGallery.postMessage({'access_token':window.access_token,'serverpath':window.serverpath}); 
                worker_loadGallery.onmessage = function (oEvent) {
                    switch(oEvent.data.message) {
                        case 'downloading_session':
                            feedback_loading_session();
                        break;
                        case 'session_downloaded':
                            if(oEvent.data.session != null){
                                Gallery.Session.reopenClass({
                                    FIXTURES: oEvent.data.session
                                });        
                            }
                            feedback_loading_session_complete();
                        break;
                        case 'downloading_errors':
                            feedback_loading_errors();
                        break;
                        case 'errors_downloaded':
                            if(oEvent.data.errors != null){
                                Gallery.Error.reopenClass({
                                    FIXTURES: oEvent.data.errors
                                });        
                            }
                            feedback_loading_errors_complete();
                        break;
                        case 'downloading_control':
                            feedback_loading_control();
                        break;
                        case 'control_downloaded':
                            if(oEvent.data.control != null){
                                Gallery.Control.reopenClass({
                                    FIXTURES: oEvent.data.control
                                });        
                            }
                            feedback_loading_control_complete();
                            feedback_loadGallery_complete();
                            worker_loadGallery.terminate();
                        break;
                    }
                };
            }else{
                console.log('Web Worker functionality not available.');
            }
        }
    }
  }
});

Gallery.ApplicationStore = DS.Store.extend({
    revision: 12,
    adapter: DS.FixtureAdapter.extend({
        queryFixtures: function(fixtures, query, type) {
            return fixtures.filter(function(item) {
                for(prop in query) {
                    if( item[prop] != query[prop]) {
                        return false;
                    }
                }
                return true;
            });
        }
    })
});

Gallery.initializer({
    name: "auth",
    before: ['ember-data'],
    initialize: function (container, application) {
        var auth = Ember.Object.extend({
            isAuthenticated: Em.computed.notEmpty('authToken')
        });
        application.register("my:authToken", auth);
        application.inject("controller", "auth", "my:authToken");
        application.inject("route", "auth", "my:authToken");
        application.inject("store:main", "auth", "my:authToken");
        application.inject("adapter", "auth", "my:authToken");
    }
});

function feedback_loading_session(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#session_loading_feedbacks").text("Downloading your session...");
    $(".loading_feedbacks_label#session_loading_feedbacks").css('opacity','1');
}
function feedback_loading_session_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#session_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#session_loading_feedbacks").text("Session loaded.");
}
function feedback_loading_errors(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#error_loading_feedbacks").text("Downloading the application errors...");
    $(".loading_feedbacks_label#error_loading_feedbacks").css('opacity','1');
}
function feedback_loading_errors_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#error_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#error_loading_feedbacks").text("Errors loaded.");
}
function feedback_loading_control(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#control_loading_feedbacks").text("Downloading the application control...");
    $(".loading_feedbacks_label#control_loading_feedbacks").css('opacity','1');
}
function feedback_loading_control_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#control_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#control_loading_feedbacks").text("Control loaded.");
}

function feedback_loadGallery_complete(){
    $('.gallery_loading').animate({
        opacity: 0
    }, 500, function() {
        $('.gallery_loading label').remove();
        $('.gallery_loading').height(0);
        $('.gallery_loading').css('display','none');
    });
    window.end = new Date();
    var secondsOpen = Math.floor((window.end - window.var_start) / 1000);
    
    var self = Gallery.__container__.lookup('controller:god_home');
    self.store.find('session').then(function(session) {
        var username = session.objectAt(0).get('username');
        self.transitionToRoute('god_home',username);
    });
}
function feedback_loadGallery_fail(){
    $('.gallery_loading').animate({
        opacity: 0
    }, 500, function() {
        $('.gallery_loading label').remove();
        $('.gallery_loading').height(0);
        $('.gallery_loading').css('display','none');
    });
    window.end = new Date();
    var secondsOpen = Math.floor((window.end - window.var_start) / 1000);
    console.log("The gallery failed to load after " + secondsOpen + " seconds of attempt.");
}