window.popup_height = 30;

console.log($('#checkbox_frame_section_enable'));
  $('#checkbox_frame_section_enable').change(function(){
    alert('000000000000000000000');
    var worker_control = new Worker("js/worker_control.js");
    worker_control.postMessage({'access_token':window.access_token,'serverpath':window.serverpath,'message':'change_frame_section_enable','newValue':$(this).is(':checked')});
    worker_control.onmessage = function (oEvent) {
      switch(oEvent.data.message) {
          case 'property_modified':
              worker_control.terminate();
          break;
          case 'property_modification_failed':
              showError('I-000','Property modification failed','god.js: $(document).ready()');  
              worker_control.terminate();
          break;
      }
    }
  });
/*########################*/
/* When document is ready */
/*########################*/
$( document ).ready(function() {
	setAllPart_sizes();
});
$( window ).resize(function() {
  setErrorPart_Sizes();
  setControlPart_Sizes();
});

function setAllPart_sizes(){

}

function setErrorPart_Sizes(){
  $('.accordion').accordion();
}

function setControlPart_Sizes(){
}

function showCloseConfirmationBox(){
	$('.veil').show();
	$('.confirmationBox#cancel_modification_artist').show();
	$('.confirmationBox#cancel_modification_artwork').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}

function showModificationConfirmationBox_user(){
	$('.veil#veil_user').show();
	$('.confirmationBox#validate_modification_user').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showCancelConfirmationBox_user(){
	$('.veil#veil_user').show();
	$('.confirmationBox#cancel_modification_user').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationBox_password(){
	$('.veil#veil_password').show();
	$('.confirmationBox#changePassword').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationConfirmationBox_password(){
	$('.veil#veil_password').show();
	$('.confirmationBox#validate_modification_password').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}

function setVisualEffects(){
  /*Switch*/
  var switch_component = $('.switch');
  switch_component.each(function( index ) {
      var switch_value = $(this).val();
    if(switch_value == '0'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(switch_value == '1'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }
  });
}
function showModifySettingBox(field_cat,field_title,field_value,field_unit){
  $('.veil').show();
  $('.confirmationBox#modification_setting .confirm_message').append(field_title);
  var componentToImplement = '';
  switch(field_cat){
    case 'text': 
      componentToImplement = '<input type="text" name="value_to_submit" value="'+field_value+'" />';
      break;
    case 'number': 
      componentToImplement = '<input type="number" name="value_to_submit" pattern= "[0-9]" value="'+field_value+'" />';
      break;
    case 'switch': 
      componentToImplement = '<input type="text" class="modify_switch" name="value_to_submit" value="'+field_value+'" readonly="readonly" />';
      break;
    case 'multiple': 
      componentToImplement = '<input type="text" name="value_to_submit" value="'+field_value+'" />';
      break;
  }
  $('.confirmationBox#modification_setting .confirm_data').empty();
  $('.confirmationBox#modification_setting .confirm_data').append(componentToImplement);

  /*Modify Part Switch*/
  var modify_switch_component = $('.modify_switch');
  modify_switch_component.each(function( index ) {
    var modify_switch_value = $(this).val();
    if(modify_switch_value == '0'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(modify_switch_value == '1'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }
  });
  modify_switch_component.click(function() {
    var modify_switch_value = $(this).val();
    if(modify_switch_value == 'ON'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(modify_switch_value == 'OFF'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }  
  });

  $('.confirmationBox#modification_setting').show();

  $(document).keydown(function(e){
    // Escape key
      if (e.keyCode == 27) {
          hideConfirmationBox();
      }
  });
}

function hideConfirmationBox(){
  	$('.veil').hide();
	$('.confirmationBox').hide();
}

function unRegisterAllEventListeners(obj) {
	if ( typeof obj._eventListeners == 'undefined' || obj._eventListeners.length == 0 ) {
		return;	
	}
	
	for(var i = 0, len = obj._eventListeners.length; i < len; i++) {
		var e = obj._eventListeners[i];
		obj.removeEventListener(e.event, e.callback);
	}
 
	obj._eventListeners = [];
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function showError(error_ref,error_message,error_source){
  window.popupQty_error = $('.god_error_popup').length;
  window.popupQty_info = $('.god_info_popup').length;
  var popupQty_total = window.popupQty_error + window.popupQty_info;
  var error_message = error_message.replace("<br>", "");  
  if(error_source == null){
    if((error_message!='') && (error_message!= null)){
      if(popupQty_total<=0){
        $('body').append('<div class="god_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
      }else{
        var error_position_bottom = popupQty_total * window.popup_height;
        $('body').append('<div class="god_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
      }   
    }else{
      if(popupQty_total<=0){
        $('body').append('<div class="god_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_ref + '</p></div>');
      }else{
        var error_position_bottom = popupQty_total * window.popup_height;
        $('body').append('<div class="god_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_ref + '</p></div>');
      }     
    }
  }else{
    if((error_message!='') && (error_message!= null)){
      if(popupQty_total<=0){
        $('body').append('<div class="god_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
      }else{
        var error_position_bottom = popupQty_total * window.popup_height;
        $('body').append('<div class="god_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
      }   
    }else{
      if(popupQty_total<=0){
        $('body').append('<div class="god_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>Error ' + error_ref + '</p></div>');
      }else{
        var error_position_bottom = popupQty_total * window.popup_height;
        $('body').append('<div class="god_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>Error ' + error_ref + '</p></div>');
      }     
    }
  }
}