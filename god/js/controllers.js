Gallery.MainController = Ember.Controller.extend({
  needs: 'login'
});

Gallery.GodHomeController = Ember.Controller.extend({
  controllerName: 'god_home',
  needs: 'login',
  userLogged: function(){
    var value = false;
    if(localStorage.getItem('userLogged') == 'true'){
      value = true;
    }else{
      value = false;
    }
    return value;
  }.property('userLogged'),
  session: function(){
    var token = localStorage.getItem('accessTok');
    var user_data = getUser(token);
    localStorage.setItem('session', JSON.stringify(user_data));
    return user_data;
  }.property('session'),
  information: function(){
    var token = localStorage.getItem('accessTok');
    var user_information = getAllInformation(token);
    localStorage.setItem('information', JSON.stringify(user_information));
    return user_information;
  }.property('information'),
  actions: {
    close_information: function(info_id){
      var token = localStorage.getItem('accessTok');
      closeInfoPopup(info_id);
      deleteInformation(token,info_id);
    }
  }
});

Gallery.LoginController = Ember.Controller.extend({
  controllerName: 'login',
  needs: ['application'],
  actions: {
    launch_identification: function(usernameoremail, password){
      if(usernameoremail==null){usernameoremail=''};
      if(password==null){password=''};
      this.send('login',usernameoremail, password);
    },
    login: function(usernameoremail, password) {
      if(usernameoremail==null){usernameoremail=''};
      if(password==null){password=''};

      if (window.Worker) {
        $('.gallery_loading').animate({
            height: $( window ).height(),
            opacity: 1
        }, 500, function() {
            // Animation complete.
            $('.loader').height($( window ).height()/4);
            $('.gallery_loading').css('display','block');
        });
        var returnValue = initUser(usernameoremail,password);
        if(returnValue == null){
          feedback_loadGallery_fail();
          this.transitionToRoute('admin_home_unlogged');
          showError('007','Wrong username or password. Try again.','LoginController: login action');
        }else{
          if(typeof(Storage) !== 'undefined') {
            localStorage.setItem('userLogged', true);
            localStorage.setItem('accessTok', returnValue.accessTok);
            var user_data = JSON.stringify(getUser(returnValue.accessTok));
            localStorage.setItem('session', user_data);
          } else {
            //Use cookies
            alert('Sorry! No Web Storage support...');
          }
          var access_token = localStorage.getItem('accessTok');
          window.var_start = new Date();
          var worker_controller_loadGallery = new Worker("js/worker_connection.js");
          worker_controller_loadGallery.postMessage({'serverpath':window.serverpath,'access_token':access_token});
          var self = this;
          worker_controller_loadGallery.onmessage = function (oEvent) {
            switch(oEvent.data.message) {
                case 'downloading_session':
                    feedback_loading_session();
                break;
                case 'session_downloaded':
                    if(oEvent.data.session != null){
                        Gallery.Session.reopenClass({
                            FIXTURES: oEvent.data.session
                        });        
                    }
                    feedback_loading_session_complete();
                break;
                case 'downloading_errors':
                    feedback_loading_errors();
                break;
                case 'errors_downloaded':
                    if(oEvent.data.error != null){
                        Gallery.Error.reopenClass({
                            FIXTURES: oEvent.data.error
                        });        
                    }
                    feedback_loading_error_complete();
                break;
                case 'downloading_control':
                    feedback_loading_control();
                break;
                case 'control_downloaded':
                    if(oEvent.data.control != null){
                        Gallery.Control.reopenClass({
                            FIXTURES: oEvent.data.control
                        });        
                    }
                    feedback_loading_control_complete();
                    feedback_loadGallery_complete();
                    worker_loadGallery.terminate();
                break;
            }
          };
        }
      }
    },
    logout: function(concernedController) {
      var access_token = localStorage.getItem('accessTok');
      removeTokenInDB(access_token);
      localStorage.setItem('userLogged', false);
      localStorage.removeItem('accessTok');
      localStorage.removeItem('session');
      window.location.replace("http://beyondyourgallery.com/god");
    }
  }
});

Gallery.GodErrorController = Ember.ArrayController.extend({
  controllerName: 'god_error',
  filter: '',
  filteredContent: function(){
    var filter = this.get('filter');
    var rx = new RegExp(filter, 'gi');
    var errors = this.get('arrangedContent');

    return errors.filter(function(error) {
      return error.get('gallery_name').match(rx);
    });
  }.property('arrangedContent', 'filter'),
  actions: {
    sortBy: function(property) {
      this.set('sortProperties', [property]);
      this.set('sortDescending', !this.get('sortDescending'));
      $('.accordion').accordion( "refresh" );
    }
  }
});