console.log('APPLICATION IS LOADING');

Gallery.Router.map(function() {
  this.resource('main', { path: '/'}, function() {
    this.resource('god_home_unlogged', { path: '/home' });
  });

  this.resource('god_home', { path: '/:username'}, function() {
    this.resource('god_error', { path: "/errors" });
    this.resource('god_control', { path: "/control" });
    this.resource('missing', { path: "/*path" });
    this.resource('loading_logged', { path: '/loading' });
  });
});

/*##################### GOD'S OFFICE #####################*/
Gallery.MainRoute = Ember.Route.extend({
  controllerName: 'main',
  beforeModel : function(){
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('god_home_unlogged');
    }else{
      var self = this;
      this.store.find('session').then(function(sessions) {
        var username = sessions.objectAt(0).get('username');
        self.transitionTo('god_home',username);
      });
    }
  }
});

Gallery.GodHomeUnloggedRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      
    }else{
      var self = this;
      this.store.find('session').then(function(sessions) {
        var username = sessions.objectAt(0).get('username');
        self.transitionTo('god_home',username);
      });
    }
  }
});

Gallery.GodHomeRoute = Ember.Route.extend({
  controllerName: 'god_home',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  },
  model: function() {
    return this.store.find('session').objectAt(0);
  },
  actions: {
    error: function () {
      console.log('Error with data');
    }
  }
});

Gallery.GodErrorRoute = Ember.Route.extend({
  controllerName: 'god_error',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  },
  model: function() {
    return this.store.find('error');
  },
  actions: {
    error: function () {
      console.log('Error with data');
    }
  }
});

Gallery.GodControlRoute = Ember.Route.extend({
  controllerName: 'god_control',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  },
  model: function() {
    return this.store.find('control');
  },
  actions: {
    error: function () {
      console.log('Error with data');
    }
  }
});

Gallery.MissingRoute = Ember.Route.extend({
  controllerName: 'missing'
});