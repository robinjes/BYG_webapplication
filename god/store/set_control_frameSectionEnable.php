<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 			= parse_ini_file("../properties.ini");

	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 			= $ini_array['db_prefix'];
	$db_ip 				= $ini_array['db_ip'];
	$db_name 			= $ini_array['db_name'];
	$db_username 		= $ini_array['db_username'];
	$db_password 		= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Set the control property: Frame section Enable
	$value = $_POST['value'];
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','set_control_frameSectionEnable.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','set_control_frameSectionEnable.php','No user found',$db);
		die('No user found');
	}

	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_control_frameSectionEnable.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$sectionId = $user_row['section_id'];

	$section = mysql_query("select * from section where id = '" . $sectionId . "'" , $db);
	if((!$section)||(mysql_num_rows($section)<=0)){
		create_error($access_token,'PHP','set_control_frameSectionEnable.php','No section found',$db);
		die('No section found');
	}
	$section_row = mysql_fetch_array($section, MYSQL_ASSOC);
	$sectionRef = $section_row['ref'];


	if(($sectionRef==1000)||($sectionRef=='1000')){
		$control_query = mysql_query("select * from control"); 
		if((!$control_query)||(mysql_num_rows($control_query)<=0)){
			create_error($access_token,'PHP','set_control_frameSectionEnable.php','No control found',$db);
			die('No control found');
		}

		$control_update = mysql_query("update control set frame_section_enable='" . $value ."'" , $db);
		if(!$control_update){
			create_error($access_token,'PHP','set_control_frameSectionEnable.php','Not able to update the control property',$db);
		  	die('Not able to update the control property');
		}
	}else{
		die('Not allowed to modify this property');
	}
	//Close the database connection
	mysql_close($db);