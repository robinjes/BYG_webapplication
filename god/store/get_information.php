<?php
	header('Content-Type: application/json; charset=utf-8');
	
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Get all information
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','get_information.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','get_information.php','No user found',$db);
		die('No user found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$infoForGallery = mysql_query("select * from info_for_user inner join information on info_for_user.info_id = information.id where user_id = '" . $userId . "'", $db);

	$json_response_infos = array();
	while ($row_infoForGallery = mysql_fetch_array($infoForGallery, MYSQL_ASSOC)) {
	    $row_array['id'] = $row_infoForGallery['info_id'];
	    $row_array['time'] = $row_infoForGallery['time'];
	    $row_array['message'] = $row_infoForGallery['message'];
	    array_push($json_response_infos,$row_array);
	}

    echo json_encode($json_response_infos);
	//Close the database connection
	mysql_close($db);