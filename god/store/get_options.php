<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 			= parse_ini_file("../properties.ini");

	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 			= $ini_array['db_prefix'];
	$db_ip 				= $ini_array['db_ip'];
	$db_name 			= $ini_array['db_name'];
	$db_username 		= $ini_array['db_username'];
	$db_password 		= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Get the setting for the gallery
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','get_options.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$row_options = array();

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','get_options.php','No user found',$db);
		die('No access found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$session = mysql_query("select * from session where user_id = '" . $userId . "'" , $db);
	if((!$session)||(mysql_num_rows($session)<=0)){
		create_error($access_token,'PHP','get_options.php','No session found',$db);
		die('No session found');
	}
	$session_row = mysql_fetch_array($session, MYSQL_ASSOC);
	$row_options['session'] = $session_row['id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','get_options.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$galleryId = $user_row['gallery_id'];
	$optionId = $user_row['settings_id'];	

	$default_artist_query = mysql_query("select MAX(id) as artist_max from artist where gallery_id='" . $galleryId ."'"); 
	if((!$default_artist_query)){
		create_error($access_token,'PHP','get_options.php','No artist_id found',$db);
		die('No artist_id found');
	}
	$artist_row = mysql_fetch_array($default_artist_query, MYSQL_ASSOC);
	$row_options['currentArtist'] = $artist_row['artist_max']; 

	$default_artwork_query = mysql_query("select MAX(id) as artwork_max from artwork where artist_id='" . $row_options['currentArtist'] ."'"); 
	if((!$default_artwork_query)){
		create_error($access_token,'PHP','get_options.php','No artwork_id found',$db);
		die('No artwork_id found');
	}
	$artwork_row = mysql_fetch_array($default_artwork_query, MYSQL_ASSOC);
	$row_options['currentArtwork'] = $artwork_row['artwork_max']; 

	$setting_query = mysql_query("select * from setting where id='" . $optionId ."'"); 
	if((!$setting_query)||(mysql_num_rows($setting_query)<=0)){
		create_error($access_token,'PHP','get_options.php','No setting found',$db);
		die('No setting found');
	}
	$setting_row = mysql_fetch_array($setting_query, MYSQL_ASSOC);
	$row_options['id'] = $setting_row['id']; 
	$row_options['resolutionPx_horizon'] = $setting_row['resolutionPx_horizon'];
	$row_options['resolutionPx_vertical'] = $setting_row['resolutionPx_vertical']; 
	$row_options['projectionCm_horizon'] = $setting_row['projectionCm_horizon']; 
	$row_options['projectionCm_vertical'] = $setting_row['projectionCm_vertical']; 
	$row_options['directory_allowed_space'] = $setting_row['allowed_diskspace_go'];
	$row_options['auto_slideshow_activate'] = $setting_row['auto_slideshow_activate']; 
	$row_options['auto_slideshow_time'] = $setting_row['auto_slideshow_time']; 
	$row_options['scale_real_activate'] = $setting_row['scale_real_activate'];
	$row_options['selection_countdown_time'] = $setting_row['selection_countdown_time']; 

    $galleryDirectory = 'gallery_' . $galleryId . '/';

    $path = $destination_folder . $galleryDirectory;
    $ar = getDirectorySize($path);

    $row_options['directory_used_space'] = sizeFormat($ar['size']);

    $json_response_options = array();
    array_push($json_response_options,$row_options);

    echo json_encode($json_response_options);

	function getDirectorySize($path){ 
	  $totalsize = 0; 
	  $totalcount = 0; 
	  $dircount = 0; 
	  if ($handle = opendir ($path)) 
	  { 
	    while (false !== ($file = readdir($handle))) 
	    { 
	      $nextpath = $path . '/' . $file; 
	      if ($file != '.' && $file != '..' && !is_link ($nextpath)) 
	      { 
	        if (is_dir ($nextpath)) 
	        { 
	          $dircount++; 
	          $result = getDirectorySize($nextpath); 
	          $totalsize += $result['size']; 
	          $totalcount += $result['count']; 
	          $dircount += $result['dircount']; 
	        } 
	        elseif (is_file ($nextpath)) 
	        { 
	          $totalsize += filesize ($nextpath); 
	          $totalcount++; 
	        } 
	      } 
	    } 
	  } 
	  closedir ($handle); 
	  $total['size'] = $totalsize; 
	  $total['count'] = $totalcount; 
	  $total['dircount'] = $dircount; 
	  return $total; 
	} 

	function sizeFormat($size){ 
	    /*if($size<1024) 
	    { 
	        return $size." bytes"; 
	    } 
	    else if($size<(1024*1024)) 
	    { 
	        $size=round($size/1024,1); 
	        return $size." KB"; 
	    } 
	    else if($size<(1024*1024*1024)) 
	    { 
	        $size=round($size/(1024*1024),1); 
	        return $size." MB"; 
	    } 
	    else 
	    { */
	        $size=round($size / (1024*1024*1024),2); 
	        return $size; 
	    /*} */
	}

	//Close the database connection
	mysql_close($db);