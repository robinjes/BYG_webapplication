<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$destination_folder     = $ini_array['datas_folder_path'];
	$root_directory 		= $ini_array['root_directory'];

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Get all artworks
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','get_artworks.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','get_artworks.php','No access found',$db);
		die('No access found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','get_artworks.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$galleryId = $user_row['gallery_id'];

	$galleryFolder = 'gallery_' . $galleryId . '/';
	$galleryDirectory = $destination_folder . $galleryFolder;

	$result_artworks = mysql_query("select artwork.id, artwork.title, artwork.year, artwork.dim_horizon_cm, artwork.dim_vertica_cm, artwork.description, artwork.technique,artwork.artist_id, artwork.img, artwork.display from artwork inner join artist on artwork.artist_id = artist.id where artist.gallery_id = '" . $galleryId . "'" , $db); 
	$json_response_artworks = array();
	while ($row = mysql_fetch_array($result_artworks, MYSQL_ASSOC)) {
	    $row_array['id'] = intval($row['id']);
	    $row_array['title'] = utf8_encode($row['title']);
	    $row_array['year'] = utf8_encode($row['year']);
	    $row_array['dim_horizon'] = utf8_encode($row['dim_horizon_cm']);
	    $row_array['dim_vertica'] = utf8_encode($row['dim_vertica_cm']);
	    $row_array['description'] = utf8_encode($row['description']);
	    $row_array['technique'] = utf8_encode($row['technique']);
	    $row_array['artist_id'] = intval($row['artist_id']);
	    $row_array['imgName'] = utf8_encode($row['img']);
	    $row_array['imgPath'] = getImgPath($galleryFolder,$galleryDirectory,utf8_encode($row['img']));
	    $row_array['imgOverviewPath'] = getImgOverviewPath($galleryDirectory,utf8_encode($row['img']));
	    if(($row['display'] == 1)||($row['display'] == '1')){
	    	$row_array['display'] = true;
	    }else{
	    	$row_array['display'] = false;
	    }
	    array_push($json_response_artworks,$row_array);
	}

	function getImgOverviewPath($destination_folder,$imgName){
		$imgName = $imgName . '_sm';
	    $imgPath = '';
	    if($handle = opendir($destination_folder)) {
	        while(false !== ($file = readdir($handle))){
	            if(strpos($file,$imgName) !== false){
	                $img = $destination_folder . $file;
	                $imgData = base64_encode(file_get_contents($img));
	                $imgPath = 'data: '.get_type($img).';base64,'.$imgData;
	            }
	        }
	        closedir($handle);
	    }
	    return $imgPath;
	}

	function getImgPath($galleryFolder,$destination_folder,$imgName){
	    $imgPath = '';

	    if($handle = opendir($destination_folder)) {
	        while(false !== ($file = readdir($handle))){
	            if((strpos($file,$imgName) !== false)&&(strpos($file,'_sm') === false)){
	                $img = $destination_folder . $file;
	                $imgData = base64_encode(file_get_contents($img));
	                $imgPath = 'data: '.get_type($img).';base64,'.$imgData;
	            }
	        }
	        closedir($handle);
	    }
	    return $imgPath;
	}

	function get_type($file){
	    $mime_types = array(
	        "pdf"=>"application/pdf"
	        ,"exe"=>"application/octet-stream"
	        ,"zip"=>"application/zip"
	        ,"docx"=>"application/msword"
	        ,"doc"=>"application/msword"
	        ,"xls"=>"application/vnd.ms-excel"
	        ,"ppt"=>"application/vnd.ms-powerpoint"
	        ,"gif"=>"image/gif"
	        ,"png"=>"image/png"
	        ,"jpeg"=>"image/jpg"
	        ,"jpg"=>"image/jpg"
	        ,"mp3"=>"audio/mpeg"
	        ,"wav"=>"audio/x-wav"
	        ,"mpeg"=>"video/mpeg"
	        ,"mpg"=>"video/mpeg"
	        ,"mpe"=>"video/mpeg"
	        ,"mov"=>"video/quicktime"
	        ,"avi"=>"video/x-msvideo"
	        ,"3gp"=>"video/3gpp"
	        ,"css"=>"text/css"
	        ,"jsc"=>"application/javascript"
	        ,"js"=>"application/javascript"
	        ,"php"=>"text/html"
	        ,"htm"=>"text/html"
	        ,"html"=>"text/html"
	    );
	    $extension = strtolower(end(explode('.',$file)));
	    return $mime_types[$extension];
	}

	echo json_encode($json_response_artworks);

	//Close the database connection
	mysql_close($db);