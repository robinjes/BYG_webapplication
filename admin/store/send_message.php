<?php
	header('Content-Type: application/json; charset=utf-8');
	
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];

	$admin_email 				= $ini_array['admin_email'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	$access_token = $_POST['access_token'];
	$message = $_POST['message'];
	if(!isset($access_token)){
		create_error('-','PHP','send_message.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}
	if((!isset($message))&&($message!='')){
		create_error($access_token,'PHP','send_message.php','Message is Missing!',$db);
		die('Message is Missing!');
	}
	$token = mysql_query("select * from access_token where token = '" . $access_token . "'", $db);
	if((!$token)||(mysql_num_rows($token)<=0)){
		create_error($access_token,'PHP','send_message.php','No access found',$db);
		die('No access found');
	}
	$rowToken = mysql_fetch_array($token, MYSQL_ASSOC);
	$userId = $rowToken['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'", $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','send_message.php','No user found',$db);
		die('No user found');
	}
	$rowUser = mysql_fetch_array($user, MYSQL_ASSOC);
	$user_email = $rowUser['email'];
	$username = $rowUser['username'];
	$name = $rowUser['username'];

	$subject = 'Bug report/Request';
	$comment = 'Name: ' . $name . '</br>Username: ' . $username . '</br>Message:</br>' . $message;
	
	$success = mail($admin_email, $subject, $comment, "From:" . $user_email);
	if(!$success){
		create_error($access_token,'PHP','send_message.php','Error happened while sending your message!',$db);
		die('Error happened while sending your message!');		
	}