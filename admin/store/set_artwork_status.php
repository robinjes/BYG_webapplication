<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify an artwork
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','set_artwork_status.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}
	$artworkId = $_POST['id'];
	if(!isset($artworkId)){
		create_error($access_token,'PHP','set_artwork_status.php','Artwork ID is Missing!',$db);
		die('Artwork ID is Missing!');
	}
	$artwork_status = $_POST['display'];
	if(!isset($artwork_status)){
		create_error($access_token,'PHP','set_artwork_status.php','Display status is Missing!',$db);
		die('Display status is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_artwork_status.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $user_row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','set_artwork_status.php','No gallery found',$db);
		die('No gallery found');
	}
	$gallery_row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $gallery_row['gallery_id'];

	$artwork = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
	if((!$artwork)||(mysql_num_rows($artwork)<=0)){
		create_error($access_token,'PHP','set_artwork_status.php','No artwork found',$db);
		die('No artwork found');
	}
	$artwork_row = mysql_fetch_array($artwork, MYSQL_ASSOC);
	$artistId = $artwork_row['artist_id'];

	$artist = mysql_query("select * from artist where id = '" . $artistId . "'" , $db);
	if((!$artist)||(mysql_num_rows($artist)<=0)){
		create_error($access_token,'PHP','set_artwork_status.php','No artist found',$db);
		die('No artist found');
	}
	$artist_row = mysql_fetch_array($artist, MYSQL_ASSOC);
	$artist_status = $artist_row['display'];

	if($artwork_status == 'true'){
		$sql2 = mysql_query("update artwork set display='1' where id =" . $artworkId , $db);
		if($artist_status == 'false'){
			$sql1 = mysql_query("update artist set display='1' where id =" . $artistId , $db);
			if(!$sql1){
				create_error($access_token,'PHP','set_artwork_status.php','Not able to find or update the status of the artist',$db);
			  	die('Not able to find or update the status of the artist');
			}
		}

	}else{
		$sql2 = mysql_query("update artwork set display='0' where id =" . $artworkId , $db);
	}
	
	if(!$sql2){
		create_error($access_token,'PHP','set_artwork_status.php','Not able to find or update the status of the artwork',$db);
	  	die('Not able to find or update the status of the artwork');
	}
	
	//Close the database connection
	mysql_close($db);
?>