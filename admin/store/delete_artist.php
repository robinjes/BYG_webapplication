<?php
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$thumb_suffix           = "_sm"; //Normal thumb Prefix
	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Delete an artist
	$access_token = $_POST['access_token'];
	$artistId = $_POST['artistId'];
	if(!isset($access_token)){
		create_error('-','PHP','delete_artist.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}
	if(!isset($artistId)){
		create_error($access_token,'PHP','delete_artist.php','Artist ID is Missing!',$db);
		die('Artist ID is Missing!');
	}

	$token = mysql_query("select * from access_token where token = '" . $access_token . "'", $db);
	if((!$token)||(mysql_num_rows($token)<=0)){
		create_error($access_token,'PHP','delete_artist.php','No access found',$db);
		die('No access found');
	}
	$rowToken = mysql_fetch_array($token, MYSQL_ASSOC);
	$userId = $rowToken['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'", $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','delete_artist.php','No user found',$db);
		die('No user found');
	}
	$rowUser = mysql_fetch_array($user, MYSQL_ASSOC);	
	$galleryId = $rowUser['gallery_id'];

	$artist = mysql_query("select * from artist where id = '" . $artistId ."' and gallery_id = '" . $galleryId ."'" , $db);
	if((!$artist)||(mysql_num_rows($artist)<=0)){
		create_error($access_token,'PHP','delete_artist.php','No artist found',$db);
		die('No artist found');
	}
	$artist_row = mysql_fetch_array($artist, MYSQL_ASSOC);
	$artistName = $artist_row['name'];

	$result_artworks_ids = mysql_query("select * from artwork where artist_id = '" . $artistId ."'" , $db);
	if(!$result_artworks_ids){
		create_error($access_token,'PHP','delete_artist.php','Not able to find the artworks',$db);
	  	die('Not able to find the artworks');
	}

	$delete_artworks = mysql_query("delete from artwork where artist_id = '" . $artistId ."'" , $db);
	if(!$delete_artworks){
		create_error($access_token,'PHP','delete_artist.php','Not able to find or delete the artworks',$db);
	  	die('Not able to find or delete the artworks');
	}

	$delete_artist = mysql_query("delete from artist where id = '" . $artistId ."' and gallery_id = '" . $galleryId ."'" , $db);
	if(!$delete_artist){
		create_error($access_token,'PHP','delete_artist.php','Not able to find or delete the artist',$db);
	  	die('Not able to find or delete the artist');
	}

	$imgName = $galleryId . '_' . $artistId;
	$galleryDirectory = 'gallery_' . $galleryId . '/';
	$wholeImgPath = $destination_folder . $galleryDirectory . $imgName;
	$exts = array('.jpg', '.jpeg', '.png', '.gif');
	############ Delete the artist img ##############
	foreach ($exts as $ext) {
		if (file_exists($wholeImgPath . $ext)) {
			unlink($wholeImgPath . $ext);
			unlink($wholeImgPath . $thumb_suffix . $ext);
			echo 'File '. $wholeImgPath . $ext . ' and ' . $wholeImgPath . $thumb_suffix . $ext .' have been deleted';
		}
	}
	########################################################

	if(mysql_num_rows($result_artworks_ids)>0){
		$json_response_artworks_ids = array();
		while ($row = mysql_fetch_array($result_artworks_ids, MYSQL_ASSOC)) {
			$row_array['id'] = $row['id'];
			array_push($json_response_artworks_ids,$row_array);
		}
		############ Delete the artworks imgs ##############
		foreach ($json_response_artworks_ids as $json_response_artworks_id) {
			$artworkId = $json_response_artworks_id['id'];
			$imgName = $galleryId . '_' . $artistId . '_' . $artworkId;
			$wholeImgPath = $destination_folder . $galleryDirectory . $imgName;
			foreach ($exts as $ext) {
				if (file_exists($wholeImgPath . $ext)) {
					unlink($wholeImgPath . $ext);
					unlink($wholeImgPath . $thumb_suffix . $ext);
					echo 'File '. $wholeImgPath . $ext . ' and ' . $wholeImgPath . $thumb_suffix . $ext .' have been deleted';
				}
			}
		}
	}

	mysql_close($db);