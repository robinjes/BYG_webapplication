<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");
    $thumb_suffix           = "_sm"; //Normal thumb Prefix
    $destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify an artist
	$access_token = $_POST['access_token'];
	$artistId = $_POST['artistId'];
	$artistName = $_POST['name'];
	$location = $_POST['location'];
	$description = $_POST['description'];
	if(!isset($access_token)){
		create_error('-','PHP','set_artist.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_artist.php','No user found',$db);
		die('No user found');
	}
	$row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','set_artist.php','No gallery found',$db);
		die('No gallery found');
	}
	$row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $row['gallery_id'];

	$artist_update = mysql_query("update artist set name='" . $artistName ."', location='" . $location ."',description='" . $description ."' where id='" . $artistId ."' and gallery_id='" . $galleryId ."'" , $db);
	if(!$artist_update){
		create_error($access_token,'PHP','set_artist.php','Not able to update the artist',$db);
	  	die('Not able to update the artist');
	}
	
	//Close the database connection
	mysql_close($db);