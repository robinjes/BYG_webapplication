<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Add an artwork
	$access_token = $_POST['access_token'];
	$artistId = $_POST['artistId'];
	$title = $_POST['title'];
	$year = $_POST['year'];
	$dim_horizon = $_POST['dim_horizon'];
	$dim_vertica = $_POST['dim_vertica'];
	$technique = $_POST['technique'];

	if(!isset($access_token)){
		create_error('-','PHP','create_artwork.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}
	if(!isset($title)){
		create_error($access_token,'PHP','create_artwork.php','Title is Missing!',$db);
		die('Title is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','create_artwork.php','No user found',$db);
		die('No user found');
	}
	$row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','create_artwork.php','No gallery found',$db);
		die('No gallery found');
	}
	$row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $row['gallery_id'];
	
	$create_query = mysql_query("insert into artwork (title, year, dim_horizon_cm, dim_vertica_cm, technique, artist_id) values ('" . $title ."','" . $year ."','" . $dim_horizon ."','" . $dim_vertica ."','" . $technique ."','" . $artistId ."')" , $db);
	if(!$create_query){
		create_error($access_token,'PHP','create_artwork.php','Not able to find or create the artwork',$db);
	  	die('Not able to find or create the artwork');
	}

	$getid_query = mysql_query("select MAX(id) as max from artwork"); 
	if((!$getid_query)||(mysql_num_rows($getid_query)<=0)){
		create_error($access_token,'PHP','create_artwork.php','No artwork_id found',$db);
		die('No artwork_id found');
	}
	$row = mysql_fetch_array($getid_query, MYSQL_ASSOC);
	$artworkId = $row['max'];

	$img = $galleryId . '_' . $artistId . '_' . $artworkId;

	$update_query = mysql_query("update artwork set img='" . $img ."' where id='" . $artworkId ."'" , $db);
	if(!$update_query){
		create_error($access_token,'PHP','create_artwork.php','No img updated',$db);
	  	die('No img updated');
	}

	$artwork_query = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
	$json_response_artwork = array();
	$artwork_row = mysql_fetch_array($artwork_query, MYSQL_ASSOC);
	$json_response_artwork['id'] = $artwork_row['id'];
    $json_response_artwork['title'] = utf8_encode($artwork_row['title']);
    $json_response_artwork['imgName'] = utf8_encode($artwork_row['img']);

	if(!$artwork_query){
		create_error($access_token,'PHP','create_artwork.php','The artwork has not been created',$db);
	  	die('The artwork has not been created');
	}else{
		echo json_encode($json_response_artwork);
	}
	
	//Close the database connection
	mysql_close($db);