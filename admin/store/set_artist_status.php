<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify an artist
	$access_token = $_POST['access_token'];
	$artistId = $_POST['id'];
	$artist_status = $_POST['display'];
	if(!isset($access_token)){
		create_error('-','PHP','set_artist_status.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_artist_status.php','No user found',$db);
		die('No user found');
	}
	$row_user = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $row_user['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','set_artist_status.php','No gallery found',$db);
		die('No gallery found');
	}
	$row_gallery = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $row_gallery['gallery_id'];

	if($artist_status == 'true'){
		$artist_update = mysql_query("update artist set display='1' where id='" . $artistId ."' and gallery_id='" . $galleryId ."'" , $db);
		$artwork_update = mysql_query("update artwork set display='1' where artist_id='" . $artistId . "'" , $db);
		if(!$artwork_update){
			create_error($access_token,'PHP','set_artist_status.php','Not able to update the status of the artwork',$db);
		  	die('Not able to update the status of the artwork');
		}
	}else{
		$artist_update = mysql_query("update artist set display='0' where id='" . $artistId ."' and gallery_id='" . $galleryId ."'" , $db);
		$artwork_update = mysql_query("update artwork set display='0' where artist_id='" . $artistId . "'" , $db);
		if(!$artwork_update){
			create_error($access_token,'PHP','set_artist_status.php','Not able to update the status of the artwork',$db);
		  	die('Not able to update the status of the artwork');
		}		
	}
	if(!$artist_update){
		create_error($access_token,'PHP','set_artist_status.php','Not able to update the status of the artist',$db);
	  	die('Not able to update the status of the artist');
	}
	
	//Close the database connection
	mysql_close($db);