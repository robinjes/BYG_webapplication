<?php
    header('Content-Type: application/json; charset=utf-8');
    ############ Configuration ##############
    $ini_array              = parse_ini_file("../properties.ini");
    $max_thumb_size         = $ini_array['records_max_thumb_size'];
    $max_image_size         = $ini_array['records_max_image_size'];
    $thumb_suffix           = "_sm"; //Normal thumb Prefix
    $destination_folder     = $ini_array['datas_folder_path'];
    $jpeg_quality_img       = $ini_array['records_jpeg_quality_img'];
    $jpeg_quality_tumb      = $ini_array['records_jpeg_quality_tumb'];

    $db_prefix              = $ini_array['db_prefix'];
    $db_ip                  = $ini_array['db_ip'];
    $db_name                = $ini_array['db_name'];
    $db_username            = $ini_array['db_username'];
    $db_password            = $ini_array['db_password'];
    ##########################################
    ############ Get datas from DB ##############
    $db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
    mysql_select_db($db_name) or die("Could not select database");
    include 'create_error.php';

    $access_token = $_GET['access_token'];
    if(!isset($access_token)){
        create_error('-','PHP','create_artwork_img.php','Access Token is Missing!',$db);
        die('Access Token is Missing!');
    }
    $artworkId = $_GET['id'];
    if(!isset($artworkId)){
        create_error($access_token,'PHP','create_artwork_img.php','Artwork ID is Missing!',$db);
        die('Artwork ID is Missing!');
    }

    $access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
    if((!$access)||(mysql_num_rows($access)<=0)){
        create_error($access_token,'PHP','create_artwork_img.php','No access found',$db);
        die('No access found');
    }
    $access_row = mysql_fetch_array($access, MYSQL_ASSOC);
    $userId = $access_row['user_id'];

    $user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
    if((!$user)||(mysql_num_rows($user)<=0)){
        create_error($access_token,'PHP','create_artwork_img.php','No user found',$db);
        die('No user found');
    }
    $user_row = mysql_fetch_array($user, MYSQL_ASSOC);
    $galleryId = $user_row['gallery_id'];

    $artwork = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
    if((!$artwork)||(mysql_num_rows($artwork)<=0)){
        create_error($access_token,'PHP','create_artwork_img.php','No artwork found',$db);
        die('No artwork found');
    }
    $artwork_row = mysql_fetch_array($artwork, MYSQL_ASSOC);
    $title = $artwork_row['title'];
    $artistId = $artwork_row['artist_id'];

    $table = array(
        ' '=>'', 'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj', 'Ž'=>'Z', 'ž'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
        'À'=>'a', 'Á'=>'a', 'Â'=>'a', 'Ã'=>'a', 'Ä'=>'a', 'Å'=>'a', 'Æ'=>'a', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'R'=>'R', 'r'=>'r', "'"=>'', '"'=>''
    );
    $imgName = $galleryId . '_' . $artistId . '_' . $artworkId;
    $galleryDirectory = 'gallery_' . $galleryId . '/';
    ##########################################
    ############ Create an image and its tumbnail for an artwork ##############
    //continue only if $_POST is set and it is a Ajax request
    if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
        // check $_FILES['ImageFile'] not empty
        if(!isset($_FILES['image_file']) || !is_uploaded_file($_FILES['image_file']['tmp_name'])){
            create_error($access_token,'PHP','create_artwork_img.php','Image file is Missing!',$db);
            die('Image file is Missing!'); // output error when above checks fail.
        }
        
        //get uploaded file info before we proceed
        $image_name_original    = $_FILES['image_file']['name']; //file name
        $image_size             = $_FILES['image_file']['size']; //file size
        $image_temp             = $_FILES['image_file']['tmp_name']; //file temp
        $image_error            = $_FILES['image_file']['error']; //file error

        if ($image_error !== UPLOAD_ERR_OK) {
            create_error($access_token,'PHP','create_artwork_img.php','An error occured with the image file: ' . $image_error,$db);
            die('An error occured with the image file: ' . $image_error);
        }

        $image_size_info        = getimagesize($image_temp); //gets image size info from valid image file
        
        if($image_size_info){
            $image_width        = $image_size_info[0]; //image width
            $image_height       = $image_size_info[1]; //image height
            $image_type         = $image_size_info['mime']; //image type
        }else{
            create_error($access_token,'PHP','create_artwork_img.php','Make sure the image file is valid!',$db);
            die('Make sure the image file is valid!');
        }

        switch($image_type){
            case 'image/png':
                $image_res =  true;
                break;
            case 'image/gif':
                $image_res =  true;
                break;
            case 'image/jpeg':
                $image_res =  true;
                break;
            case 'image/pjpeg':
                $image_res =  true;
                break;
            default:
                $image_res = false;
        }

        if($image_res){
            $image_info = pathinfo($image_name_original);
            $image_extension = strtolower($image_info["extension"]); //image extension
            //Create the thumbnail image
            $image = new Imagick($image_temp);
            //Upload the image
            $image_save_folder  = $destination_folder . $galleryDirectory . $imgName . '.' .$image_extension;
            $success = $image->writeImage($image_save_folder);
            if(!$success){ 
                create_error($access_token,'PHP','create_artwork_img.php','Fail to upload the image.',$db);
                die('Fail to upload the image.');
            }else{
                chmod($image_save_folder, 0644);
                //Create the thumbnail image
                $success = $image->cropThumbnailImage($max_thumb_size, $max_thumb_size);
                if(!$success){
                    create_error($access_token,'PHP','create_artwork_img.php','Fail to crop the image thumbnail.',$db);
                    die('Fail to crop the image thumbnail.');
                }else{
                    //Upload the thumbnail image
                    $thumb_save_folder  = $destination_folder . $galleryDirectory . $imgName . $thumb_suffix . '.' .$image_extension;
                    $success = $image->writeImage($thumb_save_folder);
                    if (!$success){
                        create_error($access_token,'PHP','create_artwork_img.php','Fail to upload the image thumbnail.',$db);
                        die('Fail to upload the image thumbnail.');
                    }
                    chmod($thumb_save_folder, 0644);
                    $result_artwork_imgOverviewPath = mysql_query("select artwork.img from artwork inner join artist on artwork.artist_id = artist.id where artist.gallery_id = '" . $galleryId . "' and artwork.id = '" . $artworkId . "'" , $db);
                    while ($row = mysql_fetch_array($result_artwork_imgOverviewPath, MYSQL_ASSOC)) {
                        $imgOverview = getImgOverviewPath($destination_folder . $galleryDirectory,utf8_encode($row['img']));
                    }
                    if(($imgOverview)||(mysql_num_rows($gallery)>0)){
                        echo json_encode($imgOverview);
                    }else{
                        create_error($access_token,'PHP','create_artwork_img.php','No thumbnail image found. Try again!',$db);
                        die('No thumbnail image found. Try again!');
                    }
                }
            }
        }else{
            create_error($access_token,'PHP','create_artwork_img.php','No valid image found.',$db);
            die('No valid image found.');
        }
    }else{
        create_error($access_token,'PHP','create_artwork_img.php','Something went wrong with the request. Try again!',$db);
        die('Something went wrong with the request. Try again!');
    }

    function getImgOverviewPath($galleryDirectory,$imgName){
        $imgName = $imgName . '_sm';
        $imgPath = '';

        if($handle = opendir($galleryDirectory)) {
            while(false !== ($file = readdir($handle))){
                if(strpos($file,$imgName) !== false){
                    $img = $galleryDirectory . $file;
                    $imgData = base64_encode(file_get_contents($img));
                    $imgPath = 'data: '.get_type($img).';base64,'.$imgData;
                }
            }
            closedir($handle);
        }else{
            create_error($access_token,'PHP','create_artwork_img.php','No image found in the gallery directory',$db);
            die('No image found in the gallery directory');
        }
        return $imgPath;
    }

    function get_type($file){
        $mime_types = array(
            "pdf"=>"application/pdf"
            ,"exe"=>"application/octet-stream"
            ,"zip"=>"application/zip"
            ,"docx"=>"application/msword"
            ,"doc"=>"application/msword"
            ,"xls"=>"application/vnd.ms-excel"
            ,"ppt"=>"application/vnd.ms-powerpoint"
            ,"gif"=>"image/gif"
            ,"png"=>"image/png"
            ,"jpeg"=>"image/jpg"
            ,"jpg"=>"image/jpg"
            ,"mp3"=>"audio/mpeg"
            ,"wav"=>"audio/x-wav"
            ,"mpeg"=>"video/mpeg"
            ,"mpg"=>"video/mpeg"
            ,"mpe"=>"video/mpeg"
            ,"mov"=>"video/quicktime"
            ,"avi"=>"video/x-msvideo"
            ,"3gp"=>"video/3gpp"
            ,"css"=>"text/css"
            ,"jsc"=>"application/javascript"
            ,"js"=>"application/javascript"
            ,"php"=>"text/html"
            ,"htm"=>"text/html"
            ,"html"=>"text/html"
        );
        $extension = strtolower(end(explode('.',$file)));
        return $mime_types[$extension];
    }

mysql_close($db);