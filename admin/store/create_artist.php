<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Add an artist
	$access_token = $_POST['access_token'];
	$name = $_POST['name'];
	$location = $_POST['location'];
	$description = $_POST['description'];
	if(!isset($access_token)){
		create_error('-','PHP','create_artist.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}
	if(!isset($name)){
		create_error($access_token,'PHP','create_artist.php','Name is Missing!',$db);
		die('Name is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','create_artist.php','No user found',$db);
		die('No user found');
	}
	$row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','create_artist.php','No gallery found',$db);
		die('No gallery found');
	}
	$row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $row['gallery_id'];
	
	$create_query = mysql_query("insert into artist (name, location, description, gallery_id) values ('" . $name ."','" . $location ."','" . $description ."','" . $galleryId ."')" , $db);
	if(!$create_query){
		create_error($access_token,'PHP','create_artist.php','Not able to find or create the artist',$db);
	  	die('Not able to find or create the artist');
	}

	$getid_query = mysql_query("select MAX(id) as max from artist"); 
	if((!$getid_query)||(mysql_num_rows($getid_query)<=0)){
		create_error($access_token,'PHP','create_artist.php','No artist_id found',$db);
		die('No artist_id found');
	}
	$row = mysql_fetch_array($getid_query, MYSQL_ASSOC);
	$artistId = $row['max'];

	$img = $galleryId . '_' . $artistId;

	$update_query = mysql_query("update artist set img='" . $img ."' where id='" . $artistId ."'" , $db);
	if(!$update_query){
		create_error($access_token,'PHP','create_artist.php','No img updated',$db);
	  	die('No img updated');
	}

	$artist_query = mysql_query("select * from artist where id = '" . $artistId . "'" , $db);
	$json_response_artist = array();
	$artist_row = mysql_fetch_array($artist_query, MYSQL_ASSOC);
	$json_response_artist['id'] = $artist_row['id'];
    $json_response_artist['name'] = utf8_encode($artist_row['name']);
    $json_response_artist['imgName'] = utf8_encode($artist_row['img']);

	if(!$artist_query){
		create_error($access_token,'PHP','create_artist.php','The artist has not been created',$db);
	  	die('The artist has not been created');
	}else{
		echo json_encode($json_response_artist);
	}
	
	//Close the database connection
	mysql_close($db);
?>