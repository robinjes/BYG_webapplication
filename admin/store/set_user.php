<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify a user
	$access_token = $_POST['access_token'];
	$username = $_POST['username'];
	$name = $_POST['name'];
	$gallery_name = $_POST['gallery_name'];
	$location = $_POST['gallery_location'];
	if(!isset($access_token)){
		create_error('-','I-100','set_user.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'I-100','set_user.php','No user found',$db);
		die('No user found');
	}
	$row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'I-100','set_user.php','No gallery found',$db);
		die('No gallery found');
	}
	$row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $row['gallery_id'];


	$sql_user = mysql_query("update user set username = '" . $username ."', name ='" . $name ."' where id='" . $userId ."'" , $db);
	if(!$sql_user){
		create_error($access_token,'I-250','set_user.php','Not able to find or update the user',$db);
	  	die('Not able to find or update the user');
	}

	$sql_gallery = mysql_query("update gallery set name = '" . $gallery_name ."', location = '" . $location ."' where id='" . $galleryId ."'" , $db);
	if(!$sql_gallery){
		create_error($access_token,'I-200','set_user.php','Not able to find or update the gallery',$db);
	  	die('Not able to find or update the gallery');
	}
	
	//Close the database connection
	mysql_close($db);
?>