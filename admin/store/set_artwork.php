<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify an artwork
	$access_token = $_POST['access_token'];
	$artworkId = $_POST['artworkId'];
	$title = $_POST['title'];
	$year = $_POST['year'];
	$dim_horizon = $_POST['dim_horizon'];
	$dim_vertica = $_POST['dim_vertica'];
	$technique = $_POST['technique'];
	if(!isset($access_token)){
		create_error('-','PHP','set_artwork.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}

	$user = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_artwork.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$userId = $user_row['user_id'];

	$gallery = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$gallery)||(mysql_num_rows($gallery)<=0)){
		create_error($access_token,'PHP','set_artwork.php','No gallery found',$db);
		die('No gallery found');
	}
	$gallery_row = mysql_fetch_array($gallery, MYSQL_ASSOC);
	$galleryId = $gallery_row['gallery_id'];

	$artwork = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
	if((!$artwork)||(mysql_num_rows($artwork)<=0)){
		create_error($access_token,'PHP','set_artwork.php','No artwork found',$db);
		die('No artwork found');
	}
	$artwork_row = mysql_fetch_array($artwork, MYSQL_ASSOC);
	$artistId = $artwork_row['artist_id'];
	
	$img = $galleryId . '_' . $artistId . '_' . $artworkId;

	$sql = mysql_query("update artwork set title='" . $title ."', year='" . $year ."', dim_horizon_cm ='" . $dim_horizon ."', dim_vertica_cm ='" . $dim_vertica ."', technique ='" . $technique ."', img = '" . $img ."' where id ='" . $artworkId ."' and artist_id='" . $artistId ."'" , $db);
	if(!$sql){
		create_error($access_token,'PHP','set_artwork.php','No artwork found',$db);
	  	die('No artwork found');
	}

	$artwork_query = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
	$json_response_artwork = array();
	$artwork_row = mysql_fetch_array($artwork_query, MYSQL_ASSOC);
	$json_response_artwork['id'] = $artwork_row['id'];
    $json_response_artwork['title'] = utf8_encode($artwork_row['title']);
    $json_response_artwork['imgName'] = utf8_encode($artwork_row['img']);

	if(!$artwork_query){
		create_error($access_token,'PHP','set_artwork.php','The artwork has not been created',$db);
	  	die('The artwork has not been created');
	}else{
		echo json_encode($json_response_artwork);
	}
	
	//Close the database connection
	mysql_close($db);
?>