<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Modify a setting
	$access_token = $_POST['access_token'];
	$name = $_POST['name'];
	$value = $_POST['value'];

	if(!isset($access_token)){
		create_error('-','PHP','set_setting.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}
	if(!isset($name)){
		create_error($access_token,'PHP','set_setting.php','Name is Missing!',$db);
		die('Name is Missing!');
	}else if(($name == 'auto_slideshow_activate')||($name == 'scale_real_activate')){
		$value = strtolower($value);
		switch ($value) {
			case 'on': $value = '1';
	        break;
			case 'off': $value = '0';
	        break;
			case '1': $value = '1';
	        break;
			case '0': $value = '0';
	        break;
	        default: $value = '0';
		}

		if(!(($value == '0')||($value == '1'))){
			$value = '0';
		}
	}

	$token = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$token)||(mysql_num_rows($token)<=0)){
		create_error($access_token,'PHP','set_setting.php','No token found',$db);
		die('No token found');
	}
	$row_token = mysql_fetch_array($token, MYSQL_ASSOC);
	$userId = $row_token['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','set_setting.php','No user found',$db);
		die('No user found');
	}
	$row_user = mysql_fetch_array($user, MYSQL_ASSOC);
	$settingId = $row_user['settings_id'];

	$sql_setting = mysql_query("update setting set " . $name . " = '" . $value ."' where id ='" . $settingId ."'", $db);
	if(!$sql_setting){
		create_error($access_token,'PHP','set_setting.php','Not able to find or update the setting',$db);
	  	die('Not able to find or update the setting');
	}
	
	//Close the database connection
	mysql_close($db);