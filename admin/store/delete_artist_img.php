<?php
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");
	
	$thumb_suffix           = "_sm"; //Normal thumb Prefix
	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	############ Get datas from DB ##############
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	$access_token = $_GET['access_token'];
	$artistId = $_POST['artistId'];
    if(!isset($access_token)){
        create_error('-','PHP','delete_artist_img.php','Access Token is Missing!',$db);
        die('Access Token is Missing!');
    }
	if(!isset($artistId)){
		create_error($access_token,'PHP','delete_artist_img.php','Artist ID is Missing!',$db);
		die('Artist ID is Missing!');
	}

    $access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
    if((!$access)||(mysql_num_rows($access)<=0)){
        create_error($access_token,'PHP','delete_artist_img.php','No access found',$db);
        die('No access found');
    }

	$artist = mysql_query("select * from artist where id = '" . $artistId . "'" , $db);
	if((!$artist)||(mysql_num_rows($artist)<=0)){
		create_error($access_token,'PHP','delete_artist_img.php','No artist found',$db);
		die('No artist found');
	}
	$artist_row = mysql_fetch_array($artist, MYSQL_ASSOC);
	$artistName = $artist_row['name'];
	$galleryId = $artist_row['gallery_id'];

	$imgName = $galleryId . '_' . $artistId;
	$galleryDirectory = 'gallery_' . $galleryId . '/';
	$wholeImgPath = $destination_folder . $galleryDirectory . $imgName;
	$exts = array('.jpg', '.jpeg', '.png', '.gif');
	##########################################
	############ Delete the corresponding imgs ##############
	foreach ($exts as $ext) {
		if (file_exists($wholeImgPath . $ext)) {
			unlink($wholeImgPath . $ext);
			unlink($wholeImgPath . $thumb_suffix . $ext);
		}
	}

mysql_close($db);