<?php
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$thumb_suffix           = "_sm"; //Normal thumb Prefix
	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Delete an artist
	$access_token = $_POST['access_token'];
	$artworkId = $_POST['artworkId'];
	if(!isset($access_token)){
		create_error('-','PHP','delete_artwork.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}
	if(!isset($artworkId)){
		create_error($access_token,'PHP','delete_artwork.php','Artwork ID is Missing!',$db);
		die('Artwork ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','delete_artwork.php','No access found',$db);
		die('No access found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','delete_artwork.php','No user found',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$galleryId = $user_row['gallery_id'];

	$artwork = mysql_query("select * from artwork where id = '" . $artworkId . "'" , $db);
	if((!$artwork)||(mysql_num_rows($artwork)<=0)){
		create_error($access_token,'PHP','delete_artwork.php','No artwork found',$db);
		die('No artwork found');
	}
	$artwork_row = mysql_fetch_array($artwork, MYSQL_ASSOC);
	$title = $artwork_row['title'];
	$artistId = $artwork_row['artist_id'];	

	$delete_artwork = mysql_query("delete from artwork where id = '" . $artworkId ."'", $db);
	if(!$delete_artwork){
		create_error($access_token,'PHP','delete_artwork.php','Not able to find or delete the artwork',$db);
	  	die('Not able to find or delete the artwork');
	}

	$imgName = $galleryId . '_' . $artistId . '_' . $artworkId;
	$galleryDirectory = 'gallery_' . $galleryId . '/';
	$wholeImgPath = $destination_folder . $galleryDirectory . $imgName;
	$exts = array('.jpg', '.jpeg', '.png', '.gif');
	############ Delete the corresponding imgs ##############
	foreach ($exts as $ext) {
		if (file_exists($wholeImgPath . $ext)) {
			unlink($wholeImgPath . $ext);
			unlink($wholeImgPath . $thumb_suffix . $ext);
			echo 'File '. $wholeImgPath . $ext . ' and ' . $wholeImgPath . $thumb_suffix . $ext .' have been deleted';
		}
	}

	mysql_close($db);