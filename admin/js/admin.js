window.popup_height = 30;
window.opened_bugReport = false;

/*########################*/
/* When document is ready */
/*########################*/
$( document ).ready(function() {
	setAllPart_sizes();
	$('.veil#admin_report_bug').click(function() {
  		close_bugReport();
	});
});
$( window ).resize(function() {
  	setArtistPart_Sizes();
  	setSettingPart_Sizes();
  	setAboutPart_Sizes();
  	setArtistModificationPart_Sizes();
  	setArtworkModificationPart_Sizes();
  	setUserModificationPart_Sizes();
});

function publicView(){
	//$( "#foo" ).slideUp( 300 ).delay( 800 ).fadeIn( 400 );
	$('img.presentation').delay(1000).animate({
    	opacity: 1
  	}, 500, function() {
    	$('#page_welcome_content').animate({
	    	opacity: 1
	  	}, 500, function() {
	    	// Animation complete.
	  	});	
	});
	$('#page_welcome_title').delay(1000).animate({
    	opacity: 1
  	}, 500);		
}

function open_bugReport(){
	if(!window.opened_bugReport){
		var viewport_width = $( window ).width();
		var viewport_height = $( window ).height();
		var container_width = $('#admin_bugReport_container').width();
		var message_height = (viewport_height/2)-50;
		$('.veil#admin_report_bug').css('display','block');
		$('.veil#admin_report_bug').height(viewport_height);
	  	$('.veil#admin_report_bug').animate({
	    	opacity: 0.9
	  	}, 500, function() {
	    	// Animation complete.
	  	});
	  	$('#admin_bugReport_container').animate({
	  		height: viewport_height/2,
	    	bottom: viewport_height/4,
	    	right: (viewport_width - container_width)/2
	  	}, 600, function() {
	    	// Animation complete.
	  	});
	  	$('#admin_bugReport_title').animate({
	  		backgroundColor: '#FFD191'
	  	}, 600, function() {
	    	// Animation complete.
	  	});
	  	$('#admin_bugReport_message').animate({
	  		height: message_height
	  	}, 600, function() {
	  		// Animation complete.
	  		$('#admin_bugReport_message textarea').css('max-height',message_height*0.80);
	  	});
	  	window.opened_bugReport = true;
  	}
}

function close_bugReport(){
	if(window.opened_bugReport){
	  	$('.veil#admin_report_bug').animate({
	    	opacity: 0
	  	}, 500, function() {
		  	$('.veil#admin_report_bug').height(0);
		  	$('.veil#admin_report_bug').css('display','none');
	  	});
	  	$('#admin_bugReport_title').animate({
	  		backgroundColor: '#EFEFEF'
	  	}, 600, function() {
	    // Animation complete.
	  	});
	  	$('#admin_bugReport_message').animate({
	  		height: '200px'
	  	}, 600, function() {
	    // Animation complete.
	  	});
	  	$('#admin_bugReport_container').animate({
	  		height: '250px',
	    	bottom: '-200px',
	    	right: '-15%'
	  	}, 600, function() {
	    // Animation complete.
	  	});
		window.opened_bugReport = false;
	}
}
function add_Artist_Img_UploadButtonListener(){
	/*############ Input file system ##############*/
	var uploadButton = document.querySelector('#imgUploadButton .admin_input_file#artist');
	var displayArea = document.querySelector('.admin_artist_picture .admin_img_overview');

	if(uploadButton != null && displayArea != null ){
		uploadButton.removeEventListener('change', uploadArtistImgFunction);
		uploadButton.addEventListener('change', uploadArtistImgFunction);
	}else{
		showError('007','No upload button or display area found!','admin.js: uploadButton');
	}
}
function uploadArtistImgFunction(){
	var uploadButton = document.querySelector('#imgUploadButton .admin_input_file#artist');
	var current_artist_id = $('.admin_artist_picture input[name="current_artist_id"]').val();
	$('.admin_artist_picture .admin_img_overview img#'+current_artist_id).fadeOut(100);
	var access_token = localStorage.getItem('accessTok');
	if (window.FormData) {
		var artistimg = new FormData();
	}
	if(window.File && window.FileReader && window.FileList && window.Blob){
		var file = uploadButton.files[0];
		var imageType = /image.*/;
		var imageSize = 5242880; //5MB
		if (file.type.match(imageType)) {
			if(file.size<=imageSize){
				var reader = new FileReader();
				reader.onload = function(e){
				    var self = Gallery.__container__.lookup('controller:admin_artist');
				    self.store.find('artist',current_artist_id).then(function(artist) {
				        artist.set('imgOverviewPath', e.target.result);
				        artist.save();
				        $('.admin_artist_picture .admin_img_overview img#'+current_artist_id).attr('src',e.target.result);
				        $('.admin_artist_picture .admin_img_overview img#'+current_artist_id).fadeIn(500);
				    });
				};
				reader.readAsDataURL(file);
				artistimg.append("image_file", file);
				addArtistImg(current_artist_id,artistimg,access_token);
			}else{
				showError('007','The image has to weight maximum 5MB!','admin.artists.js: file.size');
			}
		} else {
			showError('007','Only image types are supported!','admin.artists.js: file.type.match()');
		}
	}else{
		showError('007','Cant upload! Your browser does not support File API!','admin.js: FileReader');
	}	
}

function add_Artwork_Img_UploadButtonListener(){
	/*############ Input file system ##############*/
	var uploadButton = document.querySelector('#imgUploadButton .admin_input_file#artwork');
	var displayArea = document.querySelector('.admin_artwork_picture .admin_img_overview');

	if(uploadButton != null && displayArea != null ){
		uploadButton.removeEventListener('change', uploadArtworkImgFunction);
		uploadButton.addEventListener('change', uploadArtworkImgFunction);
	}else{
		showError('007','No upload button or display area found!','admin.js: uploadButton');
	}
}
function uploadArtworkImgFunction(){
	var uploadButton = document.querySelector('#imgUploadButton .admin_input_file#artwork');
	var current_artwork_id = $('.admin_artwork_picture input[name="current_artwork_id"]').val();
	$('.admin_artwork_picture .admin_img_overview img#'+current_artwork_id).fadeOut(100);
	var access_token = localStorage.getItem('accessTok');
	if (window.FormData) {
		var artworkimg = new FormData();
	}
	if(window.File && window.FileReader && window.FileList && window.Blob){
		var file = uploadButton.files[0];
		var imageType = /image.*/;
		var imageSize = 5242880; //5MB
		if (file.type.match(imageType)) {
			if(file.size<=imageSize){
				var reader = new FileReader();
				reader.onload = function(e){
	            	var self = Gallery.__container__.lookup('controller:admin_artist');
	            	self.store.find('artwork',current_artwork_id).then(function(artwork) {
		                artwork.set('imgOverviewPath', e.target.result);
		                artwork.save();
	                	$('.admin_artwork_picture .admin_img_overview img#'+current_artwork_id).attr('src',e.target.result);
				        $('.admin_artwork_picture .admin_img_overview img#'+current_artwork_id).fadeIn(500);
				    });
				};				
				reader.readAsDataURL(file);
				artworkimg.append("image_file", file);
				addArtworkImg(current_artwork_id,artworkimg,access_token);
			}else{
				showError('007','The image has to weight maximum 5MB!','admin.js: file.size');
			}
		} else {
			showError('007','Only image types are supported!','admin.js: file.type.match()');
		}
	}else{
		showError('007','Cant upload! Your browser does not support File API!','admin.js: FileReader');
	}	
}
function setAllPart_sizes(){

}
function setArtistPart_Sizes(){
	var menu_height = $('.admin_menu').height();
	var arianeLandmark_height = $('#arianeLandmark').height();
	var viewport_width = $( window ).width();
	var viewport_height = $( window ).height();
	var admin_menu_height = $('.admin_menu').height();
	window.stepSize = $('.admin_case').width();

	$('#wrapper').css('top',admin_menu_height);

	var window_adminArtist_height = viewport_height - menu_height - arianeLandmark_height;
	$('.admin_case').css('height', window_adminArtist_height);
	var viewport_width = $(window).width();
	var window_adminArtist_width = viewport_width*50/100;
	$('.admin_case').css('width', window_adminArtist_width);
	$('.admin_case .admin_title').css('height', window_adminArtist_height);
	$('.admin_case .admin_title h2').css('width', window_adminArtist_height);
	var window_adminDatas_marginTop = $('#arianeLandmark').height();
	$('#admin_datas').css('margin-top', window_adminDatas_marginTop);
	$('.admin_case .admin_artists').css('max-height', window_adminArtist_height);
	$('.admin_case .admin_artist').css('max-height', window_adminArtist_height);
	$('.admin_case .admin_artist .admin_img_overview img').css('max-height', window_adminArtist_height/2);
	var window_artistsName_height = $('.admin_artists_element').outerHeight();

	$('.admin_error_popup').height(window.popup_height);
	$('.admin_info_popup').height(window.popup_height);
	/*Ariane landmarks visual balance*/
	var ariane_nbElements = 0;
	$('#arianeLandmark .arianeElement').each(function() {
		ariane_nbElements = ariane_nbElements+1;
	});
	var window_arianeLandmark_width = $('#arianeLandmark').outerWidth(true);
	$('#arianeLandmark .arianeElement').each(function() {
		$(this).css('width', (window_arianeLandmark_width/ariane_nbElements)-1);
	});
	/*Confirmation boxes*/
	var window_confirmationbox_height = viewport_height/4;
	var window_confirmationbox_margintop = (viewport_height - window_confirmationbox_height)/2;
	$('.confirmationBox').css('height', window_confirmationbox_height);
	$('.confirmationBox').css('margin-top', window_confirmationbox_margintop);
	$('.confirmationBox p').css('margin-top', window_confirmationbox_height/5);
	$('.confirmationBox p').css('margin-bottom', window_confirmationbox_height/10);
	$('.confirm_options').css('height', window_confirmationbox_height/1.5);
	$('.veil:visible').css('height', viewport_height);

	setVisuelOnActiveLink();
	setBrowsingSystem();
	updateVisuals();

	/*Hide/Show visual effects*/
	$('div.admin_artists_element:has(div.shownButton)').css('color', 'rgba(77,77,77,1)');
	$('div.admin_artists_element:has(div.hiddenButton)').css('color', 'rgba(77,77,77,0.6)');
	$('div.admin_artworks_element:has(div.shownButton)').css('color', 'rgba(77,77,77,1)');
	$('div.admin_artworks_element:has(div.hiddenButton)').css('color', 'rgba(77,77,77,0.6)');
}
function setSettingPart_Sizes(){
  var viewport_height = $(window).height();
  var viewport_width = $(window).width();
  var margin_vertical = viewport_width/100;

  var window_height = viewport_height - $('.admin_menu').outerHeight() - 1;
  $('.admin_setting_window').css('height', window_height);
  $('#admin_setting').css('height', (window_height/4*3));
  $('#admin_setting h3').css('margin-bottom', margin_vertical*2);
  $('#admin_setting h3').css('margin-top', margin_vertical*2);
  $('#admin_setting input').css('margin-bottom', margin_vertical);

  var input_height = $('#admin_setting input').outerHeight();
  $('.modify_setting').css('height', input_height);
  $('.modify_setting').css('width', input_height);
  $('.modify_setting').css('margin-left', margin_vertical/2);

  var user_height = (window_height/4) - margin_vertical;
  $('#admin_user').css('height', user_height);
  $('#admin_infographics').css('height', user_height);
  $('#admin_user_data').css('height', user_height);
  $('.data_case').css('height', $('#admin_user_data').height());

  var window_confirmationbox_height = viewport_height/2;
  var window_confirmationbox_margintop = (viewport_height - window_confirmationbox_height)/2;
  $('.confirmationBox').css('height', window_confirmationbox_height);
  $('.confirmationBox').css('margin-top', window_confirmationbox_margintop);
  $('.confirmationBox p').css('margin-top', (window_confirmationbox_height/4)/3);
  $('.confirmationBox p').css('margin-bottom', (window_confirmationbox_height/4)/3);
  $('.confirm_options').css('height', window_confirmationbox_height/4);
  $('.confirm_data').css('height', window_confirmationbox_height/2);
  $('.veil').css('height', viewport_height);

  createCharts();
  setVisualEffects();
}
function setAboutPart_Sizes(){
	$('.accordion').accordion();
}
function setArtistModificationPart_Sizes(){
	$('body').css('overflow', 'hidden');
	var viewport_height = $(window).height();
	$('.modifyPage').css('height', viewport_height);
	$('.modifyPage #admin_mod_title').css('height', viewport_height/5);
	var content_height = $('.modifyPage').height()/5*3;//3/5
	$('.modifyPage #admin_toModif').css('height', content_height);
	$('.modifyPage #admin_infoToModif').css('height', content_height);
	var validIt_height = $('.modifyPage').height()/5;//1/5
	$('.modifyPage #admin_validIt').css('height', validIt_height);
	/*Confirmation boxes*/
	var window_confirmationbox_height = viewport_height/4;
	var window_confirmationbox_margintop = (viewport_height - window_confirmationbox_height)/2;
	$('.confirmationBox').css('height', window_confirmationbox_height);
	$('.confirmationBox').css('margin-top', window_confirmationbox_margintop);
	$('.confirmationBox p').css('margin-top', window_confirmationbox_height/5);
	$('.confirmationBox p').css('margin-bottom', window_confirmationbox_height/10);
	$('.confirm_options').css('height', window_confirmationbox_height/1.5);
	$('.veil').css('height', viewport_height);

	$('input:required').css('background-size','auto '+$('input:required').css('font-size'));

	unRegisterAllEventListeners(window);
	window.addEventListener('input', function (e) {
		var self = Gallery.__container__.lookup('controller:admin_artist_modification');
		self.set('userHasEnteredData',true);
	}, false);
}
function setArtworkModificationPart_Sizes(){
	$('body').css('overflow', 'hidden');
	var viewport_height = $(window).height();
	$('.modifyPage').css('height', viewport_height);
	$('.modifyPage #admin_mod_title').css('height', viewport_height/5);
	var content_height = $('.modifyPage').height()/5*3;//3/5
	$('.modifyPage #admin_toModif').css('height', content_height);
	$('.modifyPage #admin_infoToModif').css('height', content_height);
	var validIt_height = $('.modifyPage').height()/5;//1/5
	$('.modifyPage #admin_validIt').css('height', validIt_height);
	/*Confirmation boxes*/
	var window_confirmationbox_height = viewport_height/4;
	var window_confirmationbox_margintop = (viewport_height - window_confirmationbox_height)/2;
	$('.confirmationBox').css('height', window_confirmationbox_height);
	$('.confirmationBox').css('margin-top', window_confirmationbox_margintop);
	$('.confirmationBox p').css('margin-top', window_confirmationbox_height/5);
	$('.confirmationBox p').css('margin-bottom', window_confirmationbox_height/10);
	$('.confirm_options').css('height', window_confirmationbox_height/1.5);
	$('.veil').css('height', viewport_height);

	$('input:required').css('background-size','auto '+$('input:required').css('font-size'));

	unRegisterAllEventListeners(window);
	window.addEventListener('input', function (e) {
		var self = Gallery.__container__.lookup('controller:admin_artwork_modification');
		self.set('userHasEnteredData',true);
	}, false);
}
function setUserModificationPart_Sizes(){
	$('body').css('overflow', 'hidden');
	var viewport_height = $(window).height();
	$('.modifyPage').css('height', viewport_height);
	$('.modifyPage #admin_mod_title').css('height', viewport_height/5);
	var content_height = $('.modifyPage').height()/5*3;//3/5
	$('.modifyPage #admin_toModif').css('height', content_height);
	var validIt_height = $('.modifyPage').height()/5;//1/5
	$('.modifyPage #admin_validIt').css('height', validIt_height);
	/*Confirmation boxes*/
	var window_confirmationbox_height = viewport_height/4;
	var window_confirmationbox_margintop = (viewport_height - window_confirmationbox_height)/2;
	$('.confirmationBox').css('height', window_confirmationbox_height);
	$('.confirmationBox').css('margin-top', window_confirmationbox_margintop);
	$('.confirmationBox p').css('margin-top', window_confirmationbox_height/5);
	$('.confirmationBox p').css('margin-bottom', window_confirmationbox_height/10);
	$('.confirm_options').css('height', window_confirmationbox_height/1.5);
	$('.veil').css('height', viewport_height);

	var button_pass_modification = $('#passChange').parent('a').height();
  	$('#passChange').css('height', button_pass_modification);

	$('input:required').css('background-size','auto '+$('input:required').css('font-size'));

	unRegisterAllEventListeners(window);
	window.addEventListener('input', function (e) {
		var self = Gallery.__container__.lookup('controller:admin_user_modification');
		self.set('userHasEnteredData',true);
	}, false);
}
function showCloseConfirmationBox(id_name){
	var id_name = id_name;
	if((id_name!=null)&&(id_name!='')){
		id_name = '#'+id_name;
	}else{
		id_name = '';
	}
	$('.veil'+id_name).show();
	$('.confirmationBox#cancel_modification_artist').show();
	$('.confirmationBox#cancel_modification_artwork').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}

function showModificationConfirmationBox(id_name){
	var id_name = id_name;
	if((id_name!=null)&&(id_name!='')){
		id_name = '#'+id_name;
	}else{
		id_name = '';
	}
	$('.veil'+id_name).show();
	$('.confirmationBox#validate_modification_artist').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationConfirmationBox_artwork(id_name){
	var id_name = id_name;
	if((id_name!=null)&&(id_name!='')){
		id_name = '#'+id_name;
	}else{
		id_name = '';
	}
	$('.veil'+id_name).show();
	$('.confirmationBox#validate_modification_artwork').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationConfirmationBox_user(){
	$('.veil#veil_user').show();
	$('.confirmationBox#validate_modification_user').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationBox_password(){
	$('.veil#veil_password').show();
	$('.confirmationBox#changePassword').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function showModificationConfirmationBox_password(){
	$('.veil#veil_password').show();
	$('.confirmationBox#validate_modification_password').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}
function getArtistImgSrc(){
	var returnValue = 'true';
	var containsImg = $('#admin_imgToModif #imgPlace').find('img').length;
	console.log('containsImg: '+containsImg);
	if(containsImg>0){
		returnValue = 'true';
	}else{
		returnValue = 'false';
	}
	return returnValue;
}
function getArtworkImgSrc(){
	var returnValue = 'true';
	var containsImg = $('#admin_imgToModif #imgPlace').find('img').length;
	console.log('containsImg: '+containsImg);
	if(containsImg>0){
		returnValue = 'true';
	}else{
		returnValue = 'false';
	}
	return returnValue;
}
function createCharts(){
  var chart_user_space_canvas = $('.admin_space_chart').get(0).getContext("2d");
  var chart_user_space_data = [
    {
      value: $('[name="free_space"]').val(),
      color:"#878BB6",
      highlight: "#FFC870",
      label: "Free space"
    },
    {
      value : $('[name="directory_used_space"]').val(),
      color : "#4ACAB4",
      highlight: "#FFC870",
      label: "Used space"
    }
  ];
  var chart_user_space_options = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke : true,
    //String - The colour of each segment stroke
    segmentStrokeColor : "rgba(77,77,77,0.7)",
    //Number - The width of each segment stroke
    segmentStrokeWidth : 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout : 30, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps : 100,
    //String - Animation easing effect
    animationEasing : "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate : true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale : false,
    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()+Go%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  var chart_user_space = new Chart(chart_user_space_canvas).Pie(chart_user_space_data, chart_user_space_options);
}
function setVisualEffects(){
  /*Switch*/
  var switch_component = $('.switch');
  switch_component.each(function( index ) {
      var switch_value = $(this).val();
    if(switch_value == '0'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(switch_value == '1'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }
  });
}
function showModifySettingBox(field_cat,field_title,field_value,field_unit){
  $('.veil').show();
  $('.confirmationBox#modification_setting .confirm_message').append(field_title);
  var componentToImplement = '';

  switch(field_cat){
    case 'text': 
      componentToImplement = '<input type="text" name="value_to_submit" value="'+field_value+'" />';
      break;
    case 'number': 
      componentToImplement = '<input type="number" name="value_to_submit" pattern= "[0-9]" value="'+field_value+'" />';
      break;
    case 'switch': 
      componentToImplement = '<input type="text" class="modify_switch" name="value_to_submit" value="'+field_value+'" readonly="readonly" />';
      break;
    case 'multiple': 
      componentToImplement = '<input type="text" name="value_to_submit" value="'+field_value+'" />';
      break;
  }
  $('.confirmationBox#modification_setting .confirm_data').empty();
  $('.confirmationBox#modification_setting .confirm_data').append(componentToImplement);

  /*Modify Part Switch*/
  var modify_switch_component = $('.modify_switch');
  modify_switch_component.each(function( index ) {
    var modify_switch_value = $(this).val();
    if(modify_switch_value == '0'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(modify_switch_value == '1'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }
  });
  modify_switch_component.click(function() {
    var modify_switch_value = $(this).val();
    if(modify_switch_value == 'ON'){
      $(this).val('OFF');
      $(this).css('background-color','rgba(214,84,82,1)');
    }else if(modify_switch_value == 'OFF'){
      $(this).val('ON');
      $(this).css('background-color','rgba(175,205,115,1)');
    }  
  });

  $('.confirmationBox#modification_setting').show();

  $(document).keydown(function(e){
    // Escape key
      if (e.keyCode == 27) {
          hideConfirmationBox();
      }
  });
}

function getNewFieldValue(){
  return $('[name="value_to_submit"]').val();
}
function setVisuelOnActiveLink(){
  	/*Artists list link*/
	$('.admin_artists_element').click(function() {
  		$('.admin_artists_element').each(function() {
  			$(this).css('background-color', 'rgba(230,175,97, 0)');
  		});
  		$(this).css('background-color', 'rgba(230,175,97, 0.3)');
	});  
	$('.admin_artists_element').each(function() {
		$(this).children('.active').parent().css('background-color', 'rgba(230,175,97, 0.3)');	
	});
  	/*Artworks list link*/
	$('.admin_artworks_element').click(function() {
  		$('.admin_artworks_element').each(function() {
  			$(this).css('background-color', 'rgba(230,175,97, 0)');
  		});
  		$(this).css('background-color', 'rgba(230,175,97, 0.3)');
	});  
	$('.admin_artworks_element').each(function() {
		$(this).children('.active').parent().css('background-color', 'rgba(230,175,97, 0.3)');	
	});
}
/*----------------- Set browsing system -----------------*/
function setBrowsingSystem(){
	/*Artists scroll ancre*/
	/*$('.admin_artists_element').click(function() {
		$('body').scrollLeft(window.stepSize);
	});*/
	/*Artworks scroll ancre*/
	/*$('.admin_artworks_element').click(function() {
		$('body').scrollLeft(window.stepSize*2);
	});*/
	
	/*Wheel horizontal browsing*/
	$('body').on('mousewheel', function(event) {
    	var currentScrollPosition = $('body').scrollLeft();

		var artists_section_position = 0;
		var artist_section_position = window.stepSize;
		var artworks_section_position = window.stepSize*2;
		var artwork_section_position = window.stepSize*3;
    	/*if(event.deltaY<0){
    		//Go left
    		if(currentScrollPosition<=artist_section_position){
    			$('body').scrollLeft(artists_section_position);
    		}else if((currentScrollPosition>artist_section_position) && (currentScrollPosition<=(artworks_section_position))){
    			$('body').scrollLeft(artist_section_position);
    		}else if(currentScrollPosition>(artworks_section_position)){
    			$('body').scrollLeft(artworks_section_position);
    		}else{
    			$('body').scrollLeft(artist_section_position);
    		}
    	}else{
    		//Go right
    		if((currentScrollPosition>=artist_section_position) && (currentScrollPosition<(window.stepSize*2))){
    			$('body').scrollLeft(artworks_section_position);
    		}else if(currentScrollPosition>=(artworks_section_position)){
    			$('body').scrollLeft(artwork_section_position);
    		}else{
    			$('body').scrollLeft(artist_section_position);
    		}
    	}*/    	
	});
	/*Ariane landmarks visual*/
	var currentScrollPosition = $('body').scrollLeft();
	colorLandmarks(currentScrollPosition);
	$(window).scroll(function (event) {
	    var currentScrollPosition = $('body').scrollLeft();
	    if(currentScrollPosition != window.oldCurrentScrollPosition){
	    	colorLandmarks(currentScrollPosition + 1);
	    }
	});
	/*Ariane click function*/
	$('#arianeLandmark .arianeElement').click(function() {
		var arianeElement_id = $(this).attr('id');
		var section_name = getArtistSection(window.current_route);
		var artists_section_position = 0;
		var artist_section_position = window.stepSize;
		var artworks_section_position = window.stepSize*2;
		var artwork_section_position = window.stepSize*3;
		switch(section_name){
			case 'artists':
				$('body').scrollLeft(artists_section_position);
				break;
			case 'artist':
				switch(arianeElement_id){
					case 'artists':
						$('body').scrollLeft(artists_section_position);
						break;
					case 'artist':
						$('body').scrollLeft(artist_section_position);
						break;
					case 'artworks':
						$('body').scrollLeft(artworks_section_position);
						break;
				};
				break;
			case 'artworks':
				switch(arianeElement_id){
					case 'artists':
						$('body').scrollLeft(artists_section_position);
						break;
					case 'artist':
						$('body').scrollLeft(artist_section_position);
						break;
					case 'artworks':
						$('body').scrollLeft(artworks_section_position);
						break;
					case 'artwork':
						$('body').scrollLeft(artwork_section_position);
						break;	
				};
				break;
			case 'artwork':
				switch(arianeElement_id){
					case 'artists':
						$('body').scrollLeft(artists_section_position);
						break;
					case 'artist':
						$('body').scrollLeft(artist_section_position);
						break;
					case 'artworks':
						$('body').scrollLeft(artworks_section_position);
						break;
					case 'artwork':
						$('body').scrollLeft(artwork_section_position);
						break;	
				};
				break;	
		}
	});
}
function updateVisuals(){
	var section_name = getArtistSection(window.current_route);
	var artists_section_position = 0;
	var artist_section_position = window.stepSize;
	var artworks_section_position = window.stepSize*2;
	var artwork_section_position = window.stepSize*3;

	$('.arianeElement').each(function() {
		$(this).css('display', 'none');	
	});	

	switch(section_name){
		case 'artists':
			$('body').scrollLeft(artists_section_position);
			$('.arianeElement#artists').css('display', 'block');
			break;
		case 'artist':
			$('body').scrollLeft(artist_section_position);
			$('.arianeElement#artists').css('display', 'block');
			$('.arianeElement#artist').css('display', 'block');
			$('.arianeElement#artworks').css('display', 'block');
			break;
		case 'artworks':
			$('body').scrollLeft(artworks_section_position);
			$('.arianeElement#artists').css('display', 'block');
			$('.arianeElement#artist').css('display', 'block');
			$('.arianeElement#artworks').css('display', 'block');
			break;
		case 'artwork':
			$('body').scrollLeft(artwork_section_position);
			$('.arianeElement#artists').css('display', 'block');
			$('.arianeElement#artist').css('display', 'block');
			$('.arianeElement#artworks').css('display', 'block');
			$('.arianeElement#artwork').css('display', 'block');
			break;	
	};
}
function getArtistSection(currentroute){
	return currentroute.substring(currentroute.lastIndexOf('/')+1);
}
function colorLandmarks(currentScrollPosition){
	$('.arianeElement').each(function() {
		$(this).css('background-color', 'rgba(77,77,77,0.1)');	
	});
	if(currentScrollPosition<=window.stepSize){
		//In first step
		$('.arianeElement#artists').css('background-color', 'rgba(230,175,97, 0.3)');
		window.oldCurrentScrollPosition = currentScrollPosition;
	}else if((currentScrollPosition>window.stepSize) && (currentScrollPosition<=(window.stepSize*2))){
		//In second step
		$('.arianeElement#artist').css('background-color', 'rgba(230,175,97, 0.3)');
		window.oldCurrentScrollPosition = currentScrollPosition;
	}else if((currentScrollPosition>(window.stepSize*2)) && (currentScrollPosition<=(window.stepSize*3))){
		//In third step
		$('.arianeElement#artworks').css('background-color', 'rgba(230,175,97, 0.3)');
		window.oldCurrentScrollPosition = currentScrollPosition;
	}else{
		//In fourth step
		$('.arianeElement#artwork').css('background-color', 'rgba(230,175,97, 0.3)');
		window.oldCurrentScrollPosition = currentScrollPosition;
	}
}

function showRemoveConfirmationBox_artist(){
    var viewport_height = $(window).height();
    $('.veil#admin_artists').fadeIn(500);
    $('.veil#admin_artists').height(viewport_height);
	$('.confirmationBox#validate_remove_artist').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}

function showRemoveConfirmationBox_artwork(){
    var viewport_height = $(window).height();
    $('.veil#admin_artists').fadeIn(500);
    $('.veil#admin_artists').height(viewport_height);
	$('.confirmationBox#validate_remove_artwork').show();
	$(document).keydown(function(e){
    // Escape key
	    if (e.keyCode == 27) {
	        hideConfirmationBox();
	    }
	});
}

function hideConfirmationBox(){
  	$('.veil').hide();
	$('.confirmationBox').hide();
}
function showError(error_ref,error_message,error_source){
	window.popupQty_error = $('.admin_error_popup').length;
	window.popupQty_info = $('.admin_info_popup').length;
	var popupQty_total = window.popupQty_error + window.popupQty_info;
	var error_message = error_message.replace("<br>", "");	
	if(error_source == null){
		if((error_message!='') && (error_message!= null)){
			if(popupQty_total<=0){
				$('body').append('<div class="admin_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
			}else{
				var error_position_bottom = popupQty_total * window.popup_height;
				$('body').append('<div class="admin_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
			}		
		}else{
			if(popupQty_total<=0){
				$('body').append('<div class="admin_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_ref + '</p></div>');
			}else{
				var error_position_bottom = popupQty_total * window.popup_height;
				$('body').append('<div class="admin_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_ref + '</p></div>');
			}			
		}
	}else{
		if((error_message!='') && (error_message!= null)){
			if(popupQty_total<=0){
				$('body').append('<div class="admin_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
			}else{
				var error_position_bottom = popupQty_total * window.popup_height;
				$('body').append('<div class="admin_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>' + error_message + '</p></div>');
			}		
		}else{
			if(popupQty_total<=0){
				$('body').append('<div class="admin_error_popup" style="bottom: 0px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>Error ' + error_ref + '</p></div>');
			}else{
				var error_position_bottom = popupQty_total * window.popup_height;
				$('body').append('<div class="admin_error_popup" id="' + window.popupQty_error + '" style="bottom: ' + error_position_bottom + 'px"><p><i class="fa fa-times close_error" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeError(this);"></i>Error ' + error_ref + '</p></div>');
			}			
		}
	}
}

function showInfo(info_ref,info_message,info_source){
	window.popupQty_error = $('.admin_error_popup').length;
	window.popupQty_info = $('.admin_info_popup').length;
	var popupQty_total = window.popupQty_error + window.popupQty_info;
	if(popupQty_total<=0){
		$('body').append('<div class="admin_info_popup" style="position: fixed; bottom: 0px; right: 0px;"><p><i class="fa fa-times close_info" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeInfoPopup(this);"></i>' + info_message + '</p></div>');
	}else{
		var info_position_bottom = popupQty_total * window.popup_height;
		$('body').append('<div class="admin_info_popup" id="' + window.popupQty_info + '" style="position: fixed; bottom: ' + info_position_bottom + 'px; right: 0px;"><p><i class="fa fa-times close_info" style="color:rgba(255,255,255,0.6); margin-right:0.5%;" onclick="closeInfoPopup(this);"></i>' + info_message + '</p></div>');
	}
}

function closeAllError(){
	$('.admin_error_popup').remove();
}
function closeError(element){
	element.closest('.admin_error_popup').remove();
}
function closeInfoPopup(element){
	element.closest('.admin_info_popup').remove();
}

function closeAllInfoPopup(){
  $('.admin_info_popup').remove();
}

function unRegisterAllEventListeners(obj) {
	if ( typeof obj._eventListeners == 'undefined' || obj._eventListeners.length == 0 ) {
		return;	
	}
	
	for(var i = 0, len = obj._eventListeners.length; i < len; i++) {
		var e = obj._eventListeners[i];
		obj.removeEventListener(e.event, e.callback);
	}
 
	obj._eventListeners = [];
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}