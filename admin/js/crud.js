/*#################*/
/* Get all artists */
/*#################*/
function getAllArtists(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_artists.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_artist_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*##################*/
/* Set an artist */
/*##################*/
function setArtist(access_token,artistId,name,location,description){
    var success = null; 
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((name == 'undefined')||(!name)){
        name = '-';
    }
    if((location == 'undefined')||(!location)){
        location = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }
    if((artistId == 'undefined')||(!artistId)){
        showError('007','Reload the page to update your modification','crud.js: setArtist()'); 
    }else{
        $.ajax({
            url: window.serverpath+"/store/set_artist.php",
            type: "POST",
            data: {'access_token':access_token,'artistId':artistId,'name':name,'location':location,'description':description},
            async: false,
            success: function () {
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artist', artistId).then(function(artist) {
                    artist.set('name', name);
                    artist.set('location', location);
                    artist.set('description', description);
                    self.transitionToRoute('admin_artist', artist);
                });
            },
            error: function(error) {
                if(error.status == 200){
                    //Don't show the error to the user
                    var self = Gallery.__container__.lookup('controller:admin_artist');
                    self.store.find('artist', artistId).then(function(artist) {
                        artist.set('name', name);
                        artist.set('location', location);
                        artist.set('description', description);
                        self.transitionToRoute('admin_artist', artist);
                    });
                }else{
                    if(error.responseText){
                        showError(error.status,error.responseText,'crud.js: setArtist()');  
                    }else{
                        showError(error.status,error.statusText,'crud.js: setArtist()'); 
                    }
                }
            }
        });
        return success;
    }
}
/*##################*/
/* Add a new artist */
/*##################*/
function addArtist(access_token,name,location,description){
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((name == 'undefined')||(!name)){
        name = '-';
    }
    if((location == 'undefined')||(!location)){
        location = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/create_artist.php",
        type: 'post',
        data: {'access_token':access_token,'name':name,'location':location,'description':description},
        dataType: 'json',
        async: false,
        success: function (artist) {
            var artistId = artist.id;
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.push('artist', {
              id: artistId,
              name: name,
              location: location,
              description: description
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtist()');
            }else{
                showError(error.status,error.statusText,'crud.js: addArtist()');
            }
        }
    });
}
/*##################*/
/* Remove an artist */
/*##################*/
function removeArtist(access_token,id){
    $.ajax({
        url: window.serverpath+"/store/delete_artist.php",
        type: "POST",
        data: {'access_token':access_token,'artistId':id},
        async: false,
        success: function (data) {
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist', id).then(function (artist) {
                artist.destroyRecord(artist);
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: removeArtist()');  
            }else{
                showError(error.status,error.statusText,'crud.js: removeArtist()'); 
            }
        }
    });
}
/*####################################*/
/* Add an image (avatar) for an artist */
/*####################################*/
function addArtistImg(artistId,artistImg,access_token){
    var viewport_height = $(window).height();
    $('.veil#admin_upload_artistImg').fadeIn(500);
    $('.veil#admin_upload_artistImg').height(viewport_height);
    var date = new Date();
    var upload_ref = date.getDate().toString() + date.getMonth().toString() + date.getFullYear().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString() + date.getMilliseconds().toString();
    $.ajax({
        url: window.serverpath+"/store/create_artist_img.php?id="+artistId+"&access_token="+access_token,
        type: "POST",
        data: artistImg,
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener('progress', function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    var percent_uploaded_artistImg = percentComplete * 100;
                    document.querySelector('#upload_barre_artist').MaterialProgress.setProgress(percent_uploaded_artistImg);
                }
            }, false);
            //Download progress
            xhr.addEventListener("progress", function(evt) {

            }, false);
            return xhr;
        },
        processData: false,
        contentType: false,
        success: function (data) {
            $('.veil#admin_upload_artistImg').hide();
        },
        error: function(error) {
            $('.veil#admin_upload_artistImg').hide();
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtistImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtistImg()'); 
            }
        }
    });
}
/*####################################*/
/* Add an image (avatar) for an artwork */
/*####################################*/
function addArtworkImg(artworkId,artworkImg,access_token){
    var viewport_height = $(window).height();
    $('.veil#admin_upload_artworkImg').fadeIn(500);
    $('.veil#admin_upload_artworkImg').height(viewport_height);
    var date = new Date();
    var upload_ref = date.getDate().toString() + date.getMonth().toString() + date.getFullYear().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString() + date.getMilliseconds().toString();
    $.ajax({
        url: window.serverpath+"/store/create_artwork_img.php?id="+artworkId+"&access_token="+access_token,
        type: "POST",
        data: artworkImg,
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener('progress', function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    var percent_uploaded_artworkImg = percentComplete * 100;
                    document.querySelector('#upload_barre_artwork').MaterialProgress.setProgress(percent_uploaded_artworkImg);
                }
            }, false);
            //Download progress
            xhr.addEventListener("progress", function(evt) {
            }, false);
            return xhr;
        },
        processData: false,
        contentType: false,
        success: function (data) {
            $('.veil#admin_upload_artworkImg').hide();
        },
        error: function(error) {
            $('.veil#admin_upload_artworkImg').hide();
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtworkImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtworkImg()'); 
            }
        }
    });
}
/*##################*/
/* Get all artworks */
/*##################*/
function getAllArtworks(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_artworks.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_artworks_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*##################*/
/* Set an artwork */
/*##################*/
function setArtwork(access_token,artworkId,title,year,technique,dim_horizon,dim_vertica){
    var success = null; 
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((title == 'undefined')||(!title)){
        title = '-';
    }
    if((year == 'undefined')||(!year)){
        year = '-';
    }
    if((dim_horizon == 'undefined')||(!dim_horizon)){
        dim_horizon = '-';
    }
    if((dim_vertica == 'undefined')||(!dim_vertica)){
        dim_vertica = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/set_artwork.php",
        type: "POST",
        data: {'access_token':access_token,'artworkId':artworkId,'title':title,'year':year,'technique':technique,'dim_horizon':dim_horizon,'dim_vertica':dim_vertica},
        async: false,
        success: function (artwork) {
            var self = Gallery.__container__.lookup('controller:admin_artwork');
            self.store.find('artwork', artworkId).then(function(artwork) {
                artwork.set('title', title);
                artwork.set('year', year);
                artwork.set('dim_horizon', dim_horizon);
                artwork.set('dim_vertica', dim_vertica);
                artwork.set('technique', technique);
                self.transitionToRoute('admin_artwork', artwork);
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setArtwork()');
            }
        }
    });
    return success;
}
/*##################*/
/* Add a new artwork */
/*##################*/
function addArtwork(access_token,artistId,title,year,technique,dim_horizon,dim_vertica,description){
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((title == 'undefined')||(!title)){
        title = '-';
    }
    if((year == 'undefined')||(!year)){
        year = '-';
    }
    if((dim_horizon == 'undefined')||(!dim_horizon)){
        dim_horizon = '-';
    }
    if((dim_vertica == 'undefined')||(!dim_vertica)){
        dim_vertica = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/create_artwork.php",
        type: 'post',
        data: {'access_token':access_token,'artistId':artistId,'title':title,'year':year,'dim_horizon':dim_horizon,'dim_vertica':dim_vertica,'description':description},
        dataType: 'json',
        async: false,
        success: function (artwork) {
            var artworkId = artwork.id;
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist', artistId).then(function(artist) {
                self.store.push('artwork', {
                    artist:artist,
                    id: artworkId,
                    title: title,
                    year: year,
                    dim_horizon: dim_horizon,
                    dim_vertica: dim_vertica,
                    description: description,
                    artist_id: artistId
                });
                self.store.find('artwork',artworkId).then(function(artwork) {
                    self.transitionToRoute('admin_artwork', artwork);
                });
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtwork()');
            }
        }
    });

}
/*##################*/
/* Remove an artwork */
/*##################*/
function removeArtwork(access_token,id){
    var success = null;
    $.ajax({
        url: window.serverpath+"/store/delete_artwork.php",
        type: "POST",
        data: {'access_token':access_token,'artworkId':id},
        async: false,
        success: function (state) {
            var self = Gallery.__container__.lookup('controller:admin_artwork');
            self.store.find('artwork', id).then(function (artwork) {
                self.store.find('artist', artwork.get('artist_id')).then(function (artist) {
                    artwork.deleteRecord();
                    self.transitionToRoute('admin_artist',artist);
                });
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: removeArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: removeArtwork()');
            }
        }
    });
    return success; 
}
/*#################*/
/* Get all options */
/*#################*/
function getAllSettings(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_options.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_settings_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*#################*/
/* Set an option */
/*#################*/
function setSetting(access_token,settingName,value){
    $.ajax({
        url: window.serverpath+"/store/set_setting.php",
        type: "POST",
        data: {'access_token':access_token,'name':settingName,'value':value},
        async: false,
        success: function (data) {
            var self = Gallery.__container__.lookup('controller:admin_settings');
            self.store.find('setting').then(function(settings) {
                var setting = settings.objectAt(0);
                switch(settingName){
                    case 'resolutionPx_horizon': setting.set('resolutionPx_horizon', value);
                    break;
                    case 'resolutionPx_vertical': setting.set('resolutionPx_vertical', value);
                    break;
                    case 'projectionCm_horizon': setting.set('projectionCm_horizon', value);
                    break;
                    case 'projectionCm_vertical': setting.set('projectionCm_vertical', value);
                    break;
                    case 'auto_slideshow_activate': setting.set('auto_slideshow_activate', value);
                    break;
                    case 'auto_slideshow_time': setting.set('auto_slideshow_time', value);
                    break;
                    case 'scale_real_activate': setting.set('scale_real_activate', value);
                    break;
                    case 'selection_countdown_time': setting.set('selection_countdown_time', value);
                    break;
                    default: showError('007','No corresponding field found!','crud.js: setSetting');
                }
            });
        },
        error: function(error) {
            if(error.status == 200){
                var self = Gallery.__container__.lookup('controller:admin_settings');
                self.store.find('setting').then(function(settings) {
                    var setting = settings.objectAt(0);
                    switch(settingName){
                        case 'resolutionPx_horizon': setting.set('resolutionPx_horizon', value);
                        break;
                        case 'resolutionPx_vertical': setting.set('resolutionPx_vertical', value);
                        break;
                        case 'projectionCm_horizon': setting.set('projectionCm_horizon', value);
                        break;
                        case 'projectionCm_vertical': setting.set('projectionCm_vertical', value);
                        break;
                        case 'auto_slideshow_activate': setting.set('auto_slideshow_activate', value);
                        break;
                        case 'auto_slideshow_time': setting.set('auto_slideshow_time', value);
                        break;
                        case 'scale_real_activate': setting.set('scale_real_activate', value);
                        break;
                        case 'selection_countdown_time': setting.set('selection_countdown_time', value);
                        break;
                        default: showError('007','No corresponding field found!','crud.js: setSetting');
                    }
                });
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: setSetting()');  
                }else{
                    showError(error.status,error.statusText,'crud.js: setSetting()');
                }
            }
        }
    }); 
}
/*############*/
/* Check user */
/*############*/
function initUser(usernameoremail,password){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/init_user.php",
        type: "POST",
        data: {'usernameoremail':usernameoremail, 'password':password},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: initUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: initUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Get a user */
/*############*/
function getUser(access_token){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/get_user.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Set a user */
/*############*/
function setUser(access_token,name,username,gallery_name,gallery_location){
    if((username == '')||(username == 'undefined')||(username == null)){
        showError(007,'Username cannot be blank.','crud.js: setUser()'); 
    }else{
        $.ajax({
            url: window.serverpath+"/store/set_user.php",
            type: "POST",
            data: {'access_token':access_token,'name':name,'username':username,'gallery_name':gallery_name,'gallery_location':gallery_location},
            async: false,
            success: function (data) {
            },
            error: function(error) {
                if(error.status == 200){
                }else{
                    if(error.responseText){
                        showError(error.status,error.responseText,'crud.js: setUser()');  
                    }else{
                        showError(error.status,error.statusText,'crud.js: setUser()');
                    }
                }
            }
        });
    }
}
/*###############*/
/* Get a session */
/*###############*/
function getSession(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_session.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_session_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);  
}
/*#####################*/
/* Get all information */
/*#####################*/
function getAllInformation(access_token){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/get_information.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getAllInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getAllInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Add a new information */
/*#######################*/
function deleteInformation(access_token,info_id){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/delete_information.php",
        type: "POST",
        data: {'access_token':access_token,'info_id':info_id},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: deleteInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: deleteInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Remove an information */
/*#######################*/
function addInformation(access_token,message,users_ids){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/create_information.php",
        type: "POST",
        data: {'access_token':access_token,'message':message,'message':message},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addInformation()');
            }
        }
    });
    return infos;   
}
/*#####################*/
/* Set a user password */
/*#####################*/
function setPass(access_token, pass){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/set_password.php",
        type: "POST",
        data: {'access_token':access_token,'pass':pass},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setPass()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setPass()');
            }
        }
    });
    return user;   
}
/*################*/
/* Remove a token */
/*################*/
function removeTokenInDB(access_token){
    $.ajax({
        url: window.serverpath+"/store/delete_token.php",
        type: "POST",
        data: {'access_token':access_token},
        async: false,
    });
}
/*###############################*/
/* JSON file - Get all countries */
/*###############################*/
function getCountries(){
    var url = 'json/countries.json';
    var j = [];
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      success: function(data) { j = data;},
      error: function(data) { j = data;},
      async: false
    });
    return j;
}
/*################################*/
/* JSON file - Get all techniques */
/*################################*/
function getTechniques(){
    var url = 'json/techniques.json';
    var j = [];
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'json',
      success: function(data) { j = data;},
      error: function(data) { j = data;},
      async: false
    });
    return j;
}
/*######################################*/
/* Change the Artist status to "Hidden" */
/*######################################*/
function hideArtist(access_token,artist_id){
    $.ajax({
        url: window.serverpath+"/store/set_artist_status.php",
        type: "POST",
        data: {'access_token':access_token,'id':artist_id, 'display':false},
        async: false,
        success: function (data) {
          if(artist_id != null || artist_id != 'undefined'){
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist',artist_id).then(function(artist) {
                var itemQty = artist.get('artworksQty');
                self.store.find('artwork',{'artist_id':artist_id}).then(function(artworks) {
                    for (i = 0; i < itemQty; i++){
                        artworks.objectAt(i).set('display',false);
                        artworks.objectAt(i).save();
                        console.log(artworks.objectAt(i).id);
                        $('div.admin_artworks_element:has(div#'+artworks.objectAt(i).id+')').css('color', 'rgba(77,77,77,0.6)');
                    }
                }); 
                artist.set('display',false);
                artist.save();
            });
            $('.shownButton#'+artist_id).removeClass('shownButton').addClass('hiddenButton');
            $('div.admin_artists_element:has(div.hiddenButton#'+artist_id+')').css('color', 'rgba(77,77,77,0.6)');
          }
        },
        error: function(error) {
            if(error.status == 200){
              if(artist_id != null || artist_id != 'undefined'){
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artist',artist_id).then(function(artist) {
                var itemQty = artist.get('artworksQty');
                    self.store.find('artwork',{'artist_id':artist_id}).then(function(artworks) {
                        for (i = 0; i < itemQty; i++){
                            artworks.objectAt(i).set('display',false);
                            artworks.objectAt(i).save();
                            console.log(artworks.objectAt(i).id);
                            $('div.admin_artworks_element:has(div#'+artworks.objectAt(i).id+')').css('color', 'rgba(77,77,77,0.6)');
                        }
                    }); 
                    artist.set('display',false);
                    artist.save();
                });
                $('.shownButton#'+artist_id).removeClass('shownButton').addClass('hiddenButton');
                $('div.admin_artists_element:has(div.hiddenButton#'+artist_id+')').css('color', 'rgba(77,77,77,0.6)');
              }
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: hideArtist()');
                }else{
                    showError(error.status,error.statusText,'crud.js: hideArtist()');
                }
            }
        }
    });
}
/*#####################################*/
/* Change the Artist status to "Shown" */
/*#####################################*/
function showArtist(access_token,artist_id){
    $.ajax({
        url: window.serverpath+"/store/set_artist_status.php",
        type: "POST",
        data: {'access_token':access_token,'id':artist_id, 'display':true},
        async: false,
        success: function (data) {
            if(artist_id != null || artist_id != 'undefined'){
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artist',artist_id).then(function(artist) {
                    var itemQty = artist.get('artworksQty');
                    self.store.find('artwork',{'artist_id':artist_id}).then(function(artworks) {
                        for (i = 0; i < itemQty; i++){
                            artworks.objectAt(i).set('display',true);
                            artworks.objectAt(i).save();
                            console.log(artworks.objectAt(i).id);
                            $('div.admin_artworks_element:has(div#'+artworks.objectAt(i).id+')').css('color', 'rgba(77,77,77,1)');
                        }
                    });
                    artist.set('display',true);
                    artist.save();
                });
                $('.hiddenButton#'+artist_id).removeClass('hiddenButton').addClass('shownButton');
                $('div.admin_artists_element:has(div.shownButton#'+artist_id+')').css('color', 'rgba(77,77,77,1)');
            }        
        },
        error: function(error) {
            if(error.status == 200){
              if(artist_id != null || artist_id != 'undefined'){
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artist',artist_id).then(function(artist) {
                var itemQty = artist.get('artworksQty');
                    self.store.find('artwork',{'artist_id':artist_id}).then(function(artworks) {
                        for (i = 0; i < itemQty; i++){
                            artworks.objectAt(i).set('display',true);
                            artworks.objectAt(i).save();
                            console.log(artworks.objectAt(i).id);
                            $('div.admin_artworks_element:has(div#'+artworks.objectAt(i).id+')').css('color', 'rgba(77,77,77,1)');
                        }
                    });
                    artist.set('display',true);
                    artist.save();
                });
                $('.hiddenButton#'+artist_id).removeClass('hiddenButton').addClass('shownButton');
                $('div.admin_artists_element:has(div.shownButton#'+artist_id+')').css('color', 'rgba(77,77,77,1)');
              }
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: showArtist()');
                }else{
                    showError(error.status,error.statusText,'crud.js: showArtist()');
                }
            }
        }
    });
}
/*######################################*/
/* Change the Artwork status to "Hidden" */
/*######################################*/
function hideArtwork(access_token,artwork_id){
    $.ajax({
        url: window.serverpath+"/store/set_artwork_status.php",
        type: "POST",
        data: {'access_token':access_token,'id':artwork_id, 'display':false},
        async: false,
        success: function (data) {
            if(artwork_id != null || artwork_id != 'undefined'){
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artwork',artwork_id).then(function(artwork) {
                  artwork.set('display',false);
                  artwork.save();
                });         
                $('.shownButton#'+artwork_id).removeClass('shownButton').addClass('hiddenButton');
                $('div.admin_artworks_element:has(div.hiddenButton#'+artwork_id+')').css('color', 'rgba(77,77,77,0.6)');
            }
        },
        error: function(error) {
            if(error.status == 200){
                if(artwork_id != null || artwork_id != 'undefined'){
                    var self = Gallery.__container__.lookup('controller:admin_artist');
                    self.store.find('artwork',artwork_id).then(function(artwork) {
                      artwork.set('display',false);
                      artwork.save();
                    });
                    $('.shownButton#'+artwork_id).removeClass('shownButton').addClass('hiddenButton');
                    $('div.admin_artworks_element:has(div.hiddenButton#'+artwork_id+')').css('color', 'rgba(77,77,77,0.6)');
                }
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: hideArtwork()');
                }else{
                    showError(error.status,error.statusText,'crud.js: hideArtwork()');
                }
            }
        }
    });
}
/*######################################*/
/* Change the Artwork status to "Shown" */
/*######################################*/
function showArtwork(access_token,artwork_id){
    $.ajax({
        url: window.serverpath+"/store/set_artwork_status.php",
        type: "POST",
        data: {'access_token':access_token,'id':artwork_id, 'display':true},
        async: false,
        success: function (data) {
            if(artwork_id != null || artwork_id != 'undefined'){
                var self = Gallery.__container__.lookup('controller:admin_artist');
                self.store.find('artwork',artwork_id).then(function(artwork) {
                    self.store.find('artist',artwork.get('artist_id')).then(function(artist) {
                        if(artist.get('display') == false){
                            artist.set('display',true);
                            artist.save();
                            $('div.admin_artworks_element:has(div#'+artist.id+')').css('color', 'rgba(77,77,77,1)');
                        }
                    });
                    artwork.set('display',true);
                    artwork.save();
                });
                $('.hiddenButton#'+artwork_id).removeClass('hiddenButton').addClass('shownButton');
                $('div.admin_artworks_element:has(div.shownButton#'+artwork_id+')').css('color', 'rgba(77,77,77,1)');
            }        
        },
        error: function(error) {
            if(error.status == 200){
                if(artwork_id != null || artwork_id != 'undefined'){
                    var self = Gallery.__container__.lookup('controller:admin_artist');
                    self.store.find('artwork',artwork_id).then(function(artwork) {
                        self.store.find('artist',artwork.get('artist_id')).then(function(artist) {
                            if(artist.get('display') == false){
                                artist.set('display',true);
                                artist.save();
                                $('div.admin_artworks_element:has(div#'+artist.id+')').css('color', 'rgba(77,77,77,1)');
                            }
                        });
                        artwork.set('display',true);
                        artwork.save();
                    });
                    $('.hiddenButton#'+artwork_id).removeClass('hiddenButton').addClass('shownButton');
                    $('div.admin_artworks_element:has(div.shownButton#'+artwork_id+')').css('color', 'rgba(77,77,77,1)');
                }
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: showArtwork()');
                }else{
                    showError(error.status,error.statusText,'crud.js: showArtwork()');
                }
            }
        }
    });
}
/*##################################*/
/* Send a user message to the admin */
/*##################################*/
function send_message_report(access_token,message){
    $.ajax({
        url: window.serverpath+"/store/send_message.php",
        type: "POST",
        data: {'access_token':access_token,'message':message},
        async: false,
        success: function (data) {
            close_bugReport();
            showInfo(001,'Message sent','crud.js:send_message_report()');
        },
        error: function(error) {
            if(error.status == 200){
                close_bugReport();
                showInfo(001,'Message sent','crud.js:send_message_report()');
            }else{
                if(error.responseText){
                    showError(error.status,error.responseText,'crud.js: send_message_report()');
                }else{
                    showError(error.status,error.statusText,'crud.js: send_message_report()');
                }
            }
        }
    });
}