window.serverpath = 'http://beyondyourgallery.com/admin';
window.current_route = '';
var viewport_width = $( window ).outerHeight(true);
var viewport_height = $( window ).outerHeight(true);

Gallery = Ember.Application.create({
  ready: function(){
    if(localStorage.getItem('userLogged') == "true") {
        window.access_token = localStorage.getItem('accessTok');
        var get_user_response = getUser(window.access_token);
        if((get_user_response == null) || (get_user_response == '')){
            localStorage.setItem('userLogged', false);
            localStorage.removeItem('accessTok');
            localStorage.removeItem('session');
            window.location.reload();
        }else{
            var user_data = JSON.stringify(getUser(window.access_token));
            localStorage.setItem('session', user_data);
            if (window.Worker) {
                $('.gallery_loading').animate({
                    height: viewport_height,
                    opacity: 1
                }, 500, function() {
                    // Animation complete.
                    $('.loader').height(viewport_height/4);
                    $('.gallery_loading').css('display','block');
                });
                window.var_start = new Date();
                var worker_loadGallery = new Worker("js/worker_connection.js");
                worker_loadGallery.postMessage({'access_token':window.access_token,'serverpath':window.serverpath}); 
                worker_loadGallery.onmessage = function (oEvent) {
                    switch(oEvent.data.message) {
                        case 'downloading_session':
                            feedback_loading_session();
                        break;
                        case 'session_downloaded':
                            if(oEvent.data.session != null){
                                Gallery.Session.reopenClass({
                                    FIXTURES: oEvent.data.session
                                });        
                            }
                            feedback_loading_session_complete();
                        break;
                        case 'downloading_artists':
                            feedback_loading_artists();
                        break;
                        case 'artists_downloaded':
                            if(oEvent.data.artists != null){
                                Gallery.Artist.reopenClass({
                                    FIXTURES: oEvent.data.artists
                                });        
                            }
                            feedback_loading_artists_complete();
                        break;
                        case 'downloading_artworks':
                            feedback_loading_artworks();
                        break;
                        case 'artworks_dowloaded':
                            if(oEvent.data.artworks != null){
                                Gallery.Artwork.reopenClass({
                                  FIXTURES: oEvent.data.artworks
                                });        
                            }
                            feedback_loading_artworks_complete();
                        break;
                        case 'downloading_settings':
                            feedback_loading_settings();
                        break;
                        case 'settings_dowloaded':
                            if(oEvent.data.settings != null){
                                Gallery.Setting.reopenClass({
                                    FIXTURES: oEvent.data.settings
                                });        
                            }
                            feedback_loading_settings_complete();
                            feedback_loadGallery_complete();
                            worker_loadGallery.terminate();
                        break;
                    }

                };
            }else{
                console.log('Web Worker functionality not available.');
            }
        }
    }
  }
});

Gallery.ApplicationStore = DS.Store.extend({
    revision: 12,
    adapter: DS.FixtureAdapter.extend({
        queryFixtures: function(fixtures, query, type) {
            return fixtures.filter(function(item) {
                for(prop in query) {
                    if( item[prop] != query[prop]) {
                        return false;
                    }
                }
                return true;
            });
        }
    })
});

Gallery.initializer({
    name: "auth",
    before: ['ember-data'],
    initialize: function (container, application) {
        var auth = Ember.Object.extend({
            isAuthenticated: Em.computed.notEmpty('authToken')
        });
        application.register("my:authToken", auth);
        application.inject("controller", "auth", "my:authToken");
        application.inject("route", "auth", "my:authToken");
        application.inject("store:main", "auth", "my:authToken");
        application.inject("adapter", "auth", "my:authToken");
    }
});

Ember.Router.reopen({
  doSomethingOnUrlChange: function() {
    window.current_route = this.get('url'); 
    if(typeof (window.updateVisuals) == 'function') {
        updateVisuals();
    }
  }.on('didTransition')
});


/*******************************/
function feedback_loading_artists(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#artists_loading_feedbacks").text("Downloading the artists into your gallery...");
    $(".loading_feedbacks_label#artists_loading_feedbacks").css('opacity','1');
}
function feedback_loading_artists_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#artists_loading_feedbacks").text("Artists loaded.");
    $(".loading_feedbacks_label#artists_loading_feedbacks").css('opacity','1');
}
function feedback_loading_artworks(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#artworks_loading_feedbacks").text("Downloading the artworks into your gallery...");
    $(".loading_feedbacks_label#artworks_loading_feedbacks").css('opacity','1');
}
function feedback_loading_artworks_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#artworks_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#artworks_loading_feedbacks").text("Artworks loaded.");
}
function feedback_loading_settings(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#settings_loading_feedbacks").text("Downloading your settings...");
    $(".loading_feedbacks_label#settings_loading_feedbacks").css('opacity','1');
}
function feedback_loading_settings_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#settings_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#settings_loading_feedbacks").text("Settings loaded.");
}
function feedback_loading_session(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#session_loading_feedbacks").text("Downloading your session...");
    $(".loading_feedbacks_label#session_loading_feedbacks").css('opacity','1');
}
function feedback_loading_session_complete(){
    $(".gallery_loading label").css('opacity','0.5');
    $(".loading_feedbacks_label#session_loading_feedbacks").css('opacity','1');
    $(".loading_feedbacks_label#session_loading_feedbacks").text("Session loaded.");
}



function feedback_loggin_complete(){
    $('.gallery_loading').animate({
        opacity: 0
    }, 500, function() {
        $('.gallery_loading label').remove();
        $('.gallery_loading').height(0);
        $('.gallery_loading').css('display','none');
    });
    window.end = new Date();
    var secondsOpen = Math.floor((window.end - window.var_start) / 1000);
    console.log("It took " + secondsOpen + " seconds to load your gallery");
}
function feedback_loadGallery_complete(){
    $('.gallery_loading').animate({
        opacity: 0
    }, 500, function() {
        $('.gallery_loading label').remove();
        $('.gallery_loading').height(0);
        $('.gallery_loading').css('display','none');
    });
    window.end = new Date();
    var secondsOpen = Math.floor((window.end - window.var_start) / 1000);
    console.log("It took " + secondsOpen + " seconds to load your gallery");
    
    var self = Gallery.__container__.lookup('controller:gallery_home');
    self.store.find('session').then(function(session) {
        var username = session.objectAt(0).get('username');
        var gallery_name = session.objectAt(0).get('gallery_name');
        self.transitionToRoute('admin_home',gallery_name,username);
        closeAllError();
    });
}
function feedback_loadGallery_fail(){
    $('.gallery_loading').animate({
        opacity: 0
    }, 500, function() {
        $('.gallery_loading label').remove();
        $('.gallery_loading').height(0);
        $('.gallery_loading').css('display','none');
    });
    window.end = new Date();
    var secondsOpen = Math.floor((window.end - window.var_start) / 1000);
    console.log("The gallery failed to load after " + secondsOpen + " seconds of attempt.");
}