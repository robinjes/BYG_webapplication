/*####  Worker for loading the gallery & Login/Logout ####*/
importScripts('crud.js');
var serverpath;
var access_token;

onmessage = function (oEvent) {
    serverpath = oEvent.data.serverpath;
    access_token = oEvent.data.access_token;
    switch(oEvent.data.message){
        case 'change_frame_section_enable':
            if(oEvent.data.newValue){
                change_frame_section_enable('1');
            }else{
                change_frame_section_enable('0');
            }
        break;
    }
};


/*******************************/
function change_frame_section_enable(value){
    //postMessage({'message':'downloading_session'});
    if((value!=0)&&(value!='0')&&(value!=1)&&(value!='1')){
        postMessage({'message':'error'});
    }else{
        setControl_FrameSection(access_token,serverpath,value);
    }   
}

function confirm_property_modified(){
    postMessage({'message':'property_modified'});
}


function property_modification_failed(){
    postMessage({'message':'property_modification_failed'});
}