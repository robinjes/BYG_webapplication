console.log('APPLICATION IS LOADING');

Gallery.Router.map(function() {
  this.resource('main', { path: '/'}, function() {
    this.resource('admin_home_unlogged', { path: '/home' });
    this.resource('admin_about_unlogged', { path: '/about' });
    this.resource('admin_contact_unlogged', { path: '/contact' });
    this.resource('admin_pricing_unlogged', { path: '/princing' });
    this.resource('connecting', { path: '/connecting/:usernameoremail/:password' });
  });

  this.resource('admin_home', { path: ':gallery_name/:username' }, function() {
    this.resource('admin_artists', { path: '/artists' }, function() {
      this.resource('admin_artist_add', { path: '/add' });
      this.resource('admin_artist', { path: ':artist_id/artist' }, function() {
        this.resource('admin_artist_modification', { path: '/modification' });
        this.resource('admin_artwork_add', { path: '/add' });
        this.resource('admin_artwork', { path: '/artworks/:artwork_id/artwork' }, function() {
          this.resource('admin_artwork_modification', { path: '/modification' });
        });
      });
    });
    this.resource('admin_settings', { path: '/settings'}, function() {
      this.resource('admin_user_modification', { path: '/modification' });
    });
    this.resource('admin_about', { path: '/about' });
    this.resource('admin_analytics', { path: '/analytics' });
    this.resource('missing', { path: "/*path" });
    this.resource('loading_logged', { path: '/loading' });
  });
});

/*##################### ADMINISTRATION #####################*/
Gallery.MainRoute = Ember.Route.extend({
  controllerName: 'main',
  beforeModel : function(){
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('admin_home_unlogged');
    }
  }
});

Gallery.AdminHomeUnloggedRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      
    }
  }
});

Gallery.AdminAboutUnloggedRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      
    }else{
      this.transitionTo('admin_about');
    }
  }
});

Gallery.AdminPricingUnloggedRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  }
});

Gallery.AdminHomeRoute = Ember.Route.extend({
  controllerName: 'admin_home',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  },
  model: function() {
    return this.store.find('session').objectAt(0);
  },
  actions: {
    error: function () {
      console.log('Error with data');
    }
  }
});

Gallery.AdminArtistsRoute = Ember.Route.extend({
  controllerName: 'admin_artists',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }
  },
  model: function() {
    return this.store.find('artist');
  },
  actions: {
    error: function () {
      console.log('Error with data');
      this.transitionTo('admin_artists');
    }
  }
});

Gallery.AdminArtistRoute = Ember.Route.extend({
  controllerName: 'admin_artist',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }else{
      //Already logged
    }
  },
  model: function(params) {
    return this.store.find('artist', params.artist_id);
  },
  actions: {
    error: function () {
      console.log('Error with data');
      this.transitionTo('admin_artists');
    }
  }
});

Gallery.AdminArtworkRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }else{
      //Already logged
    }
  },
  model: function(params) {
    return this.store.find('artwork', params.artwork_id); 
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('admin_artists');
    }
  }
});

Gallery.AdminArtistModificationRoute = Ember.Route.extend({
  controllerName: 'admin_artist_modification'
});

Gallery.AdminArtistAddRoute = Ember.Route.extend({
  controllerName: 'admin_artist_modification',
  model: function() {
    return null;
  },
});

Gallery.AdminArtworkModificationRoute = Ember.Route.extend({
  controllerName: 'admin_artwork_modification'
});

Gallery.AdminArtworkAddRoute = Ember.Route.extend({
  controllerName: 'admin_artwork_modification'
});

Gallery.AdminSettingsRoute = Ember.Route.extend({
  controllerName: 'admin_settings',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }else{
      //Already logged
    }
  },
  model: function() {
    return this.store.find('setting');
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('missing');
    }
  }
});

Gallery.AdminUserModificationRoute = Ember.Route.extend({
  controllerName: 'admin_user_modification',
  beforeModel: function(transition) {
    if((localStorage.getItem('userLogged') == "false") || (localStorage.getItem('userLogged') === null)) {
      this.transitionTo('main');
    }else{
      //Already logged
    }
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('admin_artists');
    }
  }
});

Gallery.MissingRoute = Ember.Route.extend({
  controllerName: 'missing'
});