Gallery.GalleryHomeController = Ember.Controller.extend({
  controllerName: 'gallery_home',
  gallery_name: null,
  gallery_user: null,
  gallery_artist_id: null,
  gallery_artwork_id: null,
  storage_session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('storage_session'),
  setting: function(){
    return this.store.find('setting').then(function(settings) {
      console.log(settings);
      return settings.objectAt(0);
    });
  }.property('setting'),
  init: function(){
    if((this.get('gallery_name') == null)&&(this.get('gallery_user') == null)&&(this.get('gallery_artist_id') == null)&&(this.get('gallery_artwork_id') == null)){
      var self = this;
      this.store.find('setting').then(function(settings) {
        var setting = settings.objectAt(0);
        setting.get('currentArtist').then(function(artist){
          if(artist!=null){
            self.set('gallery_artist_id', artist.get('id'));
          }else{
            self.set('gallery_artist_id', '');
          }
          setting.get('currentArtwork').then(function(artwork){
            if(artwork!=null){
              self.set('gallery_artwork_id', artwork.get('id'));
            }else{
              self.set('gallery_artwork_id', '');
            }
            setting.get('session').then(function(session){
              if(session!=null){
                self.set('gallery_name', session.get('gallery_name'));
                self.set('gallery_user', session.get('username'));
              }else{
                self.set('gallery_name', '');
                self.set('gallery_user', '');
              }
            });
          });
        });
      });
    }
  },
  actions: {
    update_currentId: function(type,id){
      var self = this;
      switch(type){
        case 'artist':
          if(id != null || id != 'undefined'){
            self.store.find('artist',id).then(function(artist) {
              self.store.find('artwork',{'artists_id':id}).then(function(artworks) {
                self.store.find('setting').then(function(settings) {
                  settings.objectAt(0).set('currentArtist',artist);
                  settings.objectAt(0).set('currentArtwork',artworks.objectAt(0));
                  settings.objectAt(0).save();
                });
              });
            });
            this.set('gallery_artist_id',id);
          }
        break;
        case 'artwork':
          if(id != null || id != 'undefined' || id != undefined){
            self.store.find('artwork',id).then(function(artwork) {
              self.store.find('setting').then(function(settings) {
                settings.objectAt(0).set('currentArtwork',artwork);
                settings.objectAt(0).save();
              });
            });
            this.set('gallery_artwork_id',id);
          }
        break;
      }
    },
    init_artist_id: function(){
      if(this.get('gallery_artist_id') == null){
        var self = this;
        this.store.find('setting').then(function(settings) {
          var setting = settings.objectAt(0);
          setting.get('currentArtist').then(function(artist){
              if(artist!=null){
                self.set('gallery_artist_id', artist.get('id'));
              }else{
                self.set('gallery_artist_id', '');
              }
          });
        });
      }
    },
    init_artwork_id: function(){
      if(this.get('gallery_artwork_id') == null){
        var self = this;
        this.store.find('setting').then(function(settings) {
          var setting = settings.objectAt(0);
          setting.get('currentArtwork').then(function(artwork){
              if(artist!=null){
                self.set('gallery_artwork_id', artwork.get('id'));
              }else{
                self.set('gallery_artwork_id', '');
              }
          });
        });
      }
    }
  }
});

Gallery.MainController = Ember.Controller.extend({
  controllerName: 'main',
  needs: 'login',
  session: function(){
    var user_data = null;
    if(localStorage.getItem('session') != null){
      user_data = JSON.parse(localStorage.getItem('session'));
    }
    return user_data;
  }.property('session')
});

Gallery.AdminHomeController = Ember.Controller.extend({
  needs: ['login'],
  userLogged: function(){
    var value = false;
    if(localStorage.getItem('userLogged') == 'true'){
      value = true;
    }else{
      value = false;
    }
    return value;
  }.property('userLogged'),
  storage_session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('storage_session'),
  admin: function(){
    var value = false;
    /*1000=admin*/
    if((parseInt(this.get('storage_session').group)) == 1000){
      value = true;
    }else{
      value = false;
    }
    return value;
  }.property('admin'),
  information: function(){
    var token = localStorage.getItem('accessTok');
    var user_information = getAllInformation(token);
    localStorage.setItem('information', JSON.stringify(user_information));
    return user_information;
  }.property('information'),
  actions: {
    send_message_report: function(){
      var token = localStorage.getItem('accessTok');
      var report_message = $('textarea[name="report_message"').val();
      send_message_report(token,report_message);      
    },
    close_information: function(info_id){
      var token = localStorage.getItem('accessTok');
      closeInfoPopup(info_id);
      deleteInformation(token,info_id);
    }
  }
});

Gallery.LoginController = Ember.Controller.extend({
  controllerName: 'login',
  needs: ['application'],
  actions: {
    /*launch_identification: function(usernameoremail, password){
      if(usernameoremail==null){usernameoremail=''};
      if(password==null){password=''};
      this.transitionToRoute('connecting',usernameoremail, password);
      //this.send('login',usernameoremail, password);
    },*/
    login: function(usernameoremail, password) {
      if(usernameoremail==null){usernameoremail=''};
      if(password==null){password=''};

      if (window.Worker) {
        $('.gallery_loading').animate({
            height: $( window ).height(),
            opacity: 1
        }, 500, function() {
            // Animation complete.
            $('.loader').height($( window ).height()/4);
            $('.gallery_loading').css('display','block');
        });
        var returnValue = initUser(usernameoremail,password);
        if(returnValue == null){
          feedback_loadGallery_fail();
          this.transitionToRoute('admin_home_unlogged');
          showError('007','Wrong username or password. Try again.','LoginController: login action');
        }else{
          if(typeof(Storage) !== 'undefined') {
            localStorage.setItem('userLogged', true);
            localStorage.setItem('accessTok', returnValue.accessTok);
            var user_data = JSON.stringify(getUser(returnValue.accessTok));
            localStorage.setItem('session', user_data);
          } else {
            //Use cookies
            alert('Sorry! No Web Storage support...');
          }
          var access_token = localStorage.getItem('accessTok');
          window.var_start = new Date();
          var worker_controller_loadGallery = new Worker("js/worker_connection.js");
          worker_controller_loadGallery.postMessage({'serverpath':window.serverpath,'access_token':access_token});
          var self = this;
          worker_controller_loadGallery.onmessage = function (oEvent) {
            switch(oEvent.data.message) {
              case 'downloading_session':
                  feedback_loading_session();
              break;
              case 'session_downloaded':
                  if(oEvent.data.session != null){
                      Gallery.Session.reopenClass({
                          FIXTURES: oEvent.data.session
                      });        
                  }
                  feedback_loading_session_complete();
              break;
              case 'downloading_artists':
                  feedback_loading_artists();
              break;
              case 'artists_downloaded':
                  if(oEvent.data.artists != null){
                      Gallery.Artist.reopenClass({
                          FIXTURES: oEvent.data.artists
                      });        
                  }
                  feedback_loading_artists_complete();
              break;
              case 'downloading_artworks':
                  feedback_loading_artworks();
              break;
              case 'artworks_dowloaded':
                  if(oEvent.data.artworks != null){
                    Gallery.Artwork.reopenClass({
                      FIXTURES: oEvent.data.artworks
                    });        
                  }
                  feedback_loading_artworks_complete();
              break;
              case 'downloading_settings':
                  feedback_loading_settings();
              break;
              case 'settings_dowloaded':
                  if(oEvent.data.settings != null){
                    Gallery.Setting.reopenClass({
                        FIXTURES: oEvent.data.settings
                    });        
                  }
                  feedback_loading_settings_complete();
                  self.store.find('session').then(function(session) {
                      var username = session.objectAt(0).get('username');
                      var gallery_name = session.objectAt(0).get('gallery_name');
                      self.transitionToRoute('admin_home',gallery_name,username);
                      closeAllError();
                  });
                  feedback_loggin_complete();
              break;
            }
          };
        }
      }
    },
    logout: function(concernedController) {
      var access_token = localStorage.getItem('accessTok');
      removeTokenInDB(access_token);
      localStorage.setItem('userLogged', false);
      localStorage.removeItem('accessTok');
      localStorage.removeItem('session');
      window.location.reload();
    },
    toGallery: function(){
      window.open("http://beyondyourgallery.com/gallery");
    }
  }
});

Gallery.AdminArtistsController = Ember.Controller.extend({
  controllerName: 'admin_artists',
  needs: ['application', 'admin_artist_modification'],
  accessTok: function(){
    return localStorage.getItem('accessTok');
  }.property('accessTok'),
  session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('session'),
  actions: {
    hideShow_admin_artist: function(artist_status,artist_id){
      var access_token = localStorage.getItem('accessTok');
      switch(artist_status){
        case true:
          //Turn to false (hidden)
          hideArtist(access_token,artist_id);
        break;
        case false:
          //Turn to true (shown)
          showArtist(access_token,artist_id);
        break;
      }
    }
  },
});

Gallery.AdminArtistModificationController = Ember.Controller.extend({
  controllerName: 'admin_artist_modification',
  needs: ['application'],
  countries: getCountries(),
  userHasEnteredData: false,
  artist: null,
  artistName: null,
  artistLocation: null,
  artistDescription: null,
  artwork: null,
  actions: {
    open_admin_artist_modification: function(artist){
      this.set('artist',artist);
      this.transitionToRoute('admin_artist', artist);
      this.transitionToRoute('admin_artist_modification');
    },
    cancel_modification_artist: function(){
      //Open the cancel_modification_artist Confirmation Box
      if(this.get('userHasEnteredData') === true){
        showCloseConfirmationBox('admin_artist_modification');
      }else{
        this.send('leave_artistModification');
      }
    },
    cancel_add_artist: function(){
      //Open the cancel_modification_artist Confirmation Box
      if(this.get('userHasEnteredData') === true){
        showCloseConfirmationBox('admin_artist_add');
      }else{
        this.send('leave_artistModification');
      }
    },
    leave_artistModification: function(artist){
      hideConfirmationBox();
      if(artist != null){
        artist.rollback();
      }
      this.transitionToRoute('admin_artists');
      this.set('userHasEnteredData',false);
      this.set('artist',null);
      this.set('artistName',null);
      this.set('artistLocation',null);
      this.set('artistDescription',null);
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    validate_modification_artist: function(){
      //Open the validate_modification_artist Confirmation Box
      this.set('artistName',this.get('artist.name'));
      this.set('artistLocation',this.get('artist.location'));
      this.set('artistDescription',this.get('artist.description'));
      showModificationConfirmationBox('admin_artist_modification');
    },
    save_artistModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var artist_id = this.get('artist').id;
      var name = this.get('artistName');
      var location = this.get('artistLocation');
      var description = this.get('artistDescription');
      setArtist(access_token,artist_id,name,location,description);
      hideConfirmationBox();
      this.set('userHasEnteredData',false);
      this.set('artist',null);
      this.set('artistName',null);
      this.set('artistLocation',null);
      this.set('artistDescription',null);
    },
    open_admin_artist_add: function(){
      this.transitionToRoute('admin_artist_add');
    },
    validate_add_artist: function(name,location,description){
      this.set('artistName',name);
      this.set('artistLocation',location);
      this.set('artistDescription',description);
      showModificationConfirmationBox('admin_artist_add');
    },
    add_artist: function(){
      var access_token = localStorage.getItem('accessTok');
      var name = this.get('artistName');
      var location = this.get('artistLocation');
      var description = this.get('artistDescription');
      var self = this;
      addArtist(access_token,name,location,description);
      hideConfirmationBox();
      this.set('userHasEnteredData',false);
      this.set('artist',null);
      this.set('artistName',null);
      this.set('artistLocation',null);
      this.set('artistDescription',null);
      this.transitionToRoute('admin_artists');
    },
    removeArtistPage: function(artist){
      this.set('artist',artist);
      showRemoveConfirmationBox_artist();
    },
    remove_artist: function(){
      var access_token = localStorage.getItem('accessTok');
      var artistId = this.get('artist').id;
      removeArtist(access_token,artistId);
      hideConfirmationBox();
      this.set('userHasEnteredData',false);
      this.set('artist',null);
      this.set('artistName',null);
      this.set('artistLocation',null);
      this.set('artistDescription',null);
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
      this.transitionToRoute('admin_artists');
    }
  }
});

Gallery.AdminArtistController = Ember.ObjectController.extend({
  controllerName: 'admin_artist',
  needs: ['admin_artwork_modification', 'admin_artist_modification'],
  actions: {
    hideShow_admin_artwork: function(artwork_status,artwork_id){
      var access_token = localStorage.getItem('accessTok');
      switch(artwork_status){
        case true:
          //Turn to false (hidden)
          hideArtwork(access_token,artwork_id);
        break;
        case false:
          //Turn to true (shown)
          showArtwork(access_token,artwork_id);
        break;
      }
    },
    open_admin_artwork_modification: function(artwork){
      this.set('controllers.admin_artwork_modification.artwork', artwork);
      this.transitionToRoute('admin_artwork',artwork);
      this.transitionToRoute('admin_artwork_modification');
    },
    remove_artwork_page: function(artwork){
      this.set('artwork',artwork);
      showRemoveConfirmationBox_artwork();
    },
    remove_artwork: function(){
      var access_token = localStorage.getItem('accessTok');
      var artworkId = this.get('artwork').id;
      removeArtwork(access_token,artworkId);
      hideConfirmationBox();
    }
  }
});

Gallery.AdminArtworkController = Ember.ObjectController.extend({
  controllerName: 'admin_artwork',
  needs: ['admin_artwork_modification', 'admin_artist_modification'],
  actions: {
    open_admin_artwork_modification: function(artwork){
      this.set('controllers.admin_artwork_modification.artwork', artwork);
      this.transitionToRoute('admin_artwork',artwork);
      this.transitionToRoute('admin_artwork_modification');
    },
    remove_artwork_page: function(artwork){
      this.set('artwork',artwork);
      showRemoveConfirmationBox_artwork();
    },
    remove_artwork: function(){
      var access_token = localStorage.getItem('accessTok');
      var artworkId = this.get('artwork').id;
      removeArtwork(access_token,artworkId);
      hideConfirmationBox();
    }
  }
});

Gallery.AdminArtworkModificationController = Ember.Controller.extend({
  controllerName: 'admin_artwork_modification',
  needs: ['admin_artwork_modification'],
  techniques: getTechniques(),
  userHasEnteredData: false,
  artwork: null,
  artworkTitle: null,
  artworkArtistId: null,
  artworkYear: null,
  artworkDimHorizon: null,
  artworkDimVertica: null,
  artworkDescription: null,
  artworkTechnique: null,
  actions: {
    cancel_modification_artwork: function(){
      //Open the cancel_modification_artist Confirmation Box
      if(this.get('userHasEnteredData') === true){
        showCloseConfirmationBox('admin_artwork_modification');
      }else{
        this.send('leave_artworkModification');
      }
    },
    cancel_add_artwork: function(){
      //Open the cancel_modification_artist Confirmation Box
      if(this.get('userHasEnteredData') === true){
        showCloseConfirmationBox('admin_artwork_add');
      }else{
        this.send('leave_artworkModification');
      }
    },
    leave_artworkModification: function(artwork){
      hideConfirmationBox();
      if(artwork != null){
        artwork.rollback();
      }
      this.transitionToRoute('admin_artwork',artwork);
      this.set('userHasEnteredData',false);
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    validate_modification_artwork: function(){
      //Open the validate_modification_artist Confirmation Box
      this.set('artworkTitle',this.get('artwork.title'));
      this.set('artworkArtistId',this.get('artwork.artist_id'));
      this.set('artworkYear',this.get('artwork.year'));
      this.set('artworkDimHorizon',this.get('artwork.dim_horizon'));
      this.set('artworkDimVertica',this.get('artwork.dim_vertica'));
      this.set('artworkDescription',this.get('artwork.description'));
      this.set('artworkTechnique',this.get('artwork.technique'));
      showModificationConfirmationBox_artwork('admin_artwork_modification');
    },
    save_artworkModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var artworkId = this.get('artwork').id;
      var title = this.get('artworkTitle');
      var year = this.get('artworkYear');
      var dim_horizon = this.get('artworkDimHorizon');
      var dim_vertica = this.get('artworkDimVertica');
      var description = this.get('artworkDescription');
      var technique = this.get('artworkTechnique');
      setArtwork(access_token,artworkId,title,year,technique,dim_horizon,dim_vertica);
      hideConfirmationBox();
      this.set('userHasEnteredData',false);
    },
    open_admin_artwork_add: function(artist){
      this.set('artworkArtistId',artist.id);
      this.transitionToRoute('admin_artist', artist);
      this.transitionToRoute('admin_artwork_add');
    },
    validate_add_artwork: function(title,year,dim_horizon,dim_vertica,description){
      this.set('artworkTitle',title);
      this.set('artworkYear',year);
      this.set('artworkDimHorizon',dim_horizon);
      this.set('artworkDimVertica',dim_vertica);
      this.set('artworkDescription',description);
      this.set('artworkTechnique',description);
      showModificationConfirmationBox_artwork('admin_artwork_add');
    },
    add_artwork: function(){
      var access_token = localStorage.getItem('accessTok');
      var title = this.get('artworkTitle');
      var artistId = this.get('artworkArtistId');
      var year = this.get('artworkYear');
      var dim_horizon = this.get('artworkDimHorizon');
      var dim_vertica = this.get('artworkDimVertica');
      var description = this.get('artworkDescription');
      var technique = this.get('artworkTechnique');
      var self = this;
      addArtwork(access_token,artistId,title,year,technique,dim_horizon,dim_vertica,description);
      hideConfirmationBox();
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
    }
  }
});

Gallery.AdminUserModificationController = Ember.Controller.extend({
  controllerName: 'admin_user_modification',
  needs: ['admin_settings','login'],
  countries: getCountries(),
  userHasEnteredData: false,
  storage_session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('storage_session'),
  name: null,
  username: null,
  gallery_name: null,
  gallery_location: null,
  pass: null,
  actions: {
    validate_modification_user: function(name,username,gallery_name,location){
        this.set('name',name);
        this.set('username',username);
        this.set('gallery_name',gallery_name);
        this.set('gallery_location',location);
        showModificationConfirmationBox_user();
    },
    cancel_modification_user: function(){
      if(this.get('userHasEnteredData') === true){
        showCancelConfirmationBox_user();
      }else{
        this.send('leave_userModification');
      }
    },
    leave_userModification: function(){
      hideConfirmationBox();
      this.transitionToRoute('admin_settings');
      this.set('userHasEnteredData',false);
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
    },
    save_userModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var name = this.get('name');
      var username = this.get('username');
      var gallery_name = this.get('gallery_name');
      var gallery_location = this.get('gallery_location');
      setUser(access_token,name,username,gallery_name,gallery_location);
      hideConfirmationBox();
      this.set('userHasEnteredData',false);
      this.transitionToRoute('admin_settings');
      window.location.reload();
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    open_admin_user_pass_modification: function(){
      showModificationBox_password();
    },
    validate_modification_password: function(){
      var password = this.get('password');
      var confirm_password = this.get('confirm_password');
      if(password == confirm_password){
        this.set('pass',password);
        showModificationConfirmationBox_password();      
      }else{
        showError('007','Something went wrong with your new password. Try again.','AdminUserModificationController: validate modification password action');
      }
    },
    save_password: function(){
      var access_token = localStorage.getItem('accessTok');
      var pass = this.get('pass');
      setPass(access_token,pass);
      hideConfirmationBox();
      this.get('controllers.login').send('logout',this);
    },
  }
});

Gallery.AdminSettingsController = Ember.Controller.extend({
  controllerName: 'admin_settings',
  needs: [],
  fieldName: null,
  storage_session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('storage_session'),
  actions: {
    open_admin_user_modification: function(){
      this.transitionToRoute('admin_user_modification');
    },
    open_admin_setting_modification: function(field_cat,field_title,field_name,field_value,field_unit){
      this.set('fieldName',field_name);
      showModifySettingBox(field_cat,field_title,field_value,field_unit);
    },
    validate_modify_setting: function(){
      var token = localStorage.getItem('accessTok');
      var field_name = this.get('fieldName');
      var new_field_value = getNewFieldValue(field_name);
      setSetting(token,field_name,new_field_value);
      hideConfirmationBox();
    },
    cancel_modify_setting: function(){
      hideConfirmationBox();
    }
  }
});
Gallery.MissingController = Ember.Controller.extend({
  controllerName: 'missing',
  needs: ['login']
});