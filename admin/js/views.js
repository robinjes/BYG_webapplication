Gallery.MainView = Ember.View.extend({
	templateName: 'main',
	rightLogin: "Congratulation! You successfully logged!",
	wrongLogin: "Wrong username or password. Try again!"
});

Gallery.AdminHomeUnloggedView = Ember.View.extend({
	templateName: 'admin_home_unlogged',
	didInsertElement: function(){
		publicView();
	}
});


Gallery.AdminArtistsView = Ember.View.extend({
	templateName: 'admin_artists',
	didInsertElement: function(){
		setArtistPart_Sizes();
	}
});

Gallery.AdminArtistView = Ember.View.extend({
	templateName: 'admin_artist',
	didInsertElement: function(){
		setArtistPart_Sizes();
		add_Artist_Img_UploadButtonListener();
	}
});

Gallery.AdminArtworkView = Ember.View.extend({
	templateName: 'admin_artwork',
	didInsertElement: function(){
		setArtistPart_Sizes();
		add_Artwork_Img_UploadButtonListener();
	}
});

Gallery.AdminSettingsView = Ember.View.extend({
	templateName: 'admin_settings',
	didInsertElement: function(){
		setSettingPart_Sizes();
	}
});

Gallery.AdminAboutView = Ember.View.extend({
	templateName: 'admin_about',
	didInsertElement: function(){
		setAboutPart_Sizes();
	}
});

Gallery.AdminArtistModificationView = Ember.View.extend({
	templateName: 'admin_artist_modification',
	didInsertElement: function(){
		setArtistModificationPart_Sizes();
	}
});

Gallery.AdminArtistAddView = Ember.View.extend({
	templateName: 'admin_artist_add',
	didInsertElement: function(){
		setArtistModificationPart_Sizes();
	}
});

Gallery.AdminArtworkAddView = Ember.View.extend({
	templateName: 'admin_artwork_add',
	didInsertElement: function(){
		setArtworkModificationPart_Sizes();
	}
});

Gallery.AdminArtworkModificationView = Ember.View.extend({
	templateName: 'admin_artwork_modification',
	didInsertElement: function(){
		setArtworkModificationPart_Sizes();
	}
});

Gallery.AdminUserModificationView = Ember.View.extend({
	templateName: 'admin_user_modification',
	didInsertElement: function(){
		setUserModificationPart_Sizes();
	}
});