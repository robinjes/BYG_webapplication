/*####  Worker for loading the gallery & Login/Logout ####*/
importScripts('crud.js');
var serverpath;

onmessage = function (oEvent) {
    serverpath = oEvent.data.serverpath;
    load_session(oEvent.data.access_token);
};


/*******************************/
function load_session(access_token){
    postMessage({'message':'downloading_session'});
    var session = getSession(access_token,serverpath);
}
function load_session_inApp(access_token,session_data){
    postMessage({'message':'session_downloaded','session':session_data});
    load_artists(access_token);
}

function load_artists(access_token){
	postMessage({'message':'downloading_artists'});
    var artists = getAllArtists(access_token,serverpath);
}
function load_artist_inApp(access_token,artists_data){
    postMessage({'message':'artists_downloaded','artists':artists_data});
    if(artists_data!=null){
        load_artworks(access_token);   
    }else{
        //If no artists = no artworks; so, directly load settings
        load_settings(access_token);
    }
    
}

function load_artworks(access_token){
	postMessage({'message':'downloading_artworks'});
    var artworks = getAllArtworks(access_token,serverpath);
}
function load_artworks_inApp(access_token,artworks_data){
    postMessage({'message':'artworks_downloaded','artworks':artworks_data});
    load_settings(access_token);
}

function load_settings(access_token){
	postMessage({'message':'downloading_settings'});
    var settings = getAllSettings(access_token,serverpath);
}
function load_settings_inApp(access_token,settings_data){
    postMessage({'message':'settings_downloaded','settings':settings_data});
    load_control(access_token)
}
function load_control(access_token){
    postMessage({'message':'downloading_control'});
    var control = getControl(access_token,serverpath);
}
function load_control_inApp(access_token,control_data){
    postMessage({'message':'control_downloaded','control':control_data});
}