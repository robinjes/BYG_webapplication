/*####  Worker for any image treatments of the showroom ####*/
onmessage = function (oEvent) {
	switch(oEvent.data.message){
		case 'home_image_selection':
			var size = resize_homeSelection_img(oEvent.data.container_width_px,oEvent.data.container_height_px,oEvent.data.img_width,oEvent.data.img_height);
			postMessage({'message':'home_image_selection_response','new_img_width':size[0],'new_img_height':size[1],'marginTop_img':size[2]}); 
		break;
		case 'artwork_image_wh':
			var artwork_array_whsm = resize_artwork_img(oEvent.data.container_width_px,oEvent.data.container_height_px,oEvent.data.artwork_array_wh,
				oEvent.data.projection_area_height_px,oEvent.data.projection_area_height_cm,oEvent.data.projection_area_width_px,oEvent.data.projection_area_width_cm);
			postMessage({'message':'artwork_image_whsm','artwork_array_whsm':artwork_array_whsm}); 
		break;
		case 'noArtwork_image_wh':
			var noArtwork_array_whm = adaptArtworkImage(oEvent.data.container_width_px,oEvent.data.container_height_px,oEvent.data.noArtwork_array_wh);
			postMessage({'message':'noArtwork_image_whm','noArtwork_array_whm':noArtwork_array_whm});
		break;
		case 'artist_image_wh':
			var artist_array_whm = adaptArtistImage(oEvent.data.container_width_px,oEvent.data.container_height_px,oEvent.data.artist_array_wh);
			postMessage({'message':'artist_image_whm','artist_array_whm':artist_array_whm});
		break;
	}
};
/*###########################################################################*/
/* Resize the image of the currently selected artwork or artist */
/* Input: homeSelection div width & height and original image width & height */
/* Ouput: an array of the new image width and height */
/*###########################################################################*/
function resize_homeSelection_img(container_width_px,container_height_px,img_width,img_height){
	var container_height_px_backup = container_height_px;
	var container_height_px = container_height_px * 80/100;
	var container_width_px = container_width_px * 80/100;
	var new_img_width = 10;
	var new_img_height = 10;
	//image shape?
	if(img_width>=img_height){
		//landscape or square
		new_img_width = container_width_px;
		new_img_height = new_img_width * img_height / img_width;
		if(new_img_height>container_height_px){
			new_img_height = container_height_px;
			new_img_width = img_width * new_img_height / img_height;
		}
	}else{
		//portrait
		new_img_height = container_height_px;
		new_img_width = img_width * new_img_height / img_height;
		if(new_img_width>container_width_px){
			new_img_width = container_width_px;
			new_img_height = new_img_width * img_height / img_width;
		}
	}
	var marginTop_img = (container_height_px_backup - new_img_height) / 2;
	var rightSide_height = new_img_height*136.46/100;
	var marginTop_rightSide = (container_height_px - rightSide_height) / 2.5;
	var size = new Array(new_img_width,new_img_height,marginTop_img);
	return size;
}
/*###########################################################################*/
/* Resize all artists' images depending on the container size */
/* Input: Container width & height, images width & height in centimeter */
/* Ouput: an array of the new image width, height in pixel and scale */
/*###########################################################################*/
function resize_artwork_img(container_width_px, container_height_px, artwork_array_wh, area_height_px, area_height_cm, area_width_px, area_width_cm){
	var img_width_cm = artwork_array_wh[0];
	var img_height_cm = artwork_array_wh[1];
	var imgOrientation = get_ImgOrientation(img_width_cm,img_height_cm,area_width_px,area_width_cm);
	var scale = set_ImgScale(container_width_px, container_height_px, imgOrientation,img_width_cm,img_height_cm, area_height_px, area_height_cm, area_width_px, area_width_cm);
	var image_sizes = reScaleImg(container_width_px, container_height_px, scale,imgOrientation,img_width_cm,img_height_cm,area_height_px,area_height_cm,area_width_px, area_width_cm);
	var image_width_px = get_ImgWidth_px(image_sizes[0],area_width_px,area_width_cm);
	var image_height_px = get_ImgHeight_px(image_sizes[1],area_height_px,area_height_cm);
	var margin = (container_height_px - image_height_px) / 2;
	var array_whsm = new Array(image_width_px,image_height_px,scale,margin);
	return array_whsm;
}
function get_ImgOrientation(width_cm, height_cm, screen_img_area_height_px, screen_img_area_width_px,area_height_px,area_height_cm, area_width_px, area_width_cm){
	var img_width_cm = parseFloat(width_cm);
	var img_height_cm = parseFloat(height_cm);
	var horizontal_image = null;
	var square_image = null;
	var larger_than_screen = null;
	var higher_than_screen = null;

	var screen_img_area_height_cm = getScreenElementHeight_cm(screen_img_area_height_px,area_height_px,area_height_cm);
	var screen_img_area_width_cm = getScreenElementWidth_cm(screen_img_area_width_px,area_width_px, area_width_cm);

	if(img_width_cm === img_height_cm){
		square_image = true;
		horizontal_image = false;
	}else{
		if(img_width_cm > img_height_cm){
			square_image = false;
			horizontal_image = true;			
		}else{
			square_image = false;
			horizontal_image = false;				
		}
	}

	if((img_width_cm >= screen_img_area_width_cm) && (img_height_cm >= screen_img_area_height_cm)){
		larger_than_screen = true;
		higher_than_screen = true;		
	}else{
		if((img_width_cm < screen_img_area_width_cm) && (img_height_cm < screen_img_area_height_cm)){
			larger_than_screen = false;
			higher_than_screen = false;
		}else{
			if(img_width_cm >= screen_img_area_width_cm){
				larger_than_screen = true;
				higher_than_screen = false;				
			}else{
				larger_than_screen = false;
				higher_than_screen = true;							
			}
		}
	}

	var imgOrientation = new Array(horizontal_image,square_image,larger_than_screen,higher_than_screen);
	return imgOrientation;
}
/*#################################################################################################################################################*/
/* Set Image scale */
/* Input: container width & height, array of booleans with horizontal_image, square_image, larger_than_screen, higher_than_screen & image width & image height in centimeter */
/* Ouput: a scale of integer type */
/*#################################################################################################################################################*/
function set_ImgScale(container_width_px, container_height_px, imgOrientation, width_cm, height_cm, area_height_px, area_height_cm, area_width_px, area_width_cm){
	var img_width_cm = parseFloat(width_cm);
	var img_height_cm = parseFloat(height_cm);
	var value = String(imgOrientation[0]) + '-' + String(imgOrientation[1]) + '-' + String(imgOrientation[2]) + '-' + String(imgOrientation[3]);
	var scale = 1;
	var screen_img_area_height_cm = getScreenElementHeight_cm(container_height_px,area_height_px,area_height_cm);
	var screen_img_area_width_cm = getScreenElementWidth_cm(container_width_px, area_width_px, area_width_cm);
	switch(value){
		case 'false-false-false-true':
			scale = Math.ceil(img_height_cm/screen_img_area_height_cm);
		break;
		case 'false-false-true-false':
			scale = Math.ceil(img_height_cm/screen_img_area_height_cm);
		break;
		case 'true-false-false-true':
			scale = Math.ceil(img_width_cm/screen_img_area_width_cm);
		break;
		case 'true-false-true-false':
			scale = Math.ceil(img_width_cm/screen_img_area_width_cm);
		break;
		case 'false-true-true-true':
			scale = Math.ceil(img_height_cm/screen_img_area_height_cm);
			if((img_width_cm/scale)>screen_img_area_width_cm){
				scale = Math.ceil(img_width_cm/screen_img_area_width_cm);
			}
		break;
		case 'true-false-true-true':
			scale = Math.ceil(img_width_cm/screen_img_area_width_cm);
			if((img_height_cm/scale)>screen_img_area_height_cm){
				scale = Math.ceil(img_height_cm/screen_img_area_height_cm);
			}
		break;
		case 'false-false-true-true':
			scale = Math.ceil(img_height_cm/screen_img_area_height_cm);
			if((img_width_cm/scale)>screen_img_area_width_cm){
				scale = Math.ceil(img_width_cm/screen_img_area_width_cm);
			}
		break;		
	}
	if(scale < 1 || scale == Infinity){
		scale = 1;
		showError('007','Scale result is less than 1 or infinit','gallery.js:set_ImgScale()');
	}
	return scale;
}
/*#######################################################################################*/
/* ReScale Image */
/* Input: a scale & image width & image height in centimeter */
/* Ouput: an array of decimal type containing the new image width & the new image height */
/*#######################################################################################*/
function reScaleImg(container_width_px, container_height_px, scale,imgOrientation,width_cm,height_cm,area_height_px,area_height_cm,area_width_px, area_width_cm){
	var img_width_cm = parseFloat(width_cm);
	var img_height_cm = parseFloat(height_cm);
	var value = String(imgOrientation[0]) + '-' + String(imgOrientation[1]) + '-' + String(imgOrientation[2]) + '-' + String(imgOrientation[3]);
	var new_img_width_cm = 0;
	var new_img_height_cm = 0;
	var screen_img_area_height_cm = getScreenElementHeight_cm(container_height_px,area_height_px,area_height_cm);
	var screen_img_area_width_cm = getScreenElementWidth_cm(container_width_px,area_width_px, area_width_cm);

	switch(value){
		case 'false-false-false-true':
			new_img_height_cm = img_height_cm/scale;
			new_img_width_cm = 'auto';
		break;
		case 'false-false-true-false':
			new_img_height_cm = img_height_cm/scale;
			new_img_width_cm = 'auto';
		break;
		case 'false-true-true-true':
			new_img_height_cm = img_height_cm/scale;
			new_img_width_cm = 'auto';
		break;
		case 'true-false-false-true':
			new_img_height_cm = 'auto';
			new_img_width_cm = img_width_cm/scale;
		break;
		case 'true-false-true-false':
			new_img_height_cm = 'auto';
			new_img_width_cm = img_width_cm/scale;
		break;
		case 'true-false-true-true':
			new_img_height_cm = 'auto';
			new_img_width_cm = img_width_cm/scale;
		break;
		case 'false-false-true-true':
			new_img_height_cm = img_height_cm/scale;
			new_img_width_cm = 'auto';
		break;
		default:
			new_img_height_cm = height_cm;
			new_img_width_cm = width_cm;
		break;
	}
	var img_sizes = new Array(new_img_width_cm,new_img_height_cm);
	return img_sizes;
}
/*#######################################################################################*/
/* Adapt 'artwork no-picture' or 'no-artwork' image with the given calculated dimensions */
/* Input: container width & height, image width & image height in pixels */
/* Ouput: image width & image height in pixels */
/*#######################################################################################*/
function adaptArtworkImage(container_width_px,container_height_px,noArtwork_array_wh){
	var container_width_px = container_width_px;
	var container_height_px = container_height_px;
	var image_width_px = noArtwork_array_wh[0];
	var image_height_px = noArtwork_array_wh[1];
	var scale = 90/100;
	if(image_width_px <= image_height_px){
		//Portrait or square image
		var old_height = image_height_px;
		image_height_px = container_height_px * scale;
		image_width_px = image_height_px * image_width_px / old_height;
		
	}else{
		//Landscape image
		var old_width = image_width_px;
		image_width_px = container_width_px * scale;
		image_height_px = image_width_px * image_height_px / old_width;
	}
	var margin = (container_height_px - image_height_px) / 2;
	var noArtwork_array_whm = new Array(image_width_px,image_height_px,margin);
	return noArtwork_array_whm;
}
/*#####################################################################################*/
/* Adapt 'artist no-picture' or 'no-artist' image with the given calculated dimensions */
/* Input: image width & image height in pixels */
/* Ouput: image width & image height in pixels */
/*#####################################################################################*/
function adaptArtistImage(container_width_px,container_height_px,artist_array_wh){
	var container_width_px = container_width_px;
	var container_height_px = container_height_px;
	var image_width_px = artist_array_wh[0];
	var image_height_px = artist_array_wh[1];
	var scale = 90/100;
	if(container_width_px <= container_height_px){
		//Portrait or square container
		image_width_px = container_width_px * scale;
		image_height_px = img_width;
		
	}else{
		//Landscape container
		image_height_px = container_height_px * scale;
		image_width_px = image_height_px;
		
	}
	var margin = (container_height_px - image_height_px) / 2;
	var artist_array_whm = new Array(image_width_px,image_height_px,margin);
	return artist_array_whm;
}
/*###################################*/
/* Get image height in pixels */
/* Input: image height in centimeter */
/* Ouput: image height in pixels */
/*###################################*/
function get_ImgHeight_px(height_cm,area_height_px,area_height_cm){
	var imageHeight_px = '';
	if(String(height_cm) == 'auto'){
		imageHeight_px = 'auto';
	}else{
		var imageHeight_cm = parseFloat(height_cm);
		imageHeight_px = imageHeight_cm * area_height_px / area_height_cm;
	}
	return imageHeight_px;
}
/*##################################*/
/* Get image width in pixels */
/* Input: image width in centimeter */
/* Ouput: image width in pixels */
/*##################################*/
function get_ImgWidth_px(width_cm,area_width_px,area_width_cm){
	var imageWidth_px = '';
	if(String(width_cm) == 'auto'){
		imageWidth_px = 'auto';
	}else{
		var imageWidth_cm = parseFloat(width_cm);
		imageWidth_px = imageWidth_cm * area_width_px / area_width_cm;
	}
	return imageWidth_px;
}
/*#####################################*/
/* Get element height in centimeter */
/* Input: element height in pixels */
/* Ouput: element height in centimeter */
/*#####################################*/
function getScreenElementHeight_cm(screenElementHeight_px,area_height_px,area_height_cm){
	var screenElementHeight_cm = screenElementHeight_px * area_height_cm / area_height_px;
	return screenElementHeight_cm;
}
/*####################################*/
/* Get element width in centimeter */
/* Input: element width in pixels */
/* Ouput: element width in centimeter */
/*####################################*/
function getScreenElementWidth_cm(width_px,area_width_px, area_width_cm){
	var screenElementWidth_px = parseFloat(width_px);
	var screenElementWidth_cm = screenElementWidth_px * area_width_cm / area_width_px;
	return screenElementWidth_cm;
}