//Artists
Gallery.Artist = DS.Model.extend({
  slug: DS.attr('string'),
  name: DS.attr('string'),
  imgName: DS.attr('string'),
  imgPath: DS.attr('string'),
  location: DS.attr('string'),
  description: DS.attr('string'),
  artworks: DS.hasMany('artwork', {async: true}),
  artworksQty: DS.attr('string')
});
Gallery.Artist.FIXTURES = [];
//Artworks
Gallery.Artwork = DS.Model.extend({
  artist: DS.belongsTo('artist', {async: true}),
  title: DS.attr('string'),
  year: DS.attr('string'),
  dim_horizon: DS.attr('string'),
  dim_vertica: DS.attr('string'),
  description: DS.attr('string'),
  imgName: DS.attr('string'),
  imgPath: DS.attr('string'),
  artist_id: DS.attr('string')
});
Gallery.Artwork.FIXTURES = [];
//Settings
Gallery.Setting = DS.Model.extend({
  currentArtist: DS.belongsTo('artist', {async: true}),
  currentArtwork: DS.belongsTo('artwork', {async: true}),
  session: DS.belongsTo('session', {async: true}), 
  resolutionPx_horizon: DS.attr('string'),
  resolutionPx_vertical: DS.attr('string'),
  projectionCm_horizon: DS.attr('string'),
  projectionCm_vertical: DS.attr('string'),
  directory_allowed_space: DS.attr('string'),
  directory_used_space: DS.attr('string'),
  free_space: function() {
    return this.get('directory_allowed_space') - this.get('directory_used_space');
  }.property('free_space'),
  auto_slideshow_activate: DS.attr('string'),
  auto_slideshow_time: DS.attr('string'),
  selection_countdown_time: DS.attr('string')
});
Gallery.Setting.FIXTURES = [];
//Control
Gallery.Control = DS.Model.extend({
  frame_section_enable: DS.attr('string'),
  artworks_tag_enable: DS.attr('string')
});
Gallery.Control.FIXTURES = [];
//Session
Gallery.Session = DS.Model.extend({
  gallery_name: DS.attr('string'),
  username: DS.attr('string'),
});
Gallery.Session.FIXTURES = [];
