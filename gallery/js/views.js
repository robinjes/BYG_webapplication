Gallery.GalleryHomeView = Ember.View.extend({
	templateName: 'gallery_home',
	didInsertElement: function(){
		setHomePart_Sizes();
		set_viewportSizes();
	}
});

Gallery.GalleryArtistsView = Ember.View.extend({
	templateName: 'gallery_artists',
    oneArrayWithIndices: function() {
      return this.get('oneArray').map(function(item, index) {
        return {item: i, index: idx};
      });
    }.property('oneArray.@each'),
	didInsertElement: function(){
		cleanCarousel_artists();
		setArtistsPart_Sizes();
		init_carousel_artists();
		configureCarousel_artists();
		setArtistsCarousel_visuals();
		setArtistsPart_Events();
		set_viewportSizes();
	}
});

Gallery.GalleryArtistView = Ember.View.extend({
	templateName: 'gallery_artist',
	didInsertElement: function(){
		cleanCarousel_artworks();
		setArtworksPart_Sizes();
		init_carousel_artworks();
		configureCarousel_artworks();
		update_ArtworksImgs();
		set_ArtworksArrows();
		setArtworksPart_Events();
		set_viewportSizes();
	}
});

Gallery.GalleryFrameView = Ember.View.extend({
	templateName: 'gallery_frame',
	didInsertElement: function(){
		setFramesPart_Sizes();
	}
});