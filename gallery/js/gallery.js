var viewport_width = 0;
var viewport_height = 0;
var fullscreenmodechanged = false;
var artworks_images_scaled = false;
var artist_id_backup = null;

/*########################*/
/* When document is ready */
/*########################*/
$( document ).ready(function() {
	
});
/*Keydown system*/
$('body').keydown(function(ev){
	switch(ev.keyCode){
		case 38:
		case 85:
			if($('#home_menu').length>0){
				goUp();
			}
		break;
		case 40:
		case 68:
			if($('#home_menu').length>0){
				goDown();
			}
		break;
		case 13:
			if($('#home_menu').length>0){
				enter();
			}
		break;
		case 37:
		case 71:
			if($('#owl-artists').length>0){
				goPrevious_artists();
			}else if($('#owl-artworks').length>0){
				goPrevious_artworks();
			}
		break;
		case 39:
		case 68:
			if($('#owl-artists').length>0){
				goNext_artists();
			}else if($('#owl-artworks').length>0){
				goNext_artworks();
			}
		break;
		case 27:
			if($('#owl-artists').length>0){
				backHome_fromArtists();
			}else if($('#owl-artworks').length>0){
				backHome_fromArtworks();
			}
		break;
	}
});

$(window).bind("fullscreen-on", function(e, state) {
	fullscreenmodechanged = true;
	console.log("we're on fullscreen now");
});
/*#######################*/
/* When document resizes */
/*#######################*/
$( window ).resize(function() {
	artworks_images_scaled = false;
	if($('#home_menu').length>0){
		setHomePart_Sizes();
	}
	if($('#owl-artists').length>0){
		setArtistsPart_Sizes();
		update_ArtistsImgs();
		setArtistsCarousel_visuals();
	}
	if($('#owl-artworks').length>0){
		setArtworksPart_Sizes();
		if(fullscreenmodechanged){
			update_ArtworksImgs();
			fullscreenmodechanged = false;
		}else{
			addunscaledMessage();
		}
		setArtworksCarousel_visuals();
	}
});
/*############################*/
/* Set the showroom's settings*/
/* Input: - */
/* Ouput: - */
/*############################*/
function init_gallery_settings(){
	window.auto_slideshow_activated = 0;
	window.auto_slideshow_time = 0;
	var session_string = localStorage.getItem('session');
	var session_json = $.parseJSON(session_string);
	var self = Gallery.__container__.lookup('controller:gallery_home');
	var promiseObject_setting = self.get('setting');
	var promiseObject_control = self.get('control');
	/*--- Get user settings ---*/
	promiseObject_setting.then(function(object) {
		window.screen_projected_area_height_cm = object.get('projectionCm_vertical');
		window.screen_projected_area_width_cm = object.get('projectionCm_horizon');
		window.screen_projected_area_height_px = object.get('resolutionPx_vertical');
		window.screen_projected_area_width_px = object.get('resolutionPx_horizon');
		window.auto_slideshow_activated = object.get('auto_slideshow_activate');
		window.auto_slideshow_time = object.get('auto_slideshow_time');
	});
	/*--- Get application control information ---*/
	promiseObject_control.then(function(object) {
		window.frame_section_enable = object.get('frame_section_enable');
		window.artworks_tag_enable = object.get('artworks_tag_enable');
	});

}
/*########################*/
/* Set the viewport sizes */
/* Input: - */
/* Ouput: - */
/*########################*/
function set_viewportSizes(){
	viewport_width = $( window ).width();
	viewport_height = $( window ).height();
}
/*###############################################################*/
/* Compare the viewport sizes */
/* Input: Viewport width & height */
/* Ouput: True if sizes are equal to the originals, False if not */
/*###############################################################*/
function compare_viewportSizes(viewport_w,viewport_h){
	var value = false;
	if((viewport_w === $( window ).width())&&(viewport_h === $( window ).height())){
		value = true;
	}else{
		value = false;
	}
	return value;
}
/*#########################################*/
/* Resize all Artists Part visual elements */
/* Input: - */
/* Ouput: - */
/*#########################################*/
function setHomePart_Sizes(){
	$('.gallery_home_overview_panel').fadeOut(0);
	var viewportWidth = $( window ).width();
	var viewportHeight = $( window ).height();
	var unit = viewportWidth/6;

	if((window.frame_section_enable == 0)||(window.frame_section_enable == '0')){
		$('.gallery_menu_item#frameit').css('background-image','none');
		$('.gallery_menu_item#frameit').css('cursor','default');
	}

	$('.gallery#home_control').height(viewportHeight);
	$('.gallery#home_menu').height(viewportHeight);
	$('.gallery#home_countdown').height(viewportHeight);
	$('.gallery#home_selection').height(viewportHeight);
	$('.sep-home').height(viewportHeight);
	$('.gallery#menu').height(viewportHeight);
	var arrowWidth = $('.arrow').height();
	var toDoHeight = $('.gallery#home_control').height() - (arrowWidth*2) - 1;
	$('#goTop').height(viewportHeight/4);
	$('#goDown').height(viewportHeight/4);
	$('#toDo').height(viewportHeight/2);

	$('.gallery#home_control').width(unit);
	$('.gallery#home_menu').width(unit*2);
	$('.gallery#home_countdown').width(unit/2);
	$('.gallery#home_selection').width(unit*2);
	var homeMenu_width = $('.gallery#home_menu').width();
	$('.sep-home').width((homeMenu_width/5));
	$('#menu').width(homeMenu_width/5*3);

	var itemMenu_qty = $('.gallery#menu').children().length;
	$('.gallery#menu').children().height(viewportHeight/itemMenu_qty);

	$topMenuElement = $('#artists');
	$activeMenuElement = $('#artist');
	$bottomMenuElement = $('#frameit');
	setMenuElements($topMenuElement,$activeMenuElement,$bottomMenuElement);

	unRegisterAllEventListeners($('#goTop'));
	$('#goTop').click(function(){
		goUp();
	});
	unRegisterAllEventListeners($('#goDown'));
	$('#goDown').click(function(){
		goDown();
	});
}
/*#######################################################*/
/* Resize the home selection image of the artwork/artist */
/* Input: - */
/* Ouput: - */
/* Triggered from the html file, onload */
/*#######################################################*/
function resize_homeSelection_img(img_width,img_height){
	var worker_resize_imgSelectionOverview = new Worker("js/worker_imageTreatment.js");
	var container_width_px = $('#home_selection').width();
	var container_height_px = $('#home_selection').height();
	var img_width = img_width;
	var img_height = img_height;
	worker_resize_imgSelectionOverview.postMessage({'message':'home_image_selection','container_width_px':container_width_px,'container_height_px':container_height_px,'img_width':img_width,'img_height':img_height}); 
	worker_resize_imgSelectionOverview.onmessage = function (oEvent) {
		switch(oEvent.data.message){
			case 'home_image_selection_response':
				$('.gallery_home_overview').width(oEvent.data.new_img_width);
				$('.gallery_home_overview').height(oEvent.data.new_img_height);
				$('.gallery_home_overview_panel:before').height(oEvent.data.new_img_height);
				$('.gallery_home_overview_panel').css('top',oEvent.data.marginTop_img);
				$('.gallery_home_overview_panel').width(oEvent.data.new_img_width);
				worker_resize_imgSelectionOverview.terminate();
				$('.gallery_home_overview_panel').fadeIn(500);
			break;
		}
	};
}
/*#########################################*/
/* Resize all Artists Part visual elements */
/* Input: - */
/* Ouput: - */
/*#########################################*/
function setArtistsPart_Sizes(){
	$('.artist_item img').fadeOut(0);
	var viewportHeight = $( window ).height();
	$('.gallery#artists_control').height(viewportHeight);
	$('.gallery#artists_control_trigger').height(viewportHeight);
	$('.gallery#artists_view').height(viewportHeight);
	var gallery_artists_info_height = viewportHeight/10;
	$('.gallery#artists_info').height(gallery_artists_info_height);
	$('.gallery#artists_info_trigger').height(gallery_artists_info_height);
	
	$('.artists_arrow').height(gallery_artists_info_height);
	var gallery_artists_view_height = $('.gallery#artists_view').height();
	var description_height = gallery_artists_view_height/4;
	$('.artists_description').height(description_height);
	var description_title_height = $('.artists_description').height();
	$('.artists_description_title_element').height(description_title_height/2);
	
	var viewportWidth = $( window ).width();
	var unit = viewportWidth/20;
	$('.gallery#artists_control').width(unit);
	$('.gallery#artists_control_trigger').width(unit);
	
	var gallery_artists_info_width = $('.gallery#artists_info').width();
	$('.artists_arrow').width(gallery_artists_info_width/4);
	
	$('.artists_description').css('top', gallery_artists_view_height/4*1.5);

	var gallery_artists_info_width = $('.gallery#artists_info').width();
	var gallery_artists_info_height = $('.gallery#artists_info').height();
	$('.gallery_artists#carousel_bullets').width(gallery_artists_info_width/2);
	$('.gallery_artists#carousel_bullets').height(gallery_artists_info_height);
}
/*#########################################*/
/* Sizes all Artworks Part visual elements */
/* Input: - */
/* Ouput: - */
/*#########################################*/
function setArtworksPart_Sizes(){
	$('.artwork_item img').fadeOut(0);
	var viewportHeight = $( window ).height();
	$('.gallery#artworks_control').height(viewportHeight);
	$('.gallery#artworks_control_trigger').height(viewportHeight);
	$('.gallery#artworks_view').height(viewportHeight);
	var gallery_artworks_info_height = viewportHeight/10;
	$('.gallery#artworks_info').height(gallery_artworks_info_height);
	$('.gallery#artworks_info_trigger').height(gallery_artworks_info_height);
	$('.artworks_arrow').height(gallery_artworks_info_height);
	$('.artwork_info').height(gallery_artworks_info_height);
	$('.artwork_info_other').height(gallery_artworks_info_height);
	$('.artwork_scale').height(gallery_artworks_info_height/2);
	var description_scale = 90 / 100;
	if((window.artworks_tag_enable == 1)||(window.artworks_tag_enable == '1')){
		$('.artworks_description').height(gallery_artworks_info_height * description_scale);
		$('.artworks_description_title').height(gallery_artworks_info_height * description_scale);
		var description_title_height = $('.artworks_description_title').height();
		$('.artworks_description_title_element').height(description_title_height/2);
		$('.artworks_description_info').height(gallery_artworks_info_height * description_scale);
		$('.artworks_description_info_element .artist_description').parent().height(description_title_height * 0.5);
		$('.artworks_description_info_element .artist_dimensions').parent().height(description_title_height * 0.5);
	}else{
		$('.artworks_description').height('0px');
		$('.artworks_description').css('display','none');
	}
	var viewportWidth = $( window ).width();
	var unit = viewportWidth/20;
	var description_scale = 90/100;
	var description_margin = (10/100) / 2;
	var gallery_artworks_info_width = $('.gallery#artworks_info').width();
	var info_text_width = gallery_artworks_info_width/8*3;
	$('.gallery#artworks_control').width(unit);
	$('.gallery#artworks_control_trigger').width(unit);
	$('.gallery#artworks_view').width(viewportWidth);
	$('.artwork_scale').width(gallery_artworks_info_width/8);

	if((window.artworks_tag_enable == 1)||(window.artworks_tag_enable == '1')){
		$('.artworks_description').width(info_text_width * description_scale);
		var artworks_description_width = $('.artworks_description').width();
		var artworks_control_width = $('#artworks_control').width();
		var artworks_description_marginLeft = ((viewportWidth - artworks_description_width)/2)  - (artworks_control_width * 2);
		$('.artworks_description').css('margin-left',artworks_description_marginLeft);
	}
	var artworks_scale_position_right = artworks_description_marginLeft + artworks_description_width;
	$('.artwork_scale').css('left',artworks_scale_position_right);
	var artworks_scale_position_bottom = 
	$('.artwork_scale').css('left',artworks_scale_position_right);
}
/*########################################*/
/* Resize all Frames Part visual elements */
/* Input: - */
/* Ouput: - */
/*########################################*/
function setFramesPart_Sizes(){
	var viewportHeight = $( window ).height();
	$('.gallery#frames_control').height(viewportHeight);
	$('.gallery#frames_control_trigger').height(viewportHeight);
	$('.gallery#frames_view').height(viewportHeight);
	var gallery_frames_info_height = viewportHeight/10;
	$('.gallery#frame_info').height(gallery_frames_info_height);
	$('.frame_info').height(gallery_frames_info_height);
	var viewportWidth = $( window ).width();
	var unit = viewportWidth/20;

	$('.gallery#frames_control').width(unit);
	$('.gallery#frames_control_trigger').width(unit);
	$('.gallery#frames_view').width(viewportWidth);

	unRegisterAllEventListeners($('.gallery#frames_control'));
	$('.gallery#frames_control').click(function(){
		backHome_fromFrames();
	});

	$('.gallery#frames_control').css('left',(-unit));
	$('.gallery#frames_control_trigger').mouseenter(function() {
		$('.gallery#frames_control').animate({left: '0px'},200);
	});
	$('.gallery#frames_control').mouseenter(function() {
		$('.gallery#frames_control').animate({left: '0px'},200);
	});
	$('.gallery#frames_control').mouseleave(function() {
		$('.gallery#frames_control').animate({left: (-$('.gallery#frames_control').width())},200);
	});
}
/*####################################*/
/* Initialize all Artists Part events */
/* Input: - */
/* Ouput: - */
/*####################################*/
function setArtistsPart_Events(){
	$('.gallery#artists_control_trigger').click(function(){
		backHome_fromArtists();
	});
    $('#artists_goLeft').click(function(){
    	goPrevious_artists();
	});
    $('#artists_goRight').click(function(){
    	goNext_artists();
	});	
}
/*#####################################*/
/* Initialize all Artworks Part events */
/* Input: - */
/* Ouput: - */
/*#####################################*/
function setArtworksPart_Events(){
	$('.gallery#artworks_control_trigger').click(function(){
		backHome_fromArtworks();
	});
    $('#artworks_goLeft').click(function(){
    	goPrevious_artworks();
	});
    $('#artworks_goRight').click(function(){
    	goNext_artworks();
	});
}
/*#######################################################*/
/* Solve EmberJs & OwlJs conflict on the Artists carousel*/
/* Input: - */
/* Ouput: - */
/*#######################################################*/
function cleanCarousel_artists(){
    $('#carouselToClean_artists').addClass( 'owl-carousel_artists' );
	$('.owl-carousel_artists').removeAttr("id");
	$('.owl-carousel_artists').attr('id', 'owl-artists');
	$( ".owl-carousel_artists script" ).remove();
	$( "#info script" ).remove();
}
/*########################################################*/
/* Solve EmberJs & OwlJs conflict on the Artworks carousel*/
/* Input: - */
/* Ouput: - */
/*########################################################*/
function cleanCarousel_artworks(){
    $('#carouselToClean_artworks').addClass( 'owl-carousel' );
	$('.owl-carousel').removeAttr('id');
	$('.owl-carousel').attr('id', 'owl-artworks');
	$( ".owl-carousel script" ).remove();
	$( '#info script' ).remove();
}
/*#################################*/
/* Initialize the Artists carousel */
/* Input: - */
/* Ouput: - */
/*#################################*/
function init_carousel_artists(){
	$("#owl-artists").owlCarousel({
       	singleItem: true,
       	slideSpeed: 400,
       	rewindSpeed: 2000,
       	addClassActive : true,
       	itemsScaleUp:false,
       	lazyLoad : true,
		mouseDrag:false,
		navigation:false,
		pagination: false,
		responsiveBaseWidth: ".artists_view"
	});
	var owl = $("#owl-artists").data('owlCarousel');
}
/*##################################*/
/* Initialize the Artworks carousel */
/* Input: - */
/* Ouput: - */
/*##################################*/
function init_carousel_artworks(){
	/*----------------- Configure carousel  -----------------*/
	$("#owl-artworks").owlCarousel({
       	singleItem: true,
       	slideSpeed: 400,
       	rewindSpeed: 2000,
       	addClassActive : true,
       	itemsScaleUp:true,
       	lazyLoad : true,
		mouseDrag:false,
		navigation:false,
		pagination: false,
		responsiveBaseWidth: ".artworks_view"
	});
	var owl = $("#owl-artworks").data('owlCarousel');
}
/*######################################*/
/* Configure the Artists carousel sizes */
/* Input: - */
/* Ouput: - */
/*######################################*/
function configureCarousel_artists(){
	var containerHeight = $('.gallery#artists_view').height();
	$('.artist_item').height(containerHeight);
	var self = Gallery.__container__.lookup('controller:gallery_home');
	self.send('init_artist_id');
	update_ArtistsImgs();
	if(self.get('gallery_artist_id')!=null){
		var artistId = self.get('gallery_artist_id');
		var nbElementsBefore = $('div[id="'+artistId+'"]').closest('.owl-item').prevAll().length;
		var nbElementsAfter = $('div[id="'+artistId+'"]').closest('.owl-item').nextAll().length;
		var nbElementsTotal = nbElementsBefore + nbElementsAfter + 1;
		var currentItem = nbElementsTotal - nbElementsAfter - 1;
		$("#owl-artists").trigger('owl.jumpTo',currentItem);
	}
}
/*#######################################*/
/* Configure the Artworks carousel sizes */
/* Input: - */
/* Ouput: - */
/*#######################################*/
function configureCarousel_artworks(){
	setArtworksCarousel_visuals();
	var self = Gallery.__container__.lookup('controller:gallery_home');
	if(self.get('gallery_artwork_id')!=null){
		var artworkId = self.get('gallery_artwork_id');
		var nbElementsBefore = $('div[id="'+artworkId+'"]').closest('.owl-item').prevAll().length;
		var nbElementsAfter = $('div[id="'+artworkId+'"]').closest('.owl-item').nextAll().length;
		var nbElementsTotal = nbElementsBefore + nbElementsAfter + 1;
		var currentItem = nbElementsTotal - nbElementsAfter - 1;
		$("#owl-artworks").trigger('owl.jumpTo',currentItem);
	}
}
/*#######################################*/
/* Configure the Artists carousel visual */
/* Input: - */
/* Ouput: - */
/*#######################################*/
function setArtistsCarousel_visuals(){
	/*$('.owl-wrapper').css('width','100%');
	$('.owl-wrapper').css('left','0px');
	$('.owl-item').css('width','100%');*/
}
/*#######################################*/
/* Configure the Artworks carousel visual */
/* Input: - */
/* Ouput: - */
/*#######################################*/
function setArtworksCarousel_visuals(){
	var viewportHeight = $( window ).height();
	var gallery_artworks_info_height = viewportHeight/10;
	$('.gallery_artworks#carousel_bullets').height(gallery_artworks_info_height/2);
	var gallery_artworks_info_width = $('.gallery#artworks_info').width();
	$('.gallery_artworks#carousel_bullets').width(gallery_artworks_info_width/8);
	var containerHeight = $('.gallery#artworks_view').height();
	$('.artwork_item').height(containerHeight);
	var bullets_size = $('.gallery_artworks#carousel_bullets').width() * 4 / 100;
	$('.owl-page').height(bullets_size);
	$('.owl-page').width(bullets_size);
	var bullets_case_height = $('.gallery_artworks#carousel_bullets').height();
	var bullets_case_width = $('.gallery_artworks#carousel_bullets').width();
	var bullets_height = $('.owl-page').height();
	var bullets_nbElements = $('div.owl-page').length;
	var bullets_width = bullets_size*bullets_nbElements;
	var bullets_marginTop = (bullets_case_height - bullets_height)/2;
	var bullets_marginLeft = (bullets_case_width - bullets_width)/2;
	$('.owl-page').css('margin-top',bullets_marginTop);
	$('.owl-controls').css('margin-left',bullets_marginLeft);
}
/*###########################################*/
/* Return current active carousel artists ID */
/* Input: - */
/* Ouput: Artist ID */
/*###########################################*/
function getActiveOwlCarouselId_artists(){
	var elementId = $('div[class*="active"]').find('.artist_item').attr('id');
	return elementId;		
}
/*###########################################*/
/* Return current active carousel artwork ID */
/* Input: - */
/* Ouput: Artwork ID */
/*###########################################*/
function getActiveOwlCarouselId_artworks(){
	var elementId = $('div[class*="active"]').find('.artwork_item').attr('id');
	return elementId;		
}
/*#########################################################*/
/* Move the Artists carousel to the last artist of the list*/
/* Input: Artist ID */
/* Ouput: - */
/*#########################################################*/
function initializeCurrentArtistId(artistId){
	/*var owl = $("#owl-artists").data('owlCarousel');
	var nbElementsBefore = $('div[id="'+artistId+'"]').closest('.owl-item').prevAll().length;
	var nbElementsAfter = $('div[id="'+artistId+'"]').closest('.owl-item').nextAll().length;
	var nbElementsTotal = nbElementsBefore + nbElementsAfter + 1;
	var currentItem = nbElementsTotal - nbElementsAfter;*/
	$('#owl-artists').data('owl.carousel').prev();
}
/*##############################################################*/
/* Move the Artworks carousel to the currently selected artwork */
/* Input: Artwork ID */
/* Ouput: - */
/*##############################################################*/
function initializeCurrentArtworkId(artworkId){

}
/*#########################################################*/
/* Get the preloaded artists images and adapt their scales */
/* Input: - */
/* Ouput: - */
/*#########################################################*/
function update_ArtistsImgs(){
	if(window.imageArray_artists!=null){
		var container_height_px = $('#artists_view').height();
		var container_width_px = $('#artists_view').width();
		$('.artist_item').each(function(index, value) {
			var artist_item = $(this);
			var id = artist_item.attr('id');
			var displayed_artists = $.grep(window.imageArray_artists, function(a) {
			  return a.get(0) == id;
			});
			if(displayed_artists[0]!=null){
				var img = displayed_artists[0].get(1);
			    var dim_horizon_px = img.width;
			    var dim_vertica_px = img.height;
			    var artist_array_wh = new Array(dim_horizon_px,dim_vertica_px);
			    var worker_resize_artistsImgs = new Worker("js/worker_imageTreatment.js");
			    worker_resize_artistsImgs.postMessage({'message':'artist_image_wh','container_width_px':container_width_px,'container_height_px':container_height_px,'artist_array_wh':artist_array_wh});
				worker_resize_artistsImgs.onmessage = function (oEvent) {
					if(oEvent.data.message == 'artist_image_whm'){
						var artist_array_whm = oEvent.data.artist_array_whm;
						var w = artist_array_whm[0];
						var h = artist_array_whm[1];
						var margin = artist_array_whm[2];
						img.width = w;
						img.height = h;
						artist_item.children('img').replaceWith(img);
						artist_item.children('img').css('margin-top',margin);
						artist_item.children('img').css('-webkit-border-radius',w);
						artist_item.children('img').fadeIn(500);
						worker_resize_artistsImgs.terminate();
					}
				};
			}else{
			    var dim_horizon_px = 500;
			    var dim_vertica_px = 500;
			    var artist_array_wh = new Array(dim_horizon_px,dim_vertica_px);
			    var worker_resize_artistsImgs = new Worker("js/worker_imageTreatment.js");
			    worker_resize_artistsImgs.postMessage({'message':'artist_image_wh','container_width_px':container_width_px,'container_height_px':container_height_px,'artist_array_wh':artist_array_wh});
				worker_resize_artistsImgs.onmessage = function (oEvent) {
					if(oEvent.data.message == 'artist_image_whm'){
						var artist_array_whm = oEvent.data.artist_array_whm;
						var w = artist_array_whm[0];
						var h = artist_array_whm[1];
						var margin = artist_array_whm[2];
						artist_item.children('img').css('width',w);
						artist_item.children('img').css('height',h);
						artist_item.children('img').css('margin-top',margin);
						artist_item.children('img').css('-webkit-border-radius',w);
						artist_item.children('img').fadeIn(500);
						worker_resize_artistsImgs.terminate();
					}
				};
			}
		});
	}
}

/*##########################################################*/
/* Get the preloaded artworks images and adapt their scales */
/* Input: - */
/* Ouput: - */
/*##########################################################*/
function update_ArtworksImgs(){
	var container_height = $('#artworks_view').height() - $('#artworks_info').height();
	var container_width = $('#artworks_view').width();
	$('.artwork_item').each(function(index, value) {
		var artwork_item = $(this);
		var id = artwork_item.attr('id');
		var displayed_artworks = $.grep(window.array_artworks_imgs, function(a) {
		  return a.get(0) == id;
		});
		if(displayed_artworks[0]!=null){
		    var dim_horizon_cm = $(this).children('input[name="artwork_dim_horizon"]').val();
		    var dim_vertica_cm = $(this).children('input[name="artwork_dim_vertica"]').val();
		    var artwork_array_wh = new Array(dim_horizon_cm,dim_vertica_cm);
		    var worker_resize_artworksImgs = new Worker("js/worker_imageTreatment.js");
			worker_resize_artworksImgs.postMessage({'message':'artwork_image_wh','container_width_px':container_width,'container_height_px':container_height,'artwork_array_wh':artwork_array_wh,'projection_area_height_px':window.screen_projected_area_height_px,'projection_area_height_cm':window.screen_projected_area_height_cm,'projection_area_width_px':window.screen_projected_area_width_px,'projection_area_width_cm':window.screen_projected_area_width_cm});
			worker_resize_artworksImgs.onmessage = function (oEvent) {
				if(oEvent.data.message == 'artwork_image_whsm'){
					var artwork_array_whsm = oEvent.data.artwork_array_whsm;
					var w;
					var h;
					var s;
					var margin;
					var img = displayed_artworks[0].get(1);
					w = artwork_array_whsm[0];
					h = artwork_array_whsm[1];
					s = artwork_array_whsm[2];
					margin = artwork_array_whsm[3];
					artwork_item.children('img').replaceWith(img);
					artwork_item.children('img').css('width',w);
					artwork_item.children('img').css('height',h);
					artwork_item.children('img').css('margin-top',margin);
					artwork_item.children('img').css('margin-bottom',margin);
					artwork_item.children('img').fadeIn(500);
					artwork_item.find('.artwork_scale#'+id).append('<p>Scale 1:'+String(s)+'</p>');
					worker_resize_artworksImgs.terminate();
				}
			};
		}else{
			var noArtwork_item = $(this);
			var img = noArtwork_item.children('img');
			var noArtwork_array_wh = new Array(img[0].naturalWidth,img[0].naturalHeight);
			var worker_resize_artworksImgs = new Worker("js/worker_imageTreatment.js");
			worker_resize_artworksImgs.postMessage({'message':'noArtwork_image_wh','container_width_px':container_width,'container_height_px':container_height,'noArtwork_array_wh':noArtwork_array_wh,'projection_area_height_px':window.screen_projected_area_height_px,'projection_area_height_cm':window.screen_projected_area_height_cm,'projection_area_width_px':window.screen_projected_area_width_px,'projection_area_width_cm':window.screen_projected_area_width_cm});
			worker_resize_artworksImgs.onmessage = function (oEvent) {
				if(oEvent.data.message == 'noArtwork_image_whm'){
					var artwork_array_whsm = oEvent.data.noArtwork_array_whm;
					var w;
					var h;
					var margin;
					w = artwork_array_whsm[0];
					h = artwork_array_whsm[1];
					margin = artwork_array_whsm[2];
					img.css('width',w);
					img.css('height',h);
					img.css('margin-top',margin);
					img.css('margin-bottom',margin);			
					noArtwork_item.find('.artwork_scale#'+id).append('<p>Scale - </p>');
					worker_resize_artworksImgs.terminate();
				}
			};
		}
	});
	artworks_images_scaled = true;
}
/*#####################################################################*/
/* Display the Artworks carousel arrows if there is more than one item */
/* Input: - */
/* Ouput: - */
/*#####################################################################*/
function set_ArtworksArrows(){
    if(($(".owl-item").length)<=1){
      $(".artworks_arrow#artworks_goLeft").fadeTo( "fast", 0);
      $(".artworks_arrow#artworks_goRight").fadeTo( "fast", 0);
      $(".artworks_arrow#artworks_goLeft").css("cursor","default");
      $(".artworks_arrow#artworks_goRight").css("cursor","default");
    }else{
      $(".artworks_arrow#artworks_goLeft").fadeTo( "fast", 1);
      $(".artworks_arrow#artworks_goRight").fadeTo( "fast", 1);
      $(".artworks_arrow#artworks_goLeft").css("cursor","pointer");
      $(".artworks_arrow#artworks_goRight").css("cursor","pointer");
    }
}
/*##################################*/
/* Go back to the home gallery menu */
/* Input: - */
/* Ouput: - */
/*##################################*/
function backHome_fromArtists(){
	var self = Gallery.__container__.lookup('controller:gallery_home');
	var currentArtistId = getActiveOwlCarouselId_artists();
	if(currentArtistId != null || currentArtistId != 'undefined'){
		//If artist has changed
		if((artist_id_backup == null)||(artist_id_backup != currentArtistId)){
			var self = Gallery.__container__.lookup('controller:gallery_home');
			self.send('update_currentId','artist',currentArtistId);
			artist_id_backup = currentArtistId;
			self.send('reset_artwork_id');
		}else{

		}
	}
    self.store.find('session').then(function(sessions) {
       	var gallery_name = sessions.objectAt(0).get('gallery_name');
        var username = sessions.objectAt(0).get('username');
        self.transitionToRoute('gallery_home',gallery_name,username);
    });
}
/*##################################*/
/* Go back to the home gallery menu */
/* Input: - */
/* Ouput: - */
/*##################################*/
function backHome_fromArtworks(){
	var self = Gallery.__container__.lookup('controller:gallery_home');
	var currentArtworkId = getActiveOwlCarouselId_artworks();
	if(currentArtworkId != null || currentArtworkId != 'undefined' || currentArtworkId != undefined ){
		var self = Gallery.__container__.lookup('controller:gallery_home');
		self.send('update_currentId','artwork',currentArtworkId);
	}
    self.store.find('session').then(function(sessions) {
       	var gallery_name = sessions.objectAt(0).get('gallery_name');
        var username = sessions.objectAt(0).get('username');
        self.transitionToRoute('gallery_home',gallery_name,username);
    });
}
/*##################################*/
/* Go back to the home gallery menu */
/* Input: - */
/* Ouput: - */
/*##################################*/
function backHome_fromFrames(){
	var self = Gallery.__container__.lookup('controller:gallery_home');
    self.store.find('session').then(function(sessions) {
       	var gallery_name = sessions.objectAt(0).get('gallery_name');
        var username = sessions.objectAt(0).get('username');
        self.transitionToRoute('gallery_home',gallery_name,username);
    });
}
/*#############################################*/
/* Go to the next item in the Artists carousel */
/* Input: - */
/* Ouput: - */
/*#############################################*/
function goNext_artists(){
	$("#owl-artists").trigger('owl.next');
}
/*#################################################*/
/* Go to the previous item in the Artists carousel */
/* Input: - */
/* Ouput: - */
/*#################################################*/
function goPrevious_artists(){
	$("#owl-artists").trigger('owl.prev');
}
/*##############################################*/
/* Go to the next item in the Artworks carousel */
/* Input: - */
/* Ouput: - */
/*##############################################*/
function goNext_artworks(){
	$("#owl-artworks").trigger('owl.next');
}
/*##################################################*/
/* Go to the previous item in the Artworks carousel */
/* Input: - */
/* Ouput: - */
/*##################################################*/
function goPrevious_artworks(){
	$("#owl-artworks").trigger('owl.prev');
}

/*#########################*/
/* Move the home menu Down */
/* Input: - */
/* Ouput: - */
/*#########################*/
function goDown(){
	if((window.frame_section_enable == 0)||(window.frame_section_enable == '0')){
		$topMenuElement = $('.menuItem.active');
		$activeMenuElement = $('.menuItem.top');
		$bottomMenuElement = $('.menuItem.bott');
	}else{
		$topMenuElement = $('.menuItem.active');
		$activeMenuElement = $('.menuItem.bott');
		$bottomMenuElement = $('.menuItem.top');		
	}
	setMenuElements($topMenuElement,$activeMenuElement,$bottomMenuElement);
}
/*#######################*/
/* Move the home menu Up */
/* Input: - */
/* Ouput: - */
/*#######################*/
function goUp(){
	if((window.frame_section_enable == 0)||(window.frame_section_enable == '0')){
		$topMenuElement = $('.menuItem.active');
		$activeMenuElement = $('.menuItem.top');
		$bottomMenuElement = $('#frameit');
	}else{
		$topMenuElement = $('.menuItem.bott');
		$activeMenuElement = $('.menuItem.top');
		$bottomMenuElement = $('.menuItem.active');
	}

	setMenuElements($topMenuElement,$activeMenuElement,$bottomMenuElement);
	//startCmpt();
}	
/*#################################################*/
/* Go from the home page to the menu selected path */
/* Input: - */
/* Ouput: - */
/*#################################################*/
function enter(){
	var category = getMenu_ActiveItem();
	var self = Gallery.__container__.lookup('controller:gallery_home');
	var gallery_artist_id = self.get('gallery_artist_id');
	var gallery_artwork_id = self.get('gallery_artwork_id');
	switch(category){
		case 'artists':
		    self.store.find('session').then(function(sessions) {
		       	var gallery_name = sessions.objectAt(0).get('gallery_name');
		        var username = sessions.objectAt(0).get('username');
		        self.transitionToRoute('gallery_artists',gallery_name,username);
		    });
		break;
		case 'artist':
		    self.store.find('session').then(function(sessions) {
		       	var gallery_name = sessions.objectAt(0).get('gallery_name');
		        var username = sessions.objectAt(0).get('username');
		        self.transitionToRoute('gallery_artist',gallery_name,username,gallery_artist_id);
		    });
		break;
		case 'frame':
		    self.store.find('session').then(function(sessions) {
		       	var gallery_name = sessions.objectAt(0).get('gallery_name');
		        var username = sessions.objectAt(0).get('username');
		        self.transitionToRoute('gallery_frame',gallery_name,username, gallery_artwork_id);
		    });
		break;
	}
}
/*##############################################*/
/* Set the visual menu elements the right place */
/* Input: top ,active & bottom element */
/* Ouput: - */
/*##############################################*/
function setMenuElements(topElement,activeElement,bottomElement){
	$source = $('.gallery#menu');
	/*--- Set to top top element ---*/
	$topElement = topElement;
	$topElement.removeClass(); 
	$topElement.addClass('menuItem top');
	$topElement.css('background-color','rgba(255,255,255,0)');
	$topElement.css('-webkit-filter','invert(100%) blur(1px)');
	$topElement.css('filter','invert(100%) blur(1px)');
	$topElement.appendTo($source);

	/*--- Set to center active element ---*/
	$centerElement = activeElement;
	$centerElement.removeClass(); 
	$centerElement.addClass('menuItem active');
	$centerElement.css('-webkit-filter','invert(0%) blur(0px)');
	$centerElement.css('filter','invert(100%) blur(1px)');
	$centerElement.css('background-color','rgba(255,255,255,1)');

	if(bottomElement != null){
		/*--- Set to bottom bott element ---*/
		$botElement = bottomElement;
		$botElement.removeClass(); 
		$botElement.addClass('menuItem bott');
		$botElement.css('-webkit-filter','invert(100%) blur(1px)');
		$botElement.css('filter','invert(100%) blur(1px)');
		$botElement.css('background-color','rgba(255,255,255,0)');

		$botElement.prependTo($source);
		unRegisterAllEventListeners($('#frameit'));
		$('#frameit').click(function(){
			var gallery_artwork_id = self.get('gallery_artwork_id');
		    self.store.find('session').then(function(sessions) {
		       	var gallery_name = sessions.objectAt(0).get('gallery_name');
		        var username = sessions.objectAt(0).get('username');
		        self.transitionToRoute('gallery_frame',gallery_name,username, gallery_artwork_id);
		    });
		});
	}else{
		$botElement = $('#frameit');
		$('#frameit').removeClass();
		$('#frameit').addClass('menuItem bott');
		$('#frameit').appendTo($source);
		unRegisterAllEventListeners($('#frameit'));
	}

	var self = Gallery.__container__.lookup('controller:gallery_home');
	unRegisterAllEventListeners($('#artists'));
	$('#artists').click(function(){
	    self.store.find('session').then(function(sessions) {
	       	var gallery_name = sessions.objectAt(0).get('gallery_name');
	        var username = sessions.objectAt(0).get('username');
	        self.transitionToRoute('gallery_artists',gallery_name,username);
	    });
	});
	unRegisterAllEventListeners($('#artist'));
	$('#artist').click(function(){
		var gallery_artist_id = self.get('gallery_artist_id');
	    self.store.find('session').then(function(sessions) {
	       	var gallery_name = sessions.objectAt(0).get('gallery_name');
	        var username = sessions.objectAt(0).get('username');
	        self.transitionToRoute('gallery_artist',gallery_name,username, gallery_artist_id);
	    });
	});
}
/*####################################################*/
/* Get the currently active (centered) home menu item */
/* Input: Artwork ID */
/* Ouput: - */
/*####################################################*/
function getMenu_ActiveItem(){
	var elementId = $(document).find('.menuItem.active')[0].id;
	return elementId;
}
/*##########################################*/
/* Manage the error (display & record) */
/* Input: Error reference, message & source */
/* Ouput: - */
/*##########################################*/
function showError(error_ref,error_message,error_source){
	if(error_message.toLowerCase().indexOf("no access") >= 0){
		localStorage.setItem('userLogged','false');
	}
	console.log(error_ref);
	console.log(error_message);
	console.log(error_source);
}
/*########################################*/
/* Manage the information popup (display) */
/* Input: Information text & reference */
/* Ouput: - */
/*########################################*/
function showInfo(info_ref,info_text){
	popupQty_total = $('.gallery_info_popup').length;
	switch(info_ref){
		case '8888': //artworks images are not at the good scale anymore, please reload the page
		console.log($('body').find('#8888'));
			if(!$('body').find('#8888')){
				console.log('No 8888 message yet');
				if(popupQty_total<=0){
					$('body').append('<div class="gallery_info_popup" id="' + info_ref + '" style="bottom: 0px"><p>' + info_text + '</p></div>');
				}else{
					var info_height = $('body .gallery_info_popup').first().outerHeight(true);
					var info_position_bottom = popupQty_total * info_height;
					$('body').append('<div class="gallery_info_popup" id="' + info_ref + '" style="bottom: ' + upload_position_bottom + 'px;"><p>' + info_text + '</p></div>');
				}			
			}
		break;
		default:
			if(popupQty_total<=0){
				$('body').append('<div class="gallery_info_popup" id="' + info_ref + '" style="bottom: 0px"><p>' + info_text + '</p></div>');
			}else{
				var info_height = $('body .gallery_info_popup').first().outerHeight(true);
				var info_position_bottom = popupQty_total * info_height;
				$('body').append('<div class="gallery_info_popup" id="' + info_ref + '" style="bottom: ' + upload_position_bottom + 'px;"><p>' + info_text + '</p></div>');
			}
	}
}
/*################################################*/
/* Close information popup found by its reference */
/* Input: Information reference */
/* Ouput: - */
/*################################################*/
function closeInfo_byRef(info_ref){
	$('.gallery_info_popup#'+info_ref).remove();
}
/*########################################################################*/
/* Replace scales with "Reload page to scaled" */
/* Input: - */
/* Ouput: - */
/*########################################################################*/
function addunscaledMessage(){
	console.log('addunscaledMessage');
	$('.artwork_item').each(function(index, value) {
		$(this).find('.artwork_scale').empty();
		$(this).find('.artwork_scale').append('<p>Reload page to scaled</p>');
	});
}
/*############################################*/
/* Remove all event listeners from an element */
/* Input: Concerned element */
/* Ouput: - */
/*############################################*/
function unRegisterAllEventListeners(obj) {
	if ( typeof obj._eventListeners == 'undefined' || obj._eventListeners.length == 0 ) {
		return;	
	}
	
	for(var i = 0, len = obj._eventListeners.length; i < len; i++) {
		var e = obj._eventListeners[i];
		obj.removeEventListener(e.event, e.callback);
	}
 
	obj._eventListeners = [];
}