/*#################*/
/* Get all artists */
/*#################*/
function getAllArtists(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_artists.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_artist_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*##################*/
/* Set an artist */
/*##################*/
function setArtist(access_token,artistId,name,location,description){
    var success = null; 
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((name == 'undefined')||(!name)){
        name = '-';
    }
    if((location == 'undefined')||(!location)){
        location = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/set_artist.php",
        type: "POST",
        data: {'access_token':access_token,'artistId':artistId,'name':name,'location':location,'description':description},
        async: false,
        success: function (artist) {
            var self = Gallery.__container__.lookup('controller:admin_artwork');
            self.store.find('artist', artistId).then(function(artist) {
                artist.set(name, name);
                artist.set(location, location);
                artist.set(description, description);
                self.transitionToRoute('admin_artist', artist);
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setArtist()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setArtist()'); 
            }
        }
    });
    return success; 
}
/*##################*/
/* Add a new artist */
/*##################*/
function addArtist(access_token,name,location,description){
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((name == 'undefined')||(!name)){
        name = '-';
    }
    if((location == 'undefined')||(!location)){
        location = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/create_artist.php",
        type: 'post',
        data: {'access_token':access_token,'name':name,'location':location,'description':description},
        dataType: 'json',
        async: false,
        success: function (artist) {
            var artistId = artist.id;
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.push('artist', {
              id: artistId,
              name: name,
              location: location,
              description: description
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtist()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtist()'); 
            }
        }
    });

}
/*##################*/
/* Remove an artist */
/*##################*/
function removeArtist(access_token,id){
    $.ajax({
        url: window.serverpath+"/store/delete_artist.php",
        type: "POST",
        data: {'access_token':access_token,'artistId':id},
        async: false,
        success: function (state) {
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist', id).then(function (artist) {
              artist.destroyRecord();
              self.transitionToRoute('admin_artists');
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: removeArtist()');  
            }else{
                showError(error.status,error.statusText,'crud.js: removeArtist()'); 
            }
        }
    });
}
/*####################################*/
/* Add an image (avatar) for an artist */
/*####################################*/
function addArtistImg(artistId,artistImg,access_token){
    var date = new Date();
    var upload_ref = date.getDate().toString() + date.getMonth().toString() + date.getFullYear().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString() + date.getMilliseconds().toString();
    $.ajax({
        url: window.serverpath+"/store/create_artist_img.php?id="+artistId+"&access_token="+access_token,
        type: "POST",
        data: artistImg,
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            showUpload('Uploading artist image...',upload_ref);
            xhr.upload.addEventListener('progress', function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
                    var percent_uploaded_artistImg = percentComplete * 50;
                    $('.admin_upload_popup#'+upload_ref).width(percent_uploaded_artistImg+'%');
                    $('.admin_upload_popup#'+upload_ref+' p').text('Uploading artist image... '+percent_uploaded_artistImg.toFixed(0)+'%');
                }
            }, false);
           xhr.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                //Do something with download progress
                var percent_uploaded_artistImg = 50 + (percentComplete * 50);
                $('.admin_upload_popup#'+upload_ref).width(percent_uploaded_artistImg+'%');
                $('.admin_upload_popup#'+upload_ref+' p').text('Uploading artist image... '+percent_uploaded_artistImg.toFixed(0)+'%');
              }
           }, false);
            return xhr;
        },
        processData: false,
        contentType: false,
        success: function (data) {
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist',artistId).then(function(artist) {
                artist.set('imgOverviewPath', data);
                artist.save();
                $('.admin_artist_picture .admin_img_overview img#'+artistId).attr('src',data);
                $('.admin_upload_popup#'+upload_ref).remove();
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtistImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtistImg()'); 
            }
        }
    });
}
/*########################################*/
/* Delete an image (avatar) for an artist */
/*########################################*/
function deleteArtistImg(artist){
    var artistId = artist['id'];
    $.ajax({
        url: window.serverpath+"/store/delete_artist_img.php",
        type: "POST",
        data: {'artistId':artistId},
        async: false,
        success: function (res) {
            window.artistimg = null;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: deleteArtistImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: deleteArtistImg()'); 
            }
        }
    });
}
/*####################################*/
/* Add an image (avatar) for an artwork */
/*####################################*/
function addArtworkImg(artworkId,artworkImg,access_token){
    var date = new Date();
    var upload_ref = date.getDate().toString() + date.getMonth().toString() + date.getFullYear().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString() + date.getMilliseconds().toString();
    $.ajax({
        url: window.serverpath+"/store/create_artwork_img.php?id="+artworkId+"&access_token="+access_token,
        type: "POST",
        data: artworkImg,
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            showUpload('Uploading artwork image...',upload_ref);
            xhr.addEventListener('progress', function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
                    var percent_uploaded_artworkImg = percentComplete * 50;
                    $('.admin_upload_popup#'+upload_ref).width(percent_uploaded_artworkImg+'%');
                    $('.admin_upload_popup#'+upload_ref+' p').text('Uploading artwork image... '+percent_uploaded_artworkImg.toFixed(0)+'%');
                }
            }, false);
            return xhr;
        },
        processData: false,
        contentType: false,
        success: function (data) {
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artwork',artworkId).then(function(artwork) {
                artwork.set('imgOverviewPath', data);
                artwork.save();
                $('.admin_artwork_picture .admin_img_overview img#'+artworkId).attr('src',data);
                $('.admin_upload_popup#'+upload_ref).remove();
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtworkImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtworkImg()'); 
            }
        }
    });
}
/*########################################*/
/* Delete an image (avatar) for an artwork */
/*########################################*/
function deleteArtworkImg(artwork){
    var artworkId = artwork['id'];
    $.ajax({
        url: window.serverpath+"/store/delete_artwork_img.php",
        type: "POST",
        data: {'artworkId':artworkId},
        async: false,
        success: function (res) {
            window.artworkimg = null;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: deleteArtworkImg()');  
            }else{
                showError(error.status,error.statusText,'crud.js: deleteArtworkImg()');
            }
        }
    });
}
/*##################*/
/* Get all artworks */
/*##################*/
function getAllArtworks(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_artworks.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_artworks_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*##################*/
/* Set an artwork */
/*##################*/
function setArtwork(access_token,artworkId,title,year,dim_horizon,dim_vertica,description){
    var success = null; 
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((title == 'undefined')||(!title)){
        title = '-';
    }
    if((year == 'undefined')||(!year)){
        year = '-';
    }
    if((dim_horizon == 'undefined')||(!dim_horizon)){
        dim_horizon = '-';
    }
    if((dim_vertica == 'undefined')||(!dim_vertica)){
        dim_vertica = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/set_artwork.php",
        type: "POST",
        data: {'access_token':access_token,'artworkId':artworkId,'title':title,'year':year,'dim_horizon':dim_horizon,'dim_vertica':dim_vertica,'description':description},
        async: false,
        success: function (artwork) {
            var self = Gallery.__container__.lookup('controller:admin_artwork');
            self.store.find('artwork', artworkId).then(function(artwork) {
                artwork.set(title, title);
                artwork.set(year, year);
                artwork.set(dim_horizon, dim_horizon);
                artwork.set(dim_vertica, dim_vertica);
                artwork.set(description, description);
                self.transitionToRoute('admin_artwork', artwork);
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setArtwork()');
            }
        }
    });
    return success;
}
/*##################*/
/* Add a new artwork */
/*##################*/
function addArtwork(access_token,artistId,title,year,dim_horizon,dim_vertica,description){
    if((access_token == 'undefined')||(!access_token)){
        access_token = '-';
    }
    if((title == 'undefined')||(!title)){
        title = '-';
    }
    if((year == 'undefined')||(!year)){
        year = '-';
    }
    if((dim_horizon == 'undefined')||(!dim_horizon)){
        dim_horizon = '-';
    }
    if((dim_vertica == 'undefined')||(!dim_vertica)){
        dim_vertica = '-';
    }
    if((description == 'undefined')||(!description)){
        description = '-';
    }

    $.ajax({
        url: window.serverpath+"/store/create_artwork.php",
        type: 'post',
        data: {'access_token':access_token,'artistId':artistId,'title':title,'year':year,'dim_horizon':dim_horizon,'dim_vertica':dim_vertica,'description':description},
        dataType: 'json',
        async: false,
        success: function (artwork) {
            var artworkId = artwork.id;
            var self = Gallery.__container__.lookup('controller:admin_artist');
            self.store.find('artist', artistId).then(function(artist) {
                self.store.push('artwork', {
                    artist:artist,
                    id: artworkId,
                    title: title,
                    year: year,
                    dim_horizon: dim_horizon,
                    dim_vertica: dim_vertica,
                    description: description,
                    artist_id: artistId
                });
                self.store.find('artwork',artworkId).then(function(artwork) {
                    self.transitionToRoute('admin_artwork', artwork);
                });
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addArtwork()');
            }
        }
    });

}
/*##################*/
/* Remove an artwork */
/*##################*/
function removeArtwork(access_token,id){
    var success = null;
    $.ajax({
        url: window.serverpath+"/store/delete_artwork.php",
        type: "POST",
        data: {'access_token':access_token,'artworkId':id},
        async: false,
        success: function (state) {
            var self = Gallery.__container__.lookup('controller:admin_artwork');
            
            self.store.find('artwork', id).then(function (artwork) {
                var artist_id = artwork.artist.id;
                artwork.destroyRecord();
                self.store.find('artist', artist_id).then(function (artist) {
                    self.transitionToRoute('admin_artist',artist);
                });
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: removeArtwork()');  
            }else{
                showError(error.status,error.statusText,'crud.js: removeArtwork()');
            }
        }
    });
    return success; 
}
/*#################*/
/* Get all settings */
/*#################*/
function getAllSettings(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_options.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_settings_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*#################*/
/* Set an option */
/*#################*/
function setSetting(access_token,settingName,value){
    $.ajax({
        url: window.serverpath+"/store/set_setting.php",
        type: "POST",
        data: {'access_token':access_token,'name':settingName,'value':value},
        async: false,
        success: function (data) {
            var self = Gallery.__container__.lookup('controller:admin_settings');
            self.store.find('setting').then(function(settings) {
                var setting = settings.objectAt(0);
                switch(settingName){
                    case 'resolutionPx_horizon': setting.set(resolutionPx_horizon, value);
                    break;
                    case 'resolutionPx_vertical': setting.set(resolutionPx_vertical, value);
                    break;
                    case 'projectionCm_horizon': setting.set(projectionCm_horizon, value);
                    break;
                    case 'projectionCm_vertical': setting.set(projectionCm_vertical, value);
                    break;
                    case 'auto_slideshow_activate': setting.set(auto_slideshow_activate, value);
                    break;
                    case 'auto_slideshow_time': setting.set(auto_slideshow_time, value);
                    break;
                    case 'scale_real_activate': setting.set(scale_real_activate, value);
                    break;
                    case 'selection_countdown_time': setting.set(selection_countdown_time, value);
                    break;
                    default: showError('007','No corresponding field found!','crud.js: setSetting');
                }
            });
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setSetting()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setSetting()');
            }
        }
    }); 
}
/*#################*/
/* Get control */
/*#################*/
function getControl(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_control.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4 && hr.status == 200) {
            load_control_inApp(access_token,JSON.parse(hr.responseText));
        }
    }
    hr.send(vars);
}
/*############*/
/* Check user */
/*############*/
function initUser(usernameoremail,password){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/init_user.php",
        type: "POST",
        data: {'usernameoremail':usernameoremail, 'password':password},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: initUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: initUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Get a user */
/*############*/
function getUser(access_token){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/get_user.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getUser()');
            }
        }
    });
    return user;   
}
/*############*/
/* Set a user */
/*############*/
function setUser(access_token,name,username,gallery_name,gallery_location){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/set_user.php",
        type: "POST",
        data: {'access_token':access_token,'name':name,'username':username,'gallery_name':gallery_name,'gallery_location':gallery_location},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setUser()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setUser()');
            }
        }
    });
    return user;   
}
/*###############*/
/* Get a session */
/*###############*/
function getSession(access_token,serverpath){
    var hr = new XMLHttpRequest();
    var url = serverpath+"/store/get_session.php";
    var vars = "access_token="+access_token;
    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
        if(hr.readyState == 4) {
            if(hr.status == 200){
               load_session_inApp(access_token,JSON.parse(hr.responseText)); 
            }else{
                console.log(hr.status);
                console.log(hr.responseText);
                console.log('crud.js: getSession()');
            }
        }
    }
    hr.send(vars);  
}
/*#####################*/
/* Get all information */
/*#####################*/
function getAllInformation(access_token){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/get_information.php",
        type: "POST",
        data: {'access_token':access_token},
        dataType: "json",
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: getAllInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: getAllInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Add a new information */
/*#######################*/
function deleteInformation(access_token,info_id){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/delete_information.php",
        type: "POST",
        data: {'access_token':access_token,'info_id':info_id},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: deleteInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: deleteInformation()');
            }
        }
    });
    return infos;   
}
/*#######################*/
/* Remove an information */
/*#######################*/
function addInformation(access_token,message,users_ids){
    var infos = null;
    $.ajax({
        url: window.serverpath+"/store/create_information.php",
        type: "POST",
        data: {'access_token':access_token,'message':message,'message':message},
        async: false,
        success: function (data) {
            infos = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: addInformation()');  
            }else{
                showError(error.status,error.statusText,'crud.js: addInformation()');
            }
        }
    });
    return infos;   
}
/*#####################*/
/* Set a user password */
/*#####################*/
function setPass(access_token, pass){
    var user = null;
    $.ajax({
        url: window.serverpath+"/store/set_password.php",
        type: "POST",
        data: {'access_token':access_token,'pass':pass},
        async: false,
        success: function (data) {
            user = data;
        },
        error: function(error) {
            if(error.responseText){
                showError(error.status,error.responseText,'crud.js: setPass()');  
            }else{
                showError(error.status,error.statusText,'crud.js: setPass()');
            }
        }
    });
    return user;   
}
/*################*/
/* Remove a token */
/*################*/
function removeTokenInDB(access_token){
    $.ajax({
        url: window.serverpath+"/store/delete_token.php",
        type: "POST",
        data: {'access_token':access_token},
        async: false,
    });
}