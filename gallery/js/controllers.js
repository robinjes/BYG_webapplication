Gallery.GalleryHomeController = Ember.Controller.extend({
  controllerName: 'gallery_home',
  gallery_artist_id: null,
  gallery_artwork_id: null,
  session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('session'),
  setting: function(){
    return this.store.find('setting').then(function(settings) {
      return settings.objectAt(0);
      });
  }.property('setting'),
  control: function(){
    return this.store.find('control').then(function(controls) {
      return controls.objectAt(0);
    });
  }.property('control'),
  init: function(){
    if((this.get('gallery_artist_id') == null)&&(this.get('gallery_artwork_id') == null)){
      var self = this;
      this.store.find('setting').then(function(settings) {
        var len = settings.get("length");
        if(settings.isLoaded&&(len>0)){
          var setting = settings.objectAt(0);
          setting.get('currentArtist').then(function(artist){
            if(artist!=null){
              self.set('gallery_artist_id', artist.get('id'));
            }else{
              self.set('gallery_artist_id', '');
            }
            setting.get('currentArtwork').then(function(artwork){
              if(artwork!=null){
                self.set('gallery_artwork_id', artwork.get('id'));
              }else{
                self.set('gallery_artwork_id', '');
              }
              init_gallery_settings();
            });
          });
        }else{
          showError('I-200','No settings found. Need default artist and artwork ID','controllers.js:init');
        }
      });
    }
  },
  actions: {
    update_currentId: function(type,id){
      var self = this;
      switch(type){
        case 'artist':
          if(id != null && id != 'undefined'){
            self.store.find('artist',id).then(function(artist) {
              self.store.find('artwork',{'artists_id':id}).then(function(artworks) {
                self.store.find('setting').then(function(settings) {
                  settings.objectAt(0).set('currentArtist',artist);
                  settings.objectAt(0).set('currentArtwork',artworks.objectAt(0));
                  settings.objectAt(0).save();
                });
              });
            });
            this.set('gallery_artist_id',id);
          }
        break;
        case 'artwork':
          if(id != null && id != 'undefined' && id != undefined){
            self.store.find('artwork',id).then(function(artwork) {
              self.store.find('setting').then(function(settings) {
                settings.objectAt(0).set('currentArtwork',artwork);
                settings.objectAt(0).save();
              });
            });
            this.set('gallery_artwork_id',id);
          }
        break;
      }
    },
    init_artist_id: function(){
      if(this.get('gallery_artist_id') == null){
        var self = this;
        this.store.find('setting').then(function(settings) {
          var len = settings.get("length");
          if(settings.isLoaded&&(len>0)){
            var setting = settings.objectAt(0);
            setting.get('currentArtist').then(function(artist){
                if(artist!=null){
                  self.set('gallery_artist_id', artist.get('id'));
                }else{
                  self.set('gallery_artist_id', '');
                }
            });
          }else{
            showError('I-200','No settings found. Need default artist ID','controllers.js:init_artist_id');
          }
        });
      }
    },
    init_artwork_id: function(){
      if(this.get('gallery_artwork_id') == null){
        var self = this;
        this.store.find('setting').then(function(settings) {
          var len = settings.get("length");
          if(settings.isLoaded&&(len>0)){
            var setting = settings.objectAt(0);
            setting.get('currentArtwork').then(function(artwork){
                if(artist!=null){
                  self.set('gallery_artwork_id', artwork.get('id'));
                }else{
                  self.set('gallery_artwork_id', '');
                }
            });
          }else{
            showError('I-200','No settings found. Need default artwork ID','controllers.js:init_artwork_id');
          }
        });
      }
    },
    reset_artist_id: function(){
      var self = this;
      this.store.find('setting').then(function(settings) {
        var setting = settings.objectAt(0);
        setting.set('currentArtist',null);
      });
      this.set('gallery_artist_id',null);
    },
    reset_artwork_id: function(){
      var self = this;
      this.store.find('setting').then(function(settings) {
        var setting = settings.objectAt(0);
        setting.set('currentArtwork',null);
      });
      this.set('gallery_artwork_id',null);
    }
  }
});

Gallery.GalleryArtistsController = Ember.Controller.extend({
  controllerName: 'gallery_artists',
  total: function() {
    var artists = this.get('artist');
    return artists.get('length');
  }.property('total')
});

Gallery.MainController = Ember.Controller.extend({
  needs: 'login'
});

Gallery.AdminHomeController = Ember.Controller.extend({
  needs: 'login',
  userLogged: function(){
    var value = false;
    if(localStorage.getItem('userLogged') == 'true'){
      value = true;
    }else{
      value = false;
    }
    return value;
  }.property('userLogged'),
  session: function(){
    var token = localStorage.getItem('accessTok');
    var user_data = getUser(token);
    localStorage.setItem('session', JSON.stringify(user_data));
    return user_data;
  }.property('session'),
  admin: function(){
    var value = false;
    /*1000=admin*/
    if((parseInt(this.get('session').group)) == 1000){
      value = true;
    }else{
      value = false;
    }
    return value;
  }.property('admin'),
  information: function(){
    var token = localStorage.getItem('accessTok');
    var user_information = getAllInformation(token);
    localStorage.setItem('information', JSON.stringify(user_information));
    return user_information;
  }.property('information'),
  actions: {
    close_information: function(info_id){
      var token = localStorage.getItem('accessTok');
      closeInfoPopup(info_id);
      deleteInformation(token,info_id);
    }
  }
});

Gallery.LoginController = Ember.Controller.extend({
  controllerName: 'login',
  needs: ['application'],
  actions: {
    login: function(usernameoremail, password) {
      $('#loading').show();
      var returnValue = initUser(usernameoremail,password);
      if(returnValue == null){
        showError('007','Wrong username or password. Try again.','LoginController: login action');
        $('#loading').hide();
      }else{
        if(typeof(Storage) !== 'undefined') {
          localStorage.setItem('userLogged', true);
          localStorage.setItem('accessTok', returnValue.accessTok);
          var user_data = JSON.stringify(getUser(returnValue.accessTok));
          localStorage.setItem('session', user_data);
        } else {
          $('#loading').hide();
          alert('Sorry! No Web Storage support...');
        }
        var access_token = localStorage.getItem('accessTok');
        var artists = getAllArtists(access_token);
        var len = artists.get("length");
        if(artists.isLoaded&&(len>0)){
            Gallery.Artist.reopenClass({
              FIXTURES: artists
            });
            preloadImages_artists(artists);         
        }
        var artworks = getAllArtworks(access_token);
        var len = artworks.get("length");
        if(artworks.isLoaded&&(len>0)){
            Gallery.Artwork.reopenClass({
              FIXTURES: artworks
            });
            preloadImages_artworks(artworks);
        }
        var settings = getAllSettings(access_token);
        var len = settings.get("length");
        if(settings.isLoaded&&(len>0)){
            Gallery.Setting.reopenClass({
              FIXTURES: settings
            });
        }
        var session = getSession(access_token);
        var len = session.get("length");
        if(session.isLoaded&&(len>0)){
            Gallery.Session.reopenClass({
              FIXTURES: session
            });
          var self = this;
          this.store.find('session').then(function(session) {
            var username = sessions.objectAt(0).get('username');
            var gallery_name = sessions.objectAt(0).get('gallery_name');
            var route_gallery = self.get('target').generate('gallery_home',gallery_name,username);
            var url = window.serverpath + route_gallery;
            localStorage.setItem('home_path_logged', url);
            location.href = url;
            self.get('controllers.gallery_home').send('init');
            $('#loading').hide();
          });
        }
      }
    },
    logout: function(concernedController) {
      var access_token = localStorage.getItem('accessTok');
      removeTokenInDB(access_token);
      localStorage.setItem('userLogged', false);
      localStorage.removeItem('accessTok');
      localStorage.removeItem('session');
      localStorage.removeItem('home_path_logged');
      window.location.replace("http://beyondyourgallery.com/admin");
    },
    toGallery: function(){
      //window.open('popup.html');
      var self = this;
      this.store.find('session').then(function(session) {
        var username = session.objectAt(0).get('username');
        var galleryName = session.objectAt(0).get('gallery_name');
        var route = self.get('target').generate('gallery_home',galleryName,username);
        var url = window.serverpath + route;
        window.open(url);
      });
    }
  }
});

Gallery.AdminArtistsController = Ember.Controller.extend({
  controllerName: 'admin_artists',
  needs: ['application', 'admin_artist_modification'],
  accessTok: function(){
    return localStorage.getItem('accessTok');
  }.property('accessTok'),
  session: function(){
    var user_data = JSON.parse(localStorage.getItem('session'));
    return user_data;
  }.property('session'),
});

Gallery.AdminArtistModificationController = Ember.Controller.extend({
  controllerName: 'admin_artist_modification',
  needs: ['application', 'admin_artist_modification'],
  artist: null,
  artistName: null,
  artistLocation: null,
  artistDescription: null,
  artwork: null,
  actions: {
    open_admin_artist_modification: function(artist){
      this.set('artist',artist);
      this.transitionToRoute('admin_artist', artist);
      this.transitionToRoute('admin_artist_modification');
    },
    cancel_modification_artist: function(){
      //Open the cancel_modification_artist Confirmation Box
      showCloseConfirmationBox();
    },
    leave_artistModification: function(artist){
      hideConfirmationBox();
      if(artist != null){
        artist.rollback();
      }
      this.transitionToRoute('admin_artist',artist);
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    validate_modification_artist: function(){
      //Open the validate_modification_artist Confirmation Box
      this.set('artistName',this.get('artist.name'));
      this.set('artistLocation',this.get('artist.location'));
      this.set('artistDescription',this.get('artist.description'));
      showModificationConfirmationBox();
    },
    save_artistModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var id = this.get('artist').id;
      var name = this.get('artistName');
      var location = this.get('artistLocation');
      var description = this.get('artistDescription');
      setArtist(access_token,id,name,location,description);
      hideConfirmationBox();
    },
    open_admin_artist_add: function(){
      this.transitionToRoute('admin_artist_add');
    },
    validate_add_artist: function(name,location,description){
      this.set('artistName',name);
      this.set('artistLocation',location);
      this.set('artistDescription',description);
      showModificationConfirmationBox();
    },
    add_artist: function(){
      var access_token = localStorage.getItem('accessTok');
      var name = this.get('artistName');
      var location = this.get('artistLocation');
      var description = this.get('artistDescription');
      var self = this;
      addArtist(access_token,name,location,description);
      hideConfirmationBox();
      this.transitionToRoute('admin_artists');
    },
    removeArtistPage: function(artist){
      this.set('artist',artist);
      showRemoveConfirmationBox_artist();
    },
    remove_artist: function(){
      var access_token = localStorage.getItem('accessTok');
      var artistId = this.get('artist').id;
      removeArtist(access_token,artistId);
      hideConfirmationBox();
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
    }
  }
});

Gallery.AdminArtistController = Ember.ObjectController.extend({
  controllerName: 'admin_artist',
  needs: ['application', 'admin_artwork_modification'],
  actions: {
    open_admin_artwork_modification: function(artwork){
      this.set('controllers.admin_artwork_modification.artwork', artwork);
      this.transitionToRoute('admin_artwork',artwork);
      this.transitionToRoute('admin_artwork_modification');
    },
    remove_artwork_page: function(artwork){
      this.set('artwork',artwork);
      showRemoveConfirmationBox_artwork();
    },
    remove_artwork: function(){
      var access_token = localStorage.getItem('accessTok');
      var artworkId = this.get('artwork').id;
      removeArtwork(access_token,artworkId);
      hideConfirmationBox();
    }
  }
});

Gallery.AdminArtworkModificationController = Ember.Controller.extend({
  controllerName: 'admin_artwork_modification',
  needs: ['application','admin_artwork_modification'],
  artwork: null,
  artworkTitle: null,
  artworkArtistId: null,
  artworkYear: null,
  artworkDimHorizon: null,
  artworkDimVertica: null,
  artworkDescription: null,
  actions: {
    cancel_modification_artwork: function(){
      //Open the cancel_modification_artist Confirmation Box
      showCloseConfirmationBox();
    },
    leave_artworkModification: function(artwork){
      hideConfirmationBox();
      if(artwork != null){
        artwork.rollback();  
      }
      this.transitionToRoute('admin_artwork',artwork);
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    validate_modification_artwork: function(){
      //Open the validate_modification_artist Confirmation Box
      this.set('artworkTitle',this.get('artwork.title'));
      this.set('artworkArtistId',this.get('artwork.artist_id'));
      this.set('artworkYear',this.get('artwork.year'));
      this.set('artworkDimHorizon',this.get('artwork.dim_horizon'));
      this.set('artworkDimVertica',this.get('artwork.dim_vertica'));
      this.set('artworkDescription',this.get('artwork.description'));
      showModificationConfirmationBox_artwork();
    },
    save_artworkModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var artworkId = this.get('artwork').id;
      var title = this.get('artworkTitle');
      var year = this.get('artworkYear');
      var dim_horizon = this.get('artworkDimHorizon');
      var dim_vertica = this.get('artworkDimVertica');
      var description = this.get('artworkDescription');
      setArtwork(access_token,artworkId,title,year,dim_horizon,dim_vertica,description);
      hideConfirmationBox();
    },
    open_admin_artwork_add: function(artist){
      this.set('artworkArtistId',artist.id);
      this.transitionToRoute('admin_artist', artist);
      this.transitionToRoute('admin_artwork_add');
    },
    validate_add_artwork: function(title,year,dim_horizon,dim_vertica,description){
      this.set('artworkTitle',title);
      this.set('artworkYear',year);
      this.set('artworkDimHorizon',dim_horizon);
      this.set('artworkDimVertica',dim_vertica);
      this.set('artworkDescription',description);
      showModificationConfirmationBox_artwork();
    },
    add_artwork: function(){
      var access_token = localStorage.getItem('accessTok');
      var title = this.get('artworkTitle');
      var artistId = this.get('artworkArtistId');
      var year = this.get('artworkYear');
      var dim_horizon = this.get('artworkDimHorizon');
      var dim_vertica = this.get('artworkDimVertica');
      var description = this.get('artworkDescription');
      var self = this;
      addArtwork(access_token,artistId,title,year,dim_horizon,dim_vertica,description);
      hideConfirmationBox();
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
    }
  }
});

Gallery.AdminUserModificationController = Ember.Controller.extend({
  controllerName: 'admin_user_modification',
  needs: ['admin_settings','login'],
  session: function(){
    var token = localStorage.getItem('accessTok');
    var user_data = getUser(token);
    localStorage.setItem('session', JSON.stringify(user_data));
    return user_data;
  }.property('session'),
  name: null,
  username: null,
  gallery_name: null,
  gallery_location: null,
  pass: null,
  actions: {
    validate_modification_user: function(){
        this.set('name',this.get('session.name'));
        this.set('username',this.get('session.username'));
        this.set('gallery_name',this.get('session.gallery_name'));
        this.set('gallery_location',this.get('session.gallery_location'));
        showModificationConfirmationBox_user();      
    },
    leave_userModification: function(){
      hideConfirmationBox();
      this.transitionToRoute('admin_settings');
    },
    cancel_modification_user: function(){
      showCancelConfirmationBox_user();
    },
    close_cancel_confirmationBox: function(){
      hideConfirmationBox();
    },
    save_userModification: function(){
      var access_token = localStorage.getItem('accessTok');
      var name = this.get('name');
      var username = this.get('username');
      var gallery_name = this.get('gallery_name');
      var gallery_location = this.get('gallery_location');
      setUser(access_token,name,username,gallery_name,gallery_location);
      hideConfirmationBox();
      self.transitionToRoute('admin_settings');
      window.location.reload();
    },
    close_validate_confirmationBox: function(){
      hideConfirmationBox();
    },
    open_admin_user_pass_modification: function(){
      showModificationBox_password();
    },
    validate_modification_password: function(){
      var password = this.get('password');
      var confirm_password = this.get('confirm_password');
      if(password == confirm_password){
        this.set('pass',password);
        showModificationConfirmationBox_password();      
      }else{
        console.log('Something went wrong with your new password');
      }
    },
    save_password: function(){
      var access_token = localStorage.getItem('accessTok');
      var pass = this.get('pass');
      setPass(access_token,pass);
      hideConfirmationBox();
      this.get('controllers.login').send('logout',this);
    },
  }
});

Gallery.AdminSettingsController = Ember.Controller.extend({
  controllerName: 'admin_settings',
  needs: ['admin_settings'],
  fieldName: null,
  session: function(){
    var token = localStorage.getItem('accessTok');
    var user_data = getUser(token);
    localStorage.setItem('session', JSON.stringify(user_data));
    return user_data;
  }.property('session'),
  actions: {
    open_admin_user_modification: function(){
      this.transitionToRoute('admin_user_modification');
    },
    open_admin_setting_modification: function(field_cat,field_title,field_name,field_value,field_unit){
      this.set('fieldName',field_name);
      showModifySettingBox(field_cat,field_title,field_value,field_unit);
    },
    validate_modify_setting: function(){
      var token = localStorage.getItem('accessTok');
      var field_name = this.get('fieldName');
      var new_field_value = getNewFieldValue(field_name);
      setSetting(token,field_name,new_field_value);
      hideConfirmationBox();
    },
    cancel_modify_setting: function(){
      hideConfirmationBox();
    }
  }
});