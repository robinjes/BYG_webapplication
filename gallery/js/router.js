console.log('APPLICATION IS LOADING');
Gallery.Router.map(function() {
  this.resource('gallery', { path: '/' });
  this.resource('gallery_home', { path: ':gallery_name/:username' });
  this.resource('gallery_artists', { path: ':gallery_name/:username/artists' });
  this.resource('gallery_artist', { path: ':gallery_name/:username/:artist_id/artworks' });
  this.resource('gallery_frame', { path: ':gallery_name/:username/:artwork_id/frame' });
  this.resource('missing', { path: "/*path" });
});

/*##################### GALLERY #####################*/
Gallery.GalleryRoute = Ember.Route.extend({
  controllerName: 'gallery',
  beforeModel: function(transition) {
    if ((localStorage.getItem('userLogged') == "false")||(localStorage.getItem('userLogged') == null)) {
      window.location.replace("http://beyondyourgallery.com/admin");
    }else{
      var self = this;
      this.store.find('session').then(function(sessions) {
        var len = sessions.get("length");
        if(sessions.isLoaded&&(len>0)){
          var gallery_name = sessions.objectAt(0).get('gallery_name');
          var username = sessions.objectAt(0).get('username');
          self.transitionTo('gallery_home',gallery_name,username);
        }
      });
    }
  },
  actions: {
    error: function () {
      this.transitionTo('missing');
    }
  }
});
Gallery.GalleryHomeRoute = Ember.Route.extend({
  controllerName: 'gallery_home',
  beforeModel: function(transition) {
    if ((localStorage.getItem('userLogged') == "false")||(localStorage.getItem('userLogged') == null)) {
      window.location.replace("http://beyondyourgallery.com/admin");
    }
  },
  model: function(){
    return this.store.find('setting');
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('missing');
    }
  }
});

//Return all the existing artists
Gallery.GalleryArtistsRoute = Ember.Route.extend({
  controllerName: 'gallery_artists',
  beforeModel: function(transition) {
    if ((localStorage.getItem('userLogged') == "false")||(localStorage.getItem('userLogged') == null)) {
      window.location.replace("http://beyondyourgallery.com/admin");
    }
  },
  model: function() {
    return this.store.find('artist');
  },
  actions: {
    error: function () {
      this.transitionTo('missing');
    }
  }
});

Gallery.GalleryArtistRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if ((localStorage.getItem('userLogged') == "false")||(localStorage.getItem('userLogged') == null)) {
      window.location.replace("http://beyondyourgallery.com/admin");
    }else{
      var gallery_artist_id = this.controllerFor('gallery_home').get('gallery_artist_id');
      var self = this;
      if((gallery_artist_id === null) || (gallery_artist_id === 'null') || (gallery_artist_id === 'undefined') || (gallery_artist_id === undefined)){
        self.store.find('session').then(function(sessions) {
          var len = sessions.get("length");
          if(sessions.isLoaded&&(len>0)){
            var gallery_name = sessions.objectAt(0).get('gallery_name');
            var username = sessions.objectAt(0).get('username');
            self.transitionTo('gallery_home',gallery_name,username);
          }
        });
      }
    }
  },
  model: function() {
    var gallery_artist_id = this.controllerFor('gallery_home').get('gallery_artist_id');
    if(gallery_artist_id == 0){
      //Artist item with ID0 calls 'All' artworks from all artists of the gallery
      return this.store.find('artwork');
    }else{
      return this.store.find('artwork',{'artist_id':gallery_artist_id}); 
    }
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('missing');
    }
  }
});

Gallery.GalleryFrameRoute = Ember.Route.extend({
  beforeModel: function(transition) {
    if ((localStorage.getItem('userLogged') == "false")||(localStorage.getItem('userLogged') == null)) {
      window.location.replace("http://beyondyourgallery.com/admin");
    }
  },
  model: function() {
    //return frame;
  },
  actions: {
    loading: function(transition, originRoute) {
      return false;
    },
    error: function () {
      this.transitionTo('missing');
    }
  }
});