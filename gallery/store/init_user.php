<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect to the database");
	$link = mysql_select_db($db_name) or die("Could not select database");
	include("create_error.php");

	//Initialize the user
	$usernameoremail = $_POST['usernameoremail'];
	$password = $_POST['password'];

	if(!isset($usernameoremail)){
		create_error('-','PHP','create_artist_img.php','Username is Missing!');
		die('Username is Missing!');
	}
	if(!isset($password)){
		create_error('-','PHP','create_artist_img.php','Password is Missing!');
		die('Password is Missing!');
	}

	$password = md5($password);

	$result_user = mysql_query("select * from user where username = '" . $usernameoremail . "' or email = '" . $usernameoremail . "' and exclude = '0'", $db);
	$row_user = mysql_fetch_array($result_user, MYSQL_ASSOC);

	$result_gallery = mysql_query("select * from gallery where id = '" . $row_user['gallery_id'] . "'", $db);
	$row_gallery = mysql_fetch_array($result_gallery, MYSQL_ASSOC);

	$keyTok = md5(uniqid(mt_rand(), true));
	$userId = $row_user['id'];

	$access_query = mysql_query("select * from access_token where user_id = " . $userId, $db);
	if($access_query){
		if( mysql_num_rows($access_query) > 0) {
			mysql_query("update access_token set token = '" . $keyTok ."' where user_id = " . $userId, $db);
		}else{
			mysql_query("insert into access_token (token, user_id) values('" . $keyTok ."', " . $userId .")", $db);
		}
	}else{
		create_error($access_token,'PHP','init_user.php','No user found.', $db);
		die('No user found.');
	}
	$result_token = mysql_query("select * from access_token where token = '" . $keyTok . "'", $db);
	$row_token = mysql_fetch_array($result_token, MYSQL_ASSOC);

	if($row_user['password'] == $password){
		$row_array['accessTok'] = $row_token['token'];
		$row_array['username'] = $row_user['username'];
		$row_array['email'] = $row_user['email'];
    }else{
    	create_error($access_token,'PHP','init_user.php','Wrong password!',$db);
    	die('Wrong password!');
    };

    $session_get_query = mysql_query("select * from session where user_id = '" . $userId . "'", $db);
	if( mysql_num_rows($session_get_query) > 0) {
	}else{
		$session_insert_query = mysql_query("insert into session (user_id) values('" . $userId ."')", $db);
	}
    
	if((!$session_get_query)||(mysql_num_rows($session_get_query)<=0)){
		create_error($access_token,'PHP','init_user.php','No session found',$db);
		die('No session found');
	}
	$session_get_row = mysql_fetch_array($session_get_query, MYSQL_ASSOC);
	$sessionId = $session_get_row['id'];

	$time_of_creation = date("Y/m/d H:i:s");
	$history_insert_query = mysql_query("insert into history (session_id,time_of_creation) values('" . $sessionId ."','" . $time_of_creation ."')", $db);

	echo json_encode($row_array);
	//Close the database connection
	mysql_close($db);