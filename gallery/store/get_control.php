<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 			= parse_ini_file("../properties.ini");

	$destination_folder     = $ini_array['datas_folder_path'];

	$db_prefix 			= $ini_array['db_prefix'];
	$db_ip 				= $ini_array['db_ip'];
	$db_name 			= $ini_array['db_name'];
	$db_username 		= $ini_array['db_username'];
	$db_password 		= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Get the control for the whole application
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','get_control.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$row_options = array();

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','get_control.php','No user found',$db);
		die('No user found');
	}

	$control_query = mysql_query("select * from control"); 
	if((!$control_query)||(mysql_num_rows($control_query)<=0)){
		create_error($access_token,'PHP','get_control.php','No control found',$db);
		die('No control found');
	}
	$control_row = mysql_fetch_array($control_query, MYSQL_ASSOC);
	$row_options['id'] = $control_row['id'];
	$row_options['frame_section_enable'] = $control_row['frame_section_enable'];
	$row_options['artworks_tag_enable'] = $control_row['artworks_tag_enable'];

    $json_response_control = array();
    array_push($json_response_control,$row_options);

    echo json_encode($json_response_control);

	//Close the database connection
	mysql_close($db);