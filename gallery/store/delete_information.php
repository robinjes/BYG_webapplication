<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Delete an information
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','delete_information.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}
	$infoId = $_POST['info_id'];
	if(!isset($infoId)){
		create_error($access_token,'PHP','delete_information.php','Info ID is Missing!',$db);
		die('Info ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','delete_information.php','No access found',$db);
		die('No access found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$infoForGallery = mysql_query("delete from info_for_user where user_id = '" . $userId . "' and info_id = '" . $infoId . "'", $db);
	if(!$infoForGallery){
		create_error($access_token,'PHP','delete_information.php','No info for user found',$db);
		die('No info for user found');
	}

	//Close the database connection
	mysql_close($db);