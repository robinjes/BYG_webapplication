<?php
	header('Content-Type: application/json; charset=utf-8');
	
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################

	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Get the user
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','get_user.php','Access Token is Missing!',$db);
		die('Access Token ID is Missing!');
	}

	$access = mysql_query("select * from access_token where token = '" . $access_token . "'" , $db);
	if((!$access)||(mysql_num_rows($access)<=0)){
		create_error($access_token,'PHP','get_user.php','No user found - Part 1',$db);
		die('No user found');
	}
	$access_row = mysql_fetch_array($access, MYSQL_ASSOC);
	$userId = $access_row['user_id'];

	$user = mysql_query("select * from user where id = '" . $userId . "' and exclude = '0'" , $db);
	if((!$user)||(mysql_num_rows($user)<=0)){
		create_error($access_token,'PHP','get_user.php','No user found - Part 2',$db);
		die('No user found');
	}
	$user_row = mysql_fetch_array($user, MYSQL_ASSOC);
	$galleryId = $user_row['gallery_id'];
	$sectionId = $user_row['section_id'];
	$userName = $user_row['name'];
	$userEmail = $user_row['email'];
	$userUsername = $user_row['username'];

    $section = mysql_query("select * from section where id = '" . $sectionId . "'" , $db);
    if((!$section)||(mysql_num_rows($section)<=0)){
    	create_error($access_token,'PHP','get_user.php','No section found',$db);
    	die('No section found');
    }
    $section_row = mysql_fetch_array($section, MYSQL_ASSOC);
    $sectionRef = $section_row['ref'];

    $gallery = mysql_query("select * from gallery where id = '" . $galleryId . "'" , $db);
    if((!$gallery)||(mysql_num_rows($gallery)<=0)){
    	create_error($access_token,'PHP','get_user.php','No gallery found',$db);
    	die('No gallery found');
    }
    $gallery_row = mysql_fetch_array($gallery, MYSQL_ASSOC);
    $galleryName = $gallery_row['name'];
    $galleryLocation = $gallery_row['city'];

    $row_user['name'] = $userName;
    $row_user['email'] = $userEmail;
    $row_user['username'] = $userUsername;
    $row_user['location'] = $galleryLocation;
    $row_user['gallery_name'] = $galleryName;
    $row_user['group'] = $sectionRef;

    $artists = mysql_query("select * from artist where gallery_id = '" . $galleryId . "'" , $db);
    $artworks_qty = 0;
    if((!$artists)){
    	create_error($access_token,'PHP','get_user.php','No artists found',$db);
    	die('No artists found');
    }
    $row_user['qty_artists'] = mysql_num_rows($artists);
    while ($row_artists = mysql_fetch_array($artists, MYSQL_ASSOC)) {
    	$result_artworks = mysql_query("select * from artwork where artist_id= '" . $row_artists['id'] . "'", $db);
    	$artworks_qty = $artworks_qty + 1;
    }
    
    $row_user['qty_artworks'] = $artworks_qty;

    echo json_encode($row_user);
	//Close the database connection
	mysql_close($db);