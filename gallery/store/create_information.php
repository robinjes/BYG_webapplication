<?php
	header('Content-Type: application/json; charset=utf-8');
	############ Configuration ##############
	$ini_array 				= parse_ini_file("../properties.ini");

	$db_prefix 				= $ini_array['db_prefix'];
	$db_ip 					= $ini_array['db_ip'];
	$db_name 				= $ini_array['db_name'];
	$db_username 			= $ini_array['db_username'];
	$db_password 			= $ini_array['db_password'];
	##########################################
	
	$db = mysql_connect($db_ip, $db_username, $db_password) or die("Could not connect");
	mysql_select_db($db_name) or die("Could not select database");
	include 'create_error.php';

	//Create an information
	$access_token = $_POST['access_token'];
	if(!isset($access_token)){
		create_error('-','PHP','create_information.php','Access Token is Missing!',$db);
		die('Access Token is Missing!');
	}

	$message = $_POST['message'];
	if(!isset($message)){
		create_error($access_token,'PHP','create_information.php','Message is Missing!',$db);
		die('Message is Missing!');
	}
	
	//Close the database connection
	mysql_close($db);